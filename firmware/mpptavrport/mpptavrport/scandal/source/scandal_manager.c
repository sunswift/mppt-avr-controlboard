#include <scandal_engine.h>
#include <scandal_types.h>

sc_channel_id scandal_mk_channel_id(u08 priority, u08 source, u16 channel_num){
	return( ((u32)(priority & 0x07) << PRI_OFFSET) |	
		((u32)CHANNEL_TYPE << TYPE_OFFSET) |
		((u32)(source & 0xFF) << CHANNEL_SOURCE_ADDR_OFFSET) |
		((u32)(channel_num & 0x03FF) << CHANNEL_NUM_OFFSET));
}    

u32	scandal_mk_config_id(u08 priority, u08 node, u08 parameter){
	return( ((u32)(priority & 0x07) << PRI_OFFSET) |
		((u32)CONFIG_TYPE << TYPE_OFFSET) | 
		((u32)(node & 0xFF) << CONFIG_NODE_ADDR_OFFSET) |
		((u32)(parameter & 0x03FF) << CONFIG_PARAM_OFFSET ));
}
