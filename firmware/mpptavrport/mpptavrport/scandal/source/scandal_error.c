/* --------------------------------------------------------------------------
	Scandal Error Handling
	File name: scandal_error.c
	Author: David Snowdon

	Date: 14/08/02
   -------------------------------------------------------------------------- */

#include "scandal_error.h"
#include "scandal_engine.h"
#include "scandal_types.h"
#include "scandal_config.h"
#include "scandal_message.h"

u08  last_scandal_error = 0;
u08  last_user_error = 0;
u32  num_errors = 0;

/* Scandal error */
u08  scandal_get_last_scandal_error(){
	return last_scandal_error;
}

void scandal_do_scandal_err(u08  err){
	last_scandal_error = err;
	scandal_send_scandal_error(err);
	num_errors++;
}

/* User error */
u08  scandal_get_last_user_error(){
	return last_user_error;
}

void scandal_do_user_err(u08  err){
	last_user_error = err;
	scandal_send_user_error(err);
	num_errors++;
}

u32 scandal_get_num_errors(void){
  return num_errors;
}


void do_fatal_error(u08 err){
}

