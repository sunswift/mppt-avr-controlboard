/*! ------------------------------------------------------------------------- 
	CodeVisionAVR SPI Master Driver 
	 
	File name: spi_driver.h 
	Author: David Snowdon 
	Date: 19/06/02 
   -------------------------------------------------------------------------- */ 
                                
 #ifndef __SCANDAL_SPI__ 
 #define __SCANDAL_SPI__ 
  
 #include <scandal_types.h> 
 /* SPI Driver Prototypes */ 
 u08 init_spi(void);  
 u08 spi_select_device(u08	device);  
 void spi_deselect_all(void); 
 u08 spi_transfer(u08 out_data); 
  
 #endif 
  
  
