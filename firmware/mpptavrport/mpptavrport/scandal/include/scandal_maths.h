/*
 *  scandal_maths.h
 *
 *  Created by David Snowdon, MB, BE on Sat 7th June, 2003.
 *  Copyright (c) 2002. All rights reserved.
 *
 *  maths functions.
 *
 */


#ifdef    __SCANDAL_MATHS__
#define   __SCANDAL_MATHS__

#include "scandal_types.h"

s64      scandal_div64(s64 numerator, s64 denominator);
s64      scandal_div32(s32 numerator, s32 denominator);

s32      scandal_scale_value(s32 value, s32 m, s32 b);
s32      scandal_scaleaverage(s32 sum, s32 m, s32 b, s32 n);

#endif /* __SCANDAL_MATHS__ */
