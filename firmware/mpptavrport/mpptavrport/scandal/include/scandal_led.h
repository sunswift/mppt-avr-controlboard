/* --------------------------------------------------------------------------
	Scandal Engine for Atmega
	File name: scandal_led.h
	Author: William Widjaja

	Date: 15/06/13
   -------------------------------------------------------------------------- */ 


/* --------------------------------------------------------------------------
	Scandal Engine
	File name: scandal_led.h
	Author: David Snowdon

	Date: 14/08/02
   -------------------------------------------------------------------------- */ 
   
#include <scandal_types.h>   

void red_led(u08 on);
void toggle_red_led(void);

// void yellow_led(u08 on);
// void toggle_yellow_led(void);

void green_led(u08 on);
void toggle_green_led(void);

void yellow_15V_led(u08 on);
void toggle_yellow_15V_led(void);//atmega specific

void toggle_CANTX_led(void);//temp debug leds
void toggle_CANRX_led(void);//temp debug leds