/* --------------------------------------------------------------------------
	Scandal Error Handling
	File name: scandal_error.h
	Author: David Snowdon

	Date: 31/07/02
   -------------------------------------------------------------------------- */

#ifndef __SCANDAL_ERR__
#define __SCANDAL_ERR__

#include "scandal_types.h"

/* Scandal error codes */
#define NO_ERR           0	
#define LEN_ERR          1
#define BUF_FULL_ERR     2
#define NO_MSG_ERR       3
#define STD_ID_ERR	 4

u08  scandal_get_last_scandal_error();
void scandal_do_scandal_err(u08 	err);

u08  scandal_get_last_user_error();
void scandal_do_user_err(u08 	err);

void scandal_do_fatal_err(u08 	err);

u32 scandal_get_num_errors(void);
#endif
