/* config.c 
 * David Snowdon, 28 March, 2008
 */
		//gcc libraries for MSP430
//#include <io.h>
//#include <signal.h>
//#include <iomacros.h>

#include "scandal_devices.h"
#include "scandal_eeprom.h"
#include "scandal_types.h"

#include "error.h"
#include "config.h"

/* Magic number to make sure EEPROM has been programmed */ 
#define MPPTNG_CONFIG_MAGIC 0xAA

static inline void
calculate_checksum(mpptng_config_t config, uint8_t *sum, uint8_t *xor){
    uint8_t* array; 
    uint16_t i; 
    
    *sum = *xor = 0;
    array = (uint8_t*)(&config); 
    for(i=0; i<sizeof(config); i++){
        *sum += *array; 
        *xor ^= *array; 
        array++; 
    }
}

void
config_read(void){
   uint8_t sum, xor; 
   uint8_t insum, inxor; 

   sc_user_eeprom_read_block(0, (uint8_t*)&config, sizeof(config)); 
    
   insum = config.checksum; 
   inxor = config.checkxor; 
    
   config.checksum = config.checkxor = 0; 

   calculate_checksum(config, &sum, &xor); 

   if( (insum != sum) || (inxor != xor) ){
      printf("FATAL ERROR - UNSWMPPTNG_ERROR_EEPROM");
      mpptng_fatal_error(UNSWMPPTNG_ERROR_EEPROM); 
   }
}

int config_write(void){
   uint8_t sum, xor; 
 
   config.checksum = config.checkxor = 0; 
   
   calculate_checksum(config, &sum, &xor); 
   
   config.checksum = sum; 
   config.checkxor = xor; 
 
   sc_user_eeprom_write_block(0, (u08*)&config, sizeof(config));  //from mpptng.c volatile mpptng_config_t    config; 
 
   return 0; 
}
