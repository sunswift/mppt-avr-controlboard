#include "hardware.h"
#include "scandal_types.h"
#include "other_spi.h"
#include "spi_devices.h"
#include "fpga.h"
#include "scandal_led.h"
#include "mpptng.h"

void fpga_init(void){
	init_spi0();

	fpga_transfer(1, SIGNAL_RESTART, DEFAULT_RESTART);
	fpga_transfer(1, SIGNAL_AUX_LENGTH, DEFAULT_AUX_LENGTH);
	fpga_transfer(1, SIGNAL_AUX_OVERLAP, DEFAULT_AUX_OVERLAP);
	fpga_transfer(1, SIGNAL_PWM, DEFAULT_PWM);
	fpga_transfer(1, SIGNAL_DEADTIME, DEFAULT_DEADTIME);

	fpga_reset(); 
}
