/* Hardware definitions */
//Usage: Port name, Bit name (P5OUT |= BIT(0))

#define BIT(x) (1<<x)

//Atmega port definitions 
/*
//PORTB
  DDB0 1 OUT	- PSC		- Aux
	DDB1 0 IN		- NC
	DDB2 0 IN		- ADC		- Vin_meas
	DDB3 1 OUT	- LED		- GREEN
	DDB4 1 OUT	- LED		- YELLOW
	DDB5 0 IN		- ADC		- Iin_meas
	DDB6 0 IN		- NC
	DDB7 0 IN		- NC

	//PORTC
  DDC0 1 OUT	- PSC			- Main
	DDC1 0 IN		- PSCINT	- nVoutFault
	DDC2 1 OUT	- CAN			- CAN Tx
	DDC3 0 IN		- CAN			- CAN Rx
	DDC4 0 IN		- PCINT		- n15VFault
	DDC5 0 IN		- PCINT		- n5VFault
	DDC6 0 IN		- ADC			- T_heatsink
	DDC7 1 OUT	- PCINT		- Shutdown
	
	//PORTD data direction
  DDD0 1 OUT	- PSC			- Diode
	DDD1 0 IN		- PSCINT	- nCurrFault
	DDD2 0 IN		- ISP			- MISO
	DDD3 0 IN		- ISP			- MOSI
	DDD4 0 IN		- ISP			- SCK
	DDD5 0 IN		- ADC			- 15V_meas
	DDD6 0 IN		- ADC			- Vout_meas
	DDD7 1 OUT	- LED			- RED

	//PORTE data direction
  DDE0 0 IN		- RESET 
	DDE1 0 IN		- XTAL 
	DDE2 0 IN		- XTAL 
*/

//Atmega port definitions 
// Port A
//None

// Port B
#define AUX_SIG									BIT(0)		// NEW aux switch transfer from existing FPGA
#define SWITCH_MPPTNG           BIT(1)		// NEW hardware switch  tb implemented
#define UMEAS_IN1_PIN								BIT(2)		// ADC Vin measurement
#define LED1										BIT(3)		// GPIO																	Green led - scandal telemetry update Ts 800ms ~
#define ENABLE_15V		          BIT(4)		// NEW Enable 15V on board supply/			Yellow led - ADC interrupt debug LED fs 8kHz
#define IMEAS_IN1_PIN		            BIT(5)		// ADC Iin measurement
//#define INCH_IOUT		            BIT(6)		// NEW ADC Rev 2 only
//#define INCH_TAMBIENT		        BIT(7)		// NEW ADC Rev 2 only 

// Port C
#define MAIN_SIG								BIT(0)		// NEW main switch transfer from existing FPGA
#define nVOUTFAULT			        BIT(1)		// NEW Vout fault input transfer from existing FPGA
#define CAN_TX									BIT(2)		// NEW CAN Tx transceiver transfer from existing MCP2515			//Yellow tracking poll 
#define CAN_RX									BIT(3)		// NEW CAN Rx transceiver transfer from existing MCP2515			//Red pv track interrupt fs = 1Hz
#define n15VFAULT								BIT(4)		// NEW 15V fault input transfer from existing FPGA
#define n5VFAULT		            BIT(5)		// NEW 5V fault input transfer from existing FPGA
#define THEATSINK_PIN						BIT(6)		//  ADC 
#define SHUTDOWN								BIT(7)		// NEW shutdown signal to power board(transfer from FPGA

// Port D
#define DIODE_SIG								BIT(0)		// NEW diode switch
#define nCURRFAULT			        BIT(1)		// NEW Input current fault input transfer from existing FPGA
//ISP BIT(2:4)
#define MEAS15_PIN		            BIT(5)		// ADC 15V measurement 
#define UMEAS_OUT1_PIN								BIT(6)		// ADC Vout measurment
#define LED2										BIT(7)		//																			Red Led - PV tracking LED Ts 2s, 1s

// Port E
//Oscillator and master reset

/* Port 1 */
//For MSP
/*
#define CAN_INT         BIT(4)
#define FPGA_GPIO1      BIT(6)
#define FPGA_GPIO2      BIT(7)
*/

/* Port 2 */
//For MSP
/*
#define FPGA_GPIO3      BIT(0)
#define FS              BIT(1)
#define FS_RESET        BIT(2)
#define FPGA_RESET      BIT(3)
#define FPGA_ENABLE     BIT(5)
*/

/* Port 3 */
//For MSP
/*
#define STE0            BIT(0)
#define SIMO0           BIT(1)
#define SOMI0           BIT(2)
#define UCLK0           BIT(3)
#define FPGA_CS         BIT(4) // FPGA Chip Select (labelled "chpsel" on the board)
#define LED1            BIT(5)
#define LED2            BIT(6)
*/
/* Port 4 */

/* Port 5 */
//For MSP
/*
#define CAN_CS          BIT(0) // Note: re-defined in scandal_devices.h 
#define SIMO1           BIT(1)
#define SOMI1           BIT(2)
#define UCLK1           BIT(3)
*/

/* Port 6 / ADC */
//For MSP
/*
#define MEAS15_PIN      (BIT(3))
#define UMEAS_OUT1_PIN  (BIT(4))
#define UMEAS_IN1_PIN   (BIT(5))
#define IMEAS_IN1_PIN   (BIT(6))
#define THEATSINK_PIN  (BIT(7))
*/

/* ADC channel definitions */
/* These are a little different to most other MSP code I've written
   since they are optimised for the MPPTNG. 
   NOTE: The numbers below do not correspond to the pins. 
   THis is a bit of a hack, but check out control.c if you want to 
   see why this is so */ 

//priority definitions for Atmega
//Chosen for software 
#define MEAS_VOUT       3
#define MEAS_VIN1       5	
#define MEAS_IIN1       4
#define MEAS_15V        2
#define MEAS_THEATSINK  1
#define MEAS_TAMBIENT   0


#define MEAS_INIT				-1 //initial state for adc admux ??


//#define MEAS_IOUT       4 // to be imp. 
#define ADC_NUM_CHANNELS 6 //6 //check to see if array probs 

/* ADC Channel definitions 
	These define the actual channels which are used for each MEMCTL register */ 

//FIXME for atmega

//ADC MUX4:0 values
//ADC10D = 1	-> T_heatsink disable digital input 
//ADC9D = 0		->					enable digital input
//ADC8D = 0		->					enable digital input
//ADC7D = 1		-> Iout			disable digital input
//ADC6D = 1		-> Iin			disable digital input 
//ADC5D = 1		-> Vin			disable digital input 
//ADC4D = 1		-> T_amb		disable digital input 
//ADC3D = 1		-> Vout			disable digital input  
//ADC2D = 1		-> 15V			disable digital input 
//ADC1D = 0		->					enable digital input
//ADC0D = 0		->					enable digital input

//Atmega hardware ADC list (Chosen for ADMUX register)
#define INCH_VOUT               ADC_INPUT_ADC3 //ADC3 (V1 & V2) //see control.c for definitions
#define INCH_VIN1								ADC_INPUT_ADC8 //(V1)//ADC_INPUT_ADC5 (V2)//ADC5
#define INCH_IIN1								ADC_INPUT_ADC9 //ADC6 (V2)
#define INCH_15V								ADC_INPUT_ADC2 //ADC2 (V1 & V2)
#define INCH_THEATSINK	        ADC_INPUT_ADC10 //ADC10 (V1 & V2)

//#define INCH_TAMBIENT					  INCH_4  //ADC4 Rev 2 only
//#define INCH_IOUT								INCH_7 //ADC7 Rev 2 only



/*

//MSP actual hardware ADC list. Also actual pin Port 6 allocations  
#define INCH_VOUT               INCH_4
#define INCH_VIN1		INCH_5
#define INCH_IIN1		INCH_6
#define INCH_15V		INCH_3
#define INCH_THEATSINK	        INCH_7
#define INCH_TAMBIENT	        INCH_10
*/

/* Useful macros */ 
//#define FAULT_SIGNAL()  (P2IN & FS)
