
// Copyright (c) William Widjaja , 2013
// Copyright (c) David Snowdon, 2009
// Copyright (c) 2006 Atmel.

//FIXME: missing for atmega?

#include <avr/io.h>				// Define Port names for atmega64m1
#include <avr/interrupt.h> // Interrupt headers 

//#include <msp430x14x.h>
//#include <signal.h>
#include <string.h>

//for atmega
#include "scandal_types.h"
#include "psc.h"

//Uart debug stuff
#include "uart.h"		//macros and definitions for UART module

#include "control.h"
#include "scandal_led.h"
#include "scandal_engine.h"
#include "scandal_utils.h"
#include "scandal_message.h"
#include "scandal_config.h"
#include "scandal_adc.h"
#include "hardware.h"
#include "adc.h"
#include "mpptng.h"
#include "fpga.h"
#include "error.h"


// printf
//#include <stdio.h>
//#include <avr/io.h>				// Define Port names for atmega64m1

//FIX ME: how to calculate these values??
#define OUTPUT_TO_PWM(x) (((int32_t)x) >> 10)//14)//11)//14) // divide uk by 2^14 //10 works quite well
#define PWM_TO_OUTPUT(x) (((int32_t)x) << 10)//14)//11)//14) // multiply by 2^N


// #define OUTPUT_TO_PWM(x) (((int32_t)x) >> 14) // set PWM = OUTPUT 1:1 no scaling 
// #define PWM_TO_OUTPUT(x) (((int32_t)x) << 14)

#define INTEGRAL_DIVIDER_BITS 1 //not used
#define DIFFERENTIAL_DIVIDER_BITS 10 //not used

//#define OUT_MAX PWM_TO_OUTPUT(PWM_MAX) //= 996 <<14 ~ 18e6 ~ 24bit.
#define OUT_MIN PWM_TO_OUTPUT(PWM_MIN)
//volatile int32_t OUT_MAX = 250000;//25000000 works at home;//250000000 not working;//40534016;
#define OUT_MAX PWM_TO_OUTPUT(PWM_MAX) // scaled OUT_MAX = PWM_MAX<<14 = 3000<<14 = 

//Debug defines
//ADC defines

/* ADC configuration */
#define ADC_INPUT_ADC0    0
#define ADC_INPUT_ADC1    1
#define ADC_INPUT_ADC2    2		//ADC2D = 1		-> 15V			disable digital input 
#define ADC_INPUT_ADC3    3		//ADC3D = 1		-> Vout			disable digital input 
#define ADC_INPUT_ADC4    4		//ADC4D = 1		-> T_amb		disable digital input
#define ADC_INPUT_ADC5    5		//ADC5D = 1		-> Vin			disable digital input 
#define ADC_INPUT_ADC6    6		//ADC6D = 1		-> Iin			disable digital input
#define ADC_INPUT_ADC7    7		//ADC7D = 1		-> Iout			disable digital input
#define ADC_INPUT_ADC8    8
#define ADC_INPUT_ADC9    9
#define ADC_INPUT_ADC10   10	//ADC10D = 1	-> T_heatsink disable digital input
#define ADC_INPUT_AMP0    11
#define ADC_INPUT_AMP1    12
#define ADC_INPUT_ISRC    13
#define ADC_INPUT_BANDGAP 14

#define ADC_VREF_AREF     0
#define ADC_VREF_AVCC     1
#define ADC_VREF_INTERNAL 3

//Useful ADC macros Copyright (c) 2008 Atmel
#define Adc_clear_mux()  (ADMUX  &= ~((1<<MUX4)|(1<<MUX3)|(1<<MUX2)|(1<<MUX1)|(1<<MUX0)) )
#define Adc_select_channel(channel)  (ADMUX = ( ADMUX & (~((1<<MUX4)|(1<<MUX3)|(1<<MUX2)|(1<<MUX1)|(1<<MUX0)))) \
                         | (channel) ) // clear bits, then write channel

#define Adc_start_conv()                 (ADCSRA |= (1<<ADSC) )
#define Adc_start_conv_channel(channel)  (Adc_select_channel(channel),\
                                          Adc_start_conv() )

volatile int32_t target; /* target voltage mV */
volatile int32_t output; /* current pwm output */
volatile int32_t control_error; /* current pwn output */

volatile int     active_loop; /* Tracking on input or output */ 

volatile pid_data_t in_pid_data;
volatile pid_data_t out_pid_data;

//FIXME
// uint16_t min_vin_adc  = 0;    /* will be updated from the config */ 
// uint16_t max_vout_adc = 1024;//1024;// atmega 10bit ADC //4095; /* will be updated from the config*/ 

volatile int32_t min_vin_adc  = 0;    /* will be updated from the config */ 
volatile int32_t max_vout_adc = 1023;//1024;// atmega 10bit ADC //4095; /* will be updated from the config*/ 


//ADC interrupt global variables
//static 
volatile char adc_state = MEAS_INIT; // State of the ADC multiplexer 

// volatile int16_t vout = 0;//int32_t vout = 0;//int16_t vout; // last Vout sample from ADC
// volatile int16_t vin = 0;//int32_t vin = 0;//int16_t vin; //  last Vin sample from ADC /?? why is this 16 bit?

volatile int32_t vout = 0;//int32_t vout = 0;//int16_t vout; // last Vout sample from ADC
volatile int32_t vin = 0;//int32_t vin = 0;//int16_t vin; //  last Vin sample from ADC /?? why is this 16 bit?


volatile int32_t low_bits = 0; //lower 2 bits
volatile int32_t high_bits = 0; //upper 8 bits
volatile int32_t full_bits = 0; //total 10 bits
//int16_t iin; //  last Iin sample from ADC

volatile uint32_t PWM_temp = 1000;
volatile int16_t PWM_direction = 1; //signed

//control variables
volatile int32_t uk = OUT_MIN, in_uk = OUT_MIN, out_uk=OUT_MIN; //lol wtf

volatile int32_t pwm_duty_cycle = PWM_MIN;
//OUTPUT LOOP prints
volatile int32_t uk_copy;
volatile int32_t value_copy_pid;
volatile int32_t ek_copy;
volatile int32_t value_copy_pid_2;
volatile int32_t value_copy_pid_1;
volatile int32_t ek_copy_1 = 0;
volatile int32_t pid_const_kp = 0;
volatile int32_t pid_data_ek_1 = 0;
volatile int32_t pid_const_Ki = 0;
volatile int32_t pid_data_uk_1 = 0;
volatile int32_t pid_data_integral = 0;

//INPUT LOOP prints
volatile int32_t uk_copy_1;
volatile int32_t value_copy_pid_1;
volatile int32_t ek_copy_1;
volatile int32_t value_copy_pid_2_1;
volatile int32_t value_copy_pid_1_1;
volatile int32_t ek_copy_1_1 = 0;
volatile int32_t pid_const_kp_1 = 0;
volatile int32_t pid_data_ek_1_1 = 0;
volatile int32_t pid_const_Ki_1 = 0;
volatile int32_t pid_data_uk_1_1 = 0;
volatile int32_t pid_data_integral_1 = 0;

int32_t target_copy;
int32_t max_vout_adc_copy;
int error_copy;
/* Internal prototypes */ 
static inline uint16_t read_adc_value(u08 channel);


/*---------------------------------------------------------------
 ADC Code
 -- 
 The ADC code in the MPPTNG is fairly integrated with the control 
 loop to keep the latency between the measurement of the input
 voltage or input current as low as possible. 
 ----------------------------------------------------------------*/

//static volatile uint16_t			samples[ADC_NUM_CHANNELS]; //control.c
static volatile u32 samples[ADC_NUM_CHANNELS]; //adc.c

volatile uint32_t acc_value[ADC_NUM_CHANNELS]; 
volatile uint16_t acc_num[ADC_NUM_CHANNELS]; 


void init_adc(void) {
	//FIXME
	
			memset((uint16_t*)samples, 0, sizeof(samples[0])
			* ADC_NUM_CHANNELS);
	
		/* Zero out the accumulator readings */ 
	
	
		memset((uint32_t*)acc_value, 0, sizeof(acc_value[0])
			* ADC_NUM_CHANNELS);

		memset((uint16_t*)acc_num, 0, sizeof(acc_num[0])
			* ADC_NUM_CHANNELS);
	
	//Set port direction on ADC channels
	//Digital Input Disable Register 0
  DIDR0 = 0b10011100; //ADC7D = 1		-> Iout			disable digital input
											//ADC6D = 0		->					enable digital input 
											//ADC5D = 0		-> 					enable  digital input 
											//ADC4D = 1		-> T_amb		disable digital input 
											//ADC3D = 1		-> Vout			disable digital input  
											//ADC2D = 1		-> 15V			disable digital input 
											//ADC1D = 0		->					enable digital input
											//ADC0D = 0		->					enable digital input
											
	//Digital Input Disable Register 1
	DIDR1 = 0b00000111; //AMP2PD = 0		->				enable digital input
											//ACMP0D = 0		->				enable digital input
											//AMP0PD = 0		->				enable digital input 
											//AMP0ND = 0		->				enable digital input
											//ADC10D = 1		-> T_heatsink disable digital input 
											//ADC9D = 1			->	Iin		disable digital input
											//ADC8D = 1			->	Vin		disable digital input
									
	//ADMUX = 0b01101010; // REFS1:0 = 0b01			-> set AVcc as  Vref (3.3V) on AREF pin (0->3V)
											// ADLAR = 1					-> Set bit to Left adjust the ADC (top 8bits in ADRESH,bottom 2 in ADRESL (0->255)),	Clear bit-> Right justified for 10 bit (2 bit in ADRESH,8bits in ADRESL) .
											// MUX4:0  = 0b01010	-> channel source ADC10 to be converted (Theatsink)
	
	//ADMUX = 0b00101010; // REFS1:0 = 0b00			-> External Vref (3.0V) on AREF pin, Internal Vref is switched off
											// ADLAR = 1					-> Set bit to Left adjust the ADC (top 8bits in ADRESH,bottom 2 in ADRESL (0->255)),	Clear bit-> Right justified for 10 bit (2 bit in ADRESH,8bits in ADRESL) .
											// MUX4:0  = 0b00101	-> channel source ADC10 to be converted (heatsink)
											
	ADMUX = 0b00101000; // REFS1:0 = 0b00			-> External Vref (3.0V) on AREF pin, Internal Vref is switched off
											// ADLAR = 1					-> Set bit to Left adjust the ADC (top 8bits in ADRESH,bottom 2 in ADRESL (0->255)),	Clear bit-> Right justified for 10 bit (2 bit in ADRESH,8bits in ADRESL) .
											// MUX4:0  = 0b00101	-> channel source ADC5 to be converted (Vin)
	
 	adc_state = MEAS_VIN1; //ADC_INPUT_ADC5
 	Adc_select_channel(INCH_VIN1);
// 	

			//FIXME make a better multiplexer choice
	//FIXME - interrupt enabled for vin

	//ADMUX = 0b11001010; // REFS1:0 = 0b11			-> set internal Vref (2.56V) on AREF pin (0->2.56V)
											// ADLAR = 0					-> Set bit to Left adjust the ADC (top 8bits in ADRESH,bottom 2 in ADRESL (0->255)),	Clear bit-> Right justified for 10 bit (2 bit in ADRESH,8bits in ADRESL) .
											// MUX4:0  = 0b01010	-> channel source ADC10 to be converted (Theatsink)
	
	//ADMUX = (ADC_INPUT_ADC10 << MUX0); // heatsink

	
	// Fsys_clk = 16Mhz
	//Prescaler chosen to maximise resolution (125 kSPS at Maximum Resolution)
		
		//ADCSRA = 0b10101110;	// ADEN = 1					-> ADC enable
												// ADSC: 0					-> dont start ADC yet
												// ADATE: 1					-> autotrigger on (later trigger off timer 1)
												// ADIF: 0					-> interrupt flag bit
												// ADIE: 1					-> enable ADC interrupt flag, 
												// ADPS2:0 = 0b110	-> Set prescaler to 64 8E6/64=125 kHz/13 samples =9.6 kHz/Channel =>4.8 kHz to avoid aliasing 
	
	//not work vin vout 15V well. iin still oscillating with HSM on.
	//ADCSRA = 0b10101100;	// ADEN = 1					-> ADC enable
												// ADSC: 0					-> dont start ADC yet
												// ADATE: 1					-> autotrigger on (later trigger off timer 1)
												// ADIF: 0					-> interrupt flag bit
												// ADIE: 1					-> enable ADC interrupt flag, 
												// ADPS2:0 = 0b101	-> Set prescaler to 64 16E6/16= 1MHz/15.5 samples =64 kHz/Channel/4 - 16kHz =>32 kHz LP to avoid aliasing 
	
	
	//not working - only worked in shutdown modeseems to work vin vout 15V well. iin still oscillating with HSM on.
	//ADCSRA = 0b10101101;	// ADEN = 1					-> ADC enable
												// ADSC: 0					-> dont start ADC yet
												// ADATE: 1					-> autotrigger on (later trigger off timer 1)
												// ADIF: 0					-> interrupt flag bit
												// ADIE: 1					-> enable ADC interrupt flag, 
												// ADPS2:0 = 0b101	-> Set prescaler to 64 16E6/32=500 kHz/15.5 samples =32 kHz/Channel/4 - 8kHz =>16 kHz LP to avoid aliasing 
	
	
	//seems to work vin vout 15V well. iin still oscillating with HSM on.
	//ADCSRA = 0b10101110;	// ADEN = 1					-> ADC enable
												// ADSC: 0					-> dont start ADC yet
												// ADATE: 1					-> autotrigger on (later trigger off timer 1)
												// ADIF: 0					-> interrupt flag bit
												// ADIE: 1					-> enable ADC interrupt flag, 
												// ADPS2:0 = 0b110	-> Set prescaler to 64 16E6/64=250 kHz/15.5 samples =16 kHz/Channel/4 - 4kHz =>8 kHz LP to avoid aliasing 
	
	//ISR works iin is slow cant pick up 300Hz pulses acc. . vout, vin, 15V fine, temp not implemented
	ADCSRA = 0b10101111;	// ADEN = 1					-> ADC enable
												// ADSC: 0					-> dont start ADC yet
												// ADATE: 1					-> autotrigger on (later trigger off timer 1)
												// ADIF: 0					-> interrupt flag bit
												// ADIE: 1					-> enable ADC interrupt flag, 
												// ADPS2:0 = 0b111	-> Set prescaler to 128 16E6/128=125 kHz/15.5 samples =8.064 kHz/ 4 Channels => 2kHz. 4 kHz LP to avoid aliasing 

	//ADCSRA = 0b10001111;	// ADEN = 1					-> ADC enable
												// ADSC: 0					-> dont start ADC yet
												// ADATE: 0					-> autotrigger off (later trigger off timer 1)
												// ADIF: 0					-> interrupt flag bit
												// ADIE: 1					-> enable ADC interrupt flag, 
												// ADPS2:0 = 0b111	-> Set prescaler to 128 16E6/64=125 kHz/15.5 samples =8.064 kHz/ 5 Channels =>4 kHz LP to avoid aliasing 
	
	//ISR stops triggering after 2s
	//ADCSRA = 0b10011111;	// ADEN = 1					-> ADC enable
												// ADSC: 0					-> dont start ADC yet
												// ADATE: 0					-> autotrigger off (later trigger off timer 1)
												// ADIF: 1					-> interrupt flag bit set
												// ADIE: 1					-> enable ADC interrupt flag, 
												// ADPS2:0 = 0b111	-> Set prescaler to 128 16E6/64=125 kHz/15.5 samples =8.064 kHz/ 5 Channels =>4 kHz LP to avoid aliasing 
	
	//ADCSRA = 0b10001110;	// ADEN = 1					-> ADC enable
												// ADSC: 0					-> dont start ADC yet
												// ADATE: 0					-> autotrigger off 
												// ADIF: 0					-> interrupt flag bit
												// ADIE: 1					-> enable ADC interrupt flag, 
												// ADPS2:0 = 0b110	-> Set prescaler to 64 16E6/64=250 kHz/15.5 samples =16.064 kHz/ 5 Channels =>4 kHz LP to avoid aliasing 

	//ADCSRA = 0b10101110;	// ADEN = 1					-> ADC enable
												// ADSC: 0					-> dont start ADC yet
												// ADATE: 1					-> autotrigger off (later trigger off timer 1)
												// ADIF: 0					-> interrupt flag bit
												// ADIE: 1					-> enable ADC interrupt flag, 
												// ADPS2:0 = 0b110	-> Set prescaler to 64 16E6/64=250 kHz/15.5 samples =16.064 kHz/ 5 Channels =>4 kHz LP to avoid aliasing 

	//ADCSRA = 0b11101000;	// ADEN = 1					-> ADC enable
												// ADSC: 1					->  start ADC 
												// ADATE: 1					-> autotrigger on (later trigger off timer 1)
												// ADIF: 0					-> interrupt flag bit
												// ADIE: 1					-> enable ADC interrupt flag, 
												// ADPS2:0 = 0b000	-> Set prescaler to 1
	
	//ADCSRB = 0b10000100;	// ADHSM: 1					-> highspeed mode on,
												// ISRCEN: 0				-> disable current, use as reference, enable ext Aref (5), auto-trigger source (3,0)
												// AREFEN: 0				-> disconnect internal AREF circuit from AREF pin
												// ADTS3:0 = 0b0100	-> Timer/Counter1 Compare Match B 
	
	//ADCSRB = 0b10100000;	// ADHSM: 1					-> highspeed mode on,
												// ISRCEN: 0				-> disable current, use as reference, enable ext Aref (5), auto-trigger source (3,0)
												// AREFEN: 1				-> connect internal AREF circuit to AREF pin
												// ADTS3:0 = 0b0000	-> Free running
												
	//working with vin, vout, iin still patchy. 15V ok.
		//ADCSRB = 0b00100000;	// ADHSM: 0					-> highspeed mode off,
												// ISRCEN: 0				-> disable current, use as reference, enable ext Aref (5), auto-trigger source (3,0)
												// AREFEN: 1				-> connect internal AREF circuit to AREF pin
												// ADTS3:0 = 0b0000	-> Free running	
												
		//working with vin, vout, iin still patchy. 15V ok.
		ADCSRB = 0b10100000;	// ADHSM: 0					-> highspeed mode off,
												// ISRCEN: 0				-> disable current, use as reference, enable ext Aref (5), auto-trigger source (3,0)
												// AREFEN: 1				-> connect internal AREF circuit to AREF pin
												// ADTS3:0 = 0b0000	-> Free running
	
	//sei();               // enable interrupt subsystem globally -> enabling in main now, 

	//ADCSRA |= (1 << ADSC);  // Start A2D Conversions
	Adc_start_conv();
	
		/* Zero out the sample array */ 
        /* Clear sample arrays */
		

		   
		   
	
	/* Turn on 2.5V reference, enable ADC */
	/* Sample hold timer setting ?, Mulitple sample/conversion */
			//ADC12CTL0 = ADC12ON | SHT0_9 | SHT1_9 | REFON | REF2_5V | MSC;  
	
	/* Repeated, sequence mode; Use sampling timer, SMCLK */
			//ADC12CTL1 = SHP | CONSEQ_3 | ADC12SSEL_3 | CSTARTADD_0; 
	
	/* 
	 * Monitor the five channels. 
	 * These have a very deliberate ordering
	 * The latency between the control variables (Vin and Iin) and 
	 * the actual interrupt should be minimal. 
	 */
	/*
	//MSP specific
	
	
	ADC12MCTL0 = SREF_1 | INCH_TAMBIENT; // write to memory control register>
	ADC12MCTL1 = SREF_1 | INCH_THEATSINK;
	ADC12MCTL2 = SREF_1 | INCH_15V;
	ADC12MCTL3 = SREF_1 | INCH_VOUT;
	ADC12MCTL4 = SREF_1 | INCH_IIN1; 
	ADC12MCTL5 = SREF_1 | EOS | INCH_VIN1; 
	*/
	/* Enable interrupt for ADC12MCTL5 */
			//ADC12IE = (1 << 5);	// interrupt enable register 
	
	/* Zero out the sample array */ 
        /* Clear sample arrays */
		
//         memset((uint16_t*)samples, 0, sizeof(samples[0])
// 	       * ADC_NUM_CHANNELS);
	
	/* Zero out the accumulator readings */ 
	
	
//         memset((uint32_t*)acc_value, 0, sizeof(acc_value[0])
// 	       * ADC_NUM_CHANNELS);

//         memset((uint16_t*)acc_num, 0, sizeof(acc_num[0])
// 	       * ADC_NUM_CHANNELS);
	

	/* Enable conversions */
			//ADC12CTL0 |= ENC | ADC12SC;
}

/* Digital filtering scheme */ 
/* Note that this does not implement higher-order filters */ 
#define DIGITAL_FILTER(sample, new_sample){\
sample += (new_sample);		    \
sample >>= 1;			    \
}

static inline uint16_t 
read_adc_value(u08 channel){
	return samples[channel];
}

//FIXME atmega
//USage: channel is MEAS_VIN1, etc
 
// MEAS_VOUT       3
// MEAS_VIN1       5	
// MEAS_IIN1       4
// MEAS_15V        2
// MEAS_THEATSINK  1
// MEAS_TAMBIENT   0

u16 sample_adc(u08 channel){
	uint16_t sample; 
	

	/* Disable the ADC interrupt */ 
			ADCSRA &= ~(1 << ADIE);  // Atmega disable interrupts
			//ADC12IE &= ~(1 << 9); //MSP

	/* Short pause to make sure its off */
	
	//is this necessary? 
	{volatile int i; 
		for(i=15; i>0; i--)
			; 
	}
	
	sample = read_adc_value(channel); 
	//Usage: read_adc_value(u08 channel) -> return samples[channel];
	// returns latest sample in array.
	/* Turn the interrupt back on again */ 
			
			ADCSRA |= (1 << ADIE);  // Atmega enable interrupts
			//ADC12IE |= (1<<9); //MSP
	
	/* Return the most recent value of the ADC - Unscaled */
	return(sample); 
}

#define ACCUMULATE_VALUE(i, new_sample){\
  acc_value[i] += new_sample; \
  acc_num[i] ++; \
}

void adc_acc_read_and_zero(int i, uint32_t* value, uint16_t* num){
  /* Disable the ADC interrupt */ 
			//ADC12IE &= ~(1 << 9);
  
  /* Short pause to make sure its off */ 
  {volatile int j; 
    for(j=15; j>0; j--)
      ; 
  }
  
  *value = acc_value[i]; 
  *num = acc_num[i]; 

  //UART_printf("%d", i);  //failed attempt to debug

  acc_value[i] = 0;
  acc_num[i] = 0; 

  /* Turn the interrupt back on again */ 
			//ADC12IE |= (1<<9); 
}

/* Returns the number of samples */ 
uint32_t adc_acc_read_zero_divide(int i){ //(MEAS_VIN1)
  uint32_t value; 
  uint16_t num; 

  adc_acc_read_and_zero(i, &value, &num); //get value and num of samples
  
  return value / num;  //return value on samples
}


/*---------------------------------------------------------------
 Control loop code
 -- 
 The control loop code is designed to minimise the delay between 
 the input voltage measurement and the setting of the PWM. 
 ----------------------------------------------------------------*/

int control_is_saturated(void){
  return(in_pid_data.uk_1 >= OUT_MAX);
}

void
pid_init(volatile pid_data_t* pid_data){
	pid_data->uk_1 = 0;
	pid_data->ek_1 = 0; 
	pid_data->integral = 0; 
}

//determine control constant for error input ek,using PID constants in const and last samples in data

//OUTPUT control loop.
static inline int32_t /* uk */
pid_ctrl (int32_t ek, volatile pid_data_t* pid_data, volatile pid_const_t* pid_const) {
	
  int32_t uk;
  int32_t value; 
  
  // USAGE:
  //out_uk = pid_ctrl(vout - (int16_t)max_vout_adc, &out_pid_data, &config.out_pid_const);
	//in_uk = pid_ctrl_two(vin-target, &in_pid_data, &config.in_pid_const);
	
// 	ek,										the  input error ek = vout-max_vout_adc 
// 	pid_data,								old variables to be updated e(k-1), u(k-1), integral
// 	ek_1 is the e(k-1)						sample of input error
// 	integral is the sum of error*Ki			integral = integral + Ki*e(k)
//  uk_1 is the u(k-1) control input
  
// 	volatile pid_const_t* pid_const)			pid constants Kp Ki Kd 
// 		Kd is differential gain
// 		Kp is proportional gain
// 		Ki is integrations gain
		
//  int32_t uk is the control input			u(k) = Kp * e(k) + Kd * (e(k)-e(k-1))t + 
//  int32_t value	is the new input			value = uk + integral = Kp * e(k) + Kd * (e(k)-e(k-1))t + Ki * e(k) + integral
  
//   pid_const->Kp = -10000;
//   pid_const->Ki = -30;
//   pid_const->Kd = 0;
 
  //Copy control loop values to print 
		  ek_copy_1 = ek;// for print debug
		  pid_const_kp = pid_const->Kp;// for print debug
		  pid_data_ek_1 = pid_data->ek_1;// for print debug
		  pid_const_Ki = pid_const->Ki;// for print debug

	
  uk = ek * pid_const->Kp; /* Proportional */  // roughly 
  uk += pid_const->Kd * (ek - pid_data->ek_1); /* Differential */ 
  pid_data->integral += ek * pid_const->Ki; // summate integral
  
			uk_copy = uk;// for print debug
			pid_data_integral = pid_data->integral; // for print debug	
	
  /* Rate limit the increase of uk */
  value = uk + pid_data->integral; //summate the integral into uk
					uk_copy = uk;
				 value_copy_pid_1 = value;// for print debug
				//printf("value = %u\r\n", value); // for print debug very bad - slows down like the dogs
  
				pid_data_uk_1 = pid_data->uk_1; // for print debug
  
  //what is this for?? looks like a sweeper for PWM
  if(( value - pid_data->uk_1) > (OUT_MAX>>3)){// >> 3)){//3)){ // check the change in u(k) control input is ... 
	  
    value = pid_data->uk_1 + (OUT_MAX>>3);// >> 3);//3); //cap the output value
    pid_data->integral = value - uk; //update integral
	
  }
			value_copy_pid_2 = value;// for print debug
 //printf("value = %u\r\n", value);
  /* yyy <= xxx */
  if(value > OUT_MAX){
      value = OUT_MAX;
      pid_data->integral = value - uk; 
  }else if(value < OUT_MIN){
      value = OUT_MIN;
      pid_data->integral = value - uk; 
  }
	
	
	
   value_copy_pid = value; // for print debug
   ek_copy = ek;						// for print debug
	
   pid_data->uk_1 = value; // save last control output and error
   pid_data->ek_1 = ek;


   return value;
}


static inline int32_t /* uk */
pid_ctrl_two (int32_t ek, 
	 volatile pid_data_t* pid_data, 
	 volatile pid_const_t* pid_const) {
  int32_t uk_k;
  int32_t value_k; 
  
  // USAGE:
  //out_uk = pid_ctrl(vout - (int16_t)max_vout_adc, &out_pid_data, &config.out_pid_const);
	//in_uk = pid_ctrl(vin-target, &in_pid_data, &config.in_pid_const);
	
// 	int32_t ek,														the  input error ek = actual-target 
// 	volatile pid_data_t* pid_data,				old variables to be updated e(k-1), u(k-1), integral
	// 	ek_1 is the e(k-1) sample of input error
	// 	integral is the sum of error*Ki						integral = integral + Ki*e(k)
  //  uk_1 is the u(k-1) control input
  
// 	volatile pid_const_t* pid_const)			pid constants Kp Ki Kd 
// 		Kd is differential gain
// 		Kp is proportional gain
// 		Ki is integrations gain
		
//  int32_t uk is the control input			u(k) = Kp * e(k) + Kd * (e(k)-e(k-1))t + 
//  int32_t value	is the new input			value = uk + integral = Kp * e(k) + Kd * (e(k)-e(k-1))t + Ki * e(k) + integral
	
  
//   pid_const->Kp = -10000;
//   pid_const->Ki = -30;
//   pid_const->Kd = 0;
 
  //Copy control loop values to print 
  ek_copy_1_1 = ek;// for print debug
  pid_const_kp_1 = pid_const->Kp;// for print debug
  pid_data_ek_1_1 = pid_data->ek_1;// for print debug
  pid_const_Ki_1 = pid_const->Ki;// for print debug

	
  uk_k = ek * pid_const->Kp; /* Proportional */  // roughly 
  uk_k += pid_const->Kd * (ek - pid_data->ek_1); /* Differential */ 
  pid_data->integral += ek * pid_const->Ki; // summate integral
  
  uk_copy_1 = uk_k;// for print debug
	pid_data_integral_1 = pid_data->integral; // for print debug 
	
  /* Rate limit the increase of uk */
  value_k = uk_k + pid_data->integral; //summate the integral into uk
  value_copy_pid_1_1 = value_k;// for print debug
  //printf("value = %u\r\n", value); // for print debug very bad - slows down like the dogs
  
  pid_data_uk_1_1 = pid_data->uk_1; // for print debug
  
  //what is this for?? looks like a sweeper for PWM
  if(( value_k - pid_data->uk_1) > (OUT_MAX>>3)){// >> 3)){//3)){ // check the change in u(k) control input is ... 
    value_k = pid_data->uk_1 + (OUT_MAX>>3);// >> 3);//3); //cap the output value
    pid_data->integral = value_k - uk_k; //update integral
  }
  value_copy_pid_2_1 = value_k;// for print debug
 //printf("value = %u\r\n", value);
  /* yyy <= xxx */
  if(value_k > OUT_MAX){
     value_k = OUT_MAX;
     pid_data->integral = value_k - uk_k; 
  }else if(value_k < OUT_MIN){
    value_k = OUT_MIN;
    pid_data->integral = value_k - uk_k; 
  }
	
	
	
	value_copy_pid_1 = value_k; // for print debug
	ek_copy_1 = ek;						// for print debug
	
  pid_data->uk_1 = value_k; // save last control output and error
  pid_data->ek_1 = ek;


  return value_k;
}



void control_set_voltage(int32_t mvolts){
  int32_t min_vin; 
  
  min_vin = config.min_vin; 
  if(mvolts < min_vin) //sanity check
    mvolts = config.min_vin;

  /* Convert to ADC reading */
  scandal_get_unscaled_value(UNSWMPPTNG_IN_VOLTAGE, &mvolts);

			//ADC_INTERRUPT_DISABLE();
  target = mvolts;
			//ADC_INTERRUPT_ENABLE();	
}

void control_set_raw(int16_t raw){
  /* Fix this when you fix all the scaling */ 
  if(raw < min_vin_adc)
    raw = min_vin_adc; 

			//ADC_INTERRUPT_DISABLE();
  target = raw; 
			//ADC_INTERRUPT_ENABLE(); 
}

int32_t control_get_target(void){
    return target; 
}

void tracker_panic(int error){
  //fpga_enable(FPGA_OFF); 
  PSC_Enable(PSC_OFF);
  tracker_status &= ~STATUS_TRACKING; 
  //set software uC shutdown signal.
  //SHUTDOWN
  
  uC_output_state(0); //turn off PWM buffer.
  mpptng_error(error); 
  error_copy = error;
}

void control_init(void){
  active_loop = INPUT_LOOP; //toggle between control loops (mpptng.h)
  output = PWM_MIN; //mpptng.h

  /* FIXME: Should scale using the calibrated constants here */ 
  update_control_maxmin(); 
  init_adc(); 
}

void control_start(){
	pid_init(&in_pid_data); 
	pid_init(&out_pid_data); 
	output = PWM_MIN; //FIXME for ...
	active_loop = INPUT_LOOP; 
	tracker_status |= STATUS_INPUT_LOOP; //set status to input loop 
	//fpga_setpwm(output); //FIXME for PSC
	PSC_Setpwm(output);
}

/* -------------------------------
 Interrupt handlers 
 ------------------------------- */
/* ADC Interrupt -- Run the control loop */ 
//MSP
// what does this do?
//name to memory location (ADC12 memory)
/*
#define ADC12MEM_VOUT			ADC12MEM3
#define ADC12MEM_VIN1			ADC12MEM5
#define ADC12MEM_IIN1			ADC12MEM4
#define ADC12MEM_15V			ADC12MEM2
#define ADC12MEM_THEATSINK		ADC12MEM1
#define ADC12MEM_TAMBIENT		ADC12MEM0
*/

/* FIXME -- should be set to the maximum output voltage, converted to the equivalent ADC reading
	     -- should be updated whenever the config is updated */

/*void*/ 
volatile int set_max_vout_adc(int32_t new_vout_adc){//uint16_t new_vout_adc){
			//ADC_INTERRUPT_DISABLE();
  max_vout_adc = new_vout_adc; 
			//ADC_INTERRUPT_ENABLE();
  return 0; 
}

/*void*/ 
volatile int set_min_vin_adc(int32_t new_vin_adc){//uint16_t new_vin_adc){
			//ADC_INTERRUPT_DISABLE();
  min_vin_adc = new_vin_adc; 
			//ADC_INTERRUPT_ENABLE();
  return 0; 
}

/*void*/ 

//FIXME atmega?? interlinked with Scandal
volatile int update_control_maxmin(void){
   int32_t value; 
  
  value = config.max_vout; //FIXME relies on config structure
  scandal_get_unscaled_value(UNSWMPPTNG_OUT_VOLTAGE, &value);  //FIXME atmega
				//calculate the maximum scaled value based on user set m and b
  set_max_vout_adc(value); //set maximum of the scaled value, disabling interrupts 

  value = config.min_vin; 
  scandal_get_unscaled_value(UNSWMPPTNG_IN_VOLTAGE, &value); 
  set_min_vin_adc(value); 

  return 0;

}





//FIXME: set PSC. value is between 0 to 996 ~ 0 to 1000
// static inline void 
// psc_setpwm(uint16_t value){
//     if(value > PWM_MAX) { // make sure pwm within limits //see mpptng.h
//         value = PWM_MAX;
//     } else if(value < PWM_MIN) {
//         value = PWM_MIN; 
// 
// 		}   
// 		//FIXME:
// 		//fpga_transfer(1, SIGNAL_PWM, value);
// 		//set psc
// }


//FIXME ADC interrupt service 
//Atmega interrupt service routine
//Interrupt frequency is around 8kHz, 

ISR(ADC_vect){					//handler for ADC conversion complete interrupt
	ADCSRA &= ~(1<<ADEN); // turn on and off ADC. //FIXME: find faster solution.
	ADCSRA |= (1<<ADEN);
	//int32_t uk = OUT_MIN, in_uk = OUT_MIN, out_uk=OUT_MIN;  
	//uint32_t uk = OUT_MIN, in_uk = OUT_MIN, out_uk=OUT_MIN; 
	//local variables only  
// 	int16_t vout; // new sample
 //	volatile char high = 0; //  = ADC12MEM_VIN1;
	
	//check which multiplexer is chosen on ADMUX
	// currently vin, iin, and vout 
	//toggle_15V_led();
	
	//ADCSRA &= ~(1<<ADSC); // set ADSC to zero
	
	//ADCSRA|=(1<<ADIF); //set interrupt flag again?
	//toggle_yellow_15V_led(); //for debug only.
	
  	low_bits = ADCL; //MUST READ LOW BITS FIRST!!!!!!!!!!!!!!!!!!
  	high_bits = ADCH; //vin = ADCH; //save vin to ADC register value. ADCH clears its value
   full_bits = (FULL_BIT & ((low_bits&LOW_BIT)|((high_bits<<OFFSET)&HIGH_BIT)));  
   
   //full_bits = low_bits|(ADCH<<OFFSET);
   //once got data, disab;le and select channel
   
   //ADCSRA &= ~(1<<ADATE);
  //ADCSRA &= ~(1<<ADSC);
  //faster way 
  //full_bits = FULL_BIT & (ADCL|(ADCH<<OFFSET));
  //full_bits = FULL_BIT & (low_bits|(high_bits<<2));
  //vin = full_bits;
	//printf("BEFORE: vin = %d, high_bits = %d,low_bits = %d",vin, high_bits, low_bits);
			 
// 		if(full_bits >= 10){ // if it is > 1.56V
// 		  //PORTB = PORTB | 0b00010000;  //turn off YELLOW (DB4)  (set pin 4 to 1)
// 		  yellow_15V_led(1);
// 		  #if UART_DEBUG >= 1
// 		  //printf("ADCH >= 128 = %d", ADCH);
// 		  #endif
// 		  //PORTB = PORTB & 0b11101111;// just turn on YELLOW (DB4) (set pin 4 to 0)
// 			//PORTB = PORTB ^ 0b00010000;       //toggle  Yellow (DB4)
// 			//change the duty cycle on main switch (PSC B)
// 			//halve the on time 
// 			//Psc_set_module_B(B_SA_VAL,2000,B_SB_VAL);
// 			
// 	  } else {
// 		 //PORTB = PORTB & 0b11101111;// just turn on YELLOW (DB4) (set pin 4 to 0)
// 		 yellow_15V_led(0);
// 		 
// 		 #if UART_DEBUG >= 1
// 		 //printf("ADCH < 128 = %d", ADCH);
// 		 #endif
// 		 
// 		 //PORTB = PORTB ^ 0b00010000;       //toggle  Yellow (DB4)
// 		 //PORTB = PORTB | 0b00010000;  //turn off YELLOW (DB4)  (set pin 4 to 1)
// 		// Psc_set_module_B(B_SA_VAL,B_RA_VAL,B_SB_VAL);
// 		  
// 	  }
	
	
	//assume conversion is finished as interrupt triggered.
	
// 	if(adc_state == MEAS_VIN1) {
//   		// frequency is around 8/3kHz
//   	/*	toggle_15V_led();*/
//   
//   		//toggle_CANRX_led();
//    		
//   		
//   		if(ADCH >= 128){
//   			//printf("??????????????????????????????????????????????");
//   		} else {
//   			printf("ADCH = %d, ADCL = %d\n", ADCH, ADCL);
//   		}		
//   		
//   		if(vin < ADC_ABS_MIN_VIN){
//   			//tracker_panic(UNSWMPPTNG_ERROR_INPUT_UNDER_VOLTAGE); //turn off PSC and flag error
//   			//toggle_CANRX_led(); //Red
//   		}
//   				
//   		//adc_state = MEAS_IIN1; 
//   		
//     } else if (adc_state == MEAS_IIN1) {
//   	  //??
//   	  
//   	 // adc_state = MEAS_VOUT; 
//   	  
//     } else if (adc_state == MEAS_VOUT) {
//   		vout = ADC; //save vout to ADC register value
//   		//toggle_CANTX_led();
//   		//Check if measurement is over
//   		if(vout > ADC_ABS_MAX_VOUT){ //compares ADC value with scaled Absolute limit in mpptng.h
//   			//tracker_panic(UNSWMPPTNG_ERROR_OUTPUT_OVER_VOLTAGE); 
//   			//toggle_CANTX_led(); //Yelow
//   		}		
//   		//adc_state == MEAS_VIN1; 
//   	// check state stuff
//       
	//change multiplexer 
	
 
	
	//work out ADC status to filter and accumulate
	


 	if(adc_state == MEAS_VIN1) {
		 
// 		if(full_bits < ADC_ABS_MIN_VIN){
// 			//tracker_panic(UNSWMPPTNG_ERROR_INPUT_UNDER_VOLTAGE); //turn off PSC and flag error
// 			//toggle_CANRX_led(); //Red
// 		} 
 		vin = full_bits;	 // save most recent value
		 
		#if UART_DEBUG >= 1
		//printf("BEFORE samples(MEAS_VIN1) = %d", samples[MEAS_VIN1]);
 		#endif
 		DIGITAL_FILTER(samples[MEAS_VIN1], full_bits); 
		ACCUMULATE_VALUE(MEAS_VIN1, full_bits);
		#if UART_DEBUG >= 1
		//printf("AFTER DIGITAL samples(MEAS_VIN1) = %d, full_bits = %d", samples[MEAS_VIN1], full_bits);
		#endif
		#if UART_DEBUG >= 1
		//printf("AFTER samples(MEAS_VIN1) = %d, full_bits = %d", samples[MEAS_VIN1], full_bits);
 		#endif
		 
		//adc_state = MEAS_VIN1; //debug only -force vin to be read only
		adc_state = MEAS_IIN1; //debug only -force vin to be read only
 		//adc_state = MEAS_VOUT; //debug only -force vin to be read only
 		//Adc_start_conv_channel(INCH_VIN1); //debug only
		//Adc_select_channel(INCH_VIN1);
		Adc_select_channel(INCH_IIN1);
		//Adc_select_channel(INCH_VOUT);
		//Adc_start_conv_channel(INCH_IIN1); // select channel, then start conversion
		
  } else if (adc_state == MEAS_IIN1) {
	  #if UART_DEBUG >= 1
	  //printf("BEFORE DIGITAL samples(MEAS_IIN1) = %d, full_bits = %d", samples[MEAS_IIN1], full_bits);
		#endif
	  DIGITAL_FILTER(samples[MEAS_IIN1], full_bits); 
		ACCUMULATE_VALUE(MEAS_IIN1, full_bits);
	  
	  #if UART_DEBUG >= 1
	  //printf("AFTER DIGITAL samples(MEAS_IIN1) = %d, full_bits = %d", samples[MEAS_IIN1], full_bits);
		#endif
		#if UART_DEBUG >= 1
		//printf("AFTER vin = %d, ADCH = %d,samples(MEAS_VIN1) = %d, vin_low = %d, ADCL = %d ",vin, ADCH, samples[MEAS_VIN1], vin_low, ADCL);
 		#endif
		 
 		adc_state = MEAS_VOUT; 
// 	  Adc_start_conv_channel(INCH_VOUT);
		//adc_state = MEAS_VIN1; 
		//Adc_start_conv_channel(INCH_VIN1);
	  //Adc_select_channel(INCH_VIN1);
	  Adc_select_channel(INCH_VOUT);
	  
  } else if (adc_state == MEAS_VOUT) {
	  
	  
// 	  if(full_bits > ADC_ABS_MAX_VOUT){ //compares ADC value with scaled Absolute limit in mpptng.h
// 			//tracker_panic(UNSWMPPTNG_ERROR_OUTPUT_OVER_VOLTAGE); 
// 			//toggle_CANTX_led(); //Yelow
// 		}	
	  
	  vout = full_bits;
	  #if UART_DEBUG >= 1
	  //printf("BEFORE DIGITAL samples(MEAS_VOUT) = %d, full_bits = %d", samples[MEAS_VOUT], full_bits);
		#endif
		DIGITAL_FILTER(samples[MEAS_VOUT], full_bits); 
		ACCUMULATE_VALUE(MEAS_VOUT, full_bits);
		#if UART_DEBUG >= 1
		//printf("AFTER DIGITAL samples(MEAS_VOUT) = %d, full_bits = %d", samples[MEAS_VOUT], full_bits);
		#endif
		//adc_state = MEAS_VIN1; 
		adc_state = MEAS_15V;
		
		//Adc_select_channel(INCH_VIN1);
		Adc_select_channel(INCH_15V);
		
  }	else if (adc_state == MEAS_15V) {
	  
	  
// 	  if(full_bits > ADC_ABS_MAX_VOUT){ //compares ADC value with scaled Absolute limit in mpptng.h
// 			//tracker_panic(UNSWMPPTNG_ERROR_OUTPUT_OVER_VOLTAGE); 
// 			//toggle_CANTX_led(); //Yelow
// 		}	
	  
	  //toggle_yellow_15V_led();
	  #if UART_DEBUG >= 1
	  //printf("BEFORE DIGITAL samples(MEAS_VOUT) = %d, full_bits = %d", samples[MEAS_VOUT], full_bits);
		#endif
		DIGITAL_FILTER(samples[MEAS_15V], full_bits); 
		ACCUMULATE_VALUE(MEAS_15V, full_bits);
		#if UART_DEBUG >= 1
		//printf("AFTER DIGITAL samples(MEAS_VOUT) = %d, full_bits = %d", samples[MEAS_VOUT], full_bits);
		#endif
		adc_state = MEAS_VIN1; 
		//Adc_start_conv_channel(INCH_VIN1);
		Adc_select_channel(INCH_VIN1);
  }	
	//ADCSRA |= (1<<ADSC); //set ADSC again.
	
	 //ADCSRA |= (1<<ADATE);
   ADCSRA |= (1<<ADSC);
   
   //assume we have the a new sample from vin, vout and iin
	//printf("ADC_ABS_MAX_VOUT = %d\r\n", ADC_ABS_MAX_VOUT);
	//printf("ADC_ABS_MIN_VIN = %d\r\n", ADC_ABS_MIN_VIN);
	
	//Check hardware limits
	
	if(vout > ADC_ABS_MAX_VOUT){ //compares ADC value with scaled Absolute limit in mpptng.h
		tracker_panic(UNSWMPPTNG_ERROR_OUTPUT_OVER_VOLTAGE); 
		
	}else if(vin < ADC_ABS_MIN_VIN){
		tracker_panic(UNSWMPPTNG_ERROR_INPUT_UNDER_VOLTAGE);
			
	}else if((tracker_status & STATUS_TRACKING) == 0){
	    PSC_Setpwm(PWM_MIN);     //fpga_setpwm(PWM_MIN); //FIXME
	
 	}else{
	//if(1) {
		// If we have a fault signal from the FPGA, panic
		//FIXME
		//check the hardware fault inputs.
		// fault inputs are active low.
// 		if(((PORTC & nVOUTFAULT) == nVOUTFAULT)((PORTD & nCURRFAULT) == nCURRFAULT)||((PORTC & n15VFAULT) == n15VFAULT))) //n5VFAULT??
//		{
// 			tracker_panic(UNSWMPPTNG_ERROR_FPGA_SHUTDOWN); //deifferent error.
// 			return; 
// 			
// 		}
		
		
// 		if(fpga_nFS() == 0){
// 			tracker_panic(UNSWMPPTNG_ERROR_FPGA_SHUTDOWN); 
// 			return; 
// 		}

//Calculate control parameters
// Currently, these parameters dont give full range of PSC 
	//max_vout_adc = 800;//good for 140V //900; //works quite well//1023; still overshoots//1200 has overvoltage cutoffs //1023;// works up to certain power level. //700; //working
	//max_vout_adc = 620;//good for 100V //900; //works quite well//1023; still overshoots//1200 has overvoltage cutoffs //1023;// works up to certain power level. //700; //working
	
	   max_vout_adc_copy = max_vout_adc;//(int32_t)max_vout_adc;
	   //target = 50;
	   target_copy = target;
		   // Run the output control loop 
	   out_uk = pid_ctrl(vout -(int16_t)max_vout_adc, &out_pid_data, &config.out_pid_const);//(int16_t)max_vout_adc, &out_pid_data, &config.out_pid_const);
		   //inner input loop only
	   //in_uk = pid_ctrl_two(vin-target, &in_pid_data, &config.in_pid_const);
	   //in_uk = pid_ctrl(vin-target, &in_pid_data, &config.in_pid_const);
	
		   // Two control loops, Vout control and Vin control
		   //pick the lowest uk (back off when vout too high or vin too low.)
		
	   //if(out_uk < in_uk) {
	      uk = out_uk; //uk = out_uk;
   	//}else{
	      //uk = in_uk;
   	//}		    
				
		   //fpga_setpwm(OUTPUT_TO_PWM(uk)); //FIXME
		
		   //Test code
		   // 		if(vout -max_vout_adc < 0 & pwm_duty_cycle<PWM_MAX)//& uk <OUT_MAX ) 
		   // 		{
		   // 			pwm_duty_cycle++;
		   // 		} else if(vout -max_vout_adc > 0 & pwm_duty_cycle>PWM_MIN) {//& uk >OUT_MIN )  {
		   // 			pwm_duty_cycle--;
		   // 		} else {
		   // 			pwm_duty_cycle = PWM_MIN;
		   // 		}
		   // 		uk_copy = pwm_duty_cycle;
		
		   //PSC_Setpwm(pwm_duty_cycle); //FIXME
	   PSC_Setpwm(OUTPUT_TO_PWM(uk)); //FIXME
		 
		
		   //toggle_yellow_15V_led();
		   //(out = -
		   //More Test code	
		   // 		if(PWM_direction==1) {
		   // 			PWM_temp++;w
		   // 		} else if(PWM_direction==-1) {
		   // 			PWM_temp--;
		   // 		
		   // 		}			
		   // 		//PWM_temp =PWM_temp+ PWM_direction;
		   // 		
		   // 		if(PWM_temp>=PWM_MAX) {
		   // 			PWM_direction = -1;
		   // 		} else if(PWM_temp<=PWM_MIN) {
		   // 			PWM_direction = 1;
		   // 		}
		   // 			
		   // 		PSC_Setpwm(PWM_temp); //FIXME
		   // 		
		   // 		
	   output = OUTPUT_TO_PWM(uk); //convert uk to output for printing. Range should be between PWM_MIN and PWM_MAX
	   control_error = out_uk; //vout - (int16_t)max_vout_adc;
//
/*

		if(out_uk > in_uk){
				//	//Input tracking = 3 if output error is greater
			tracker_status |= STATUS_INPUT_LOOP; //set to input loop as in_uk is the limiting factor
			tracker_status &= ~STATUS_OUTPUT_LOOP; 
		}else{//Output tracking = 5 //in_uk>out_uk
		 //if(1){
			 */
	//tracker_status |= STATUS_OUTPUT_LOOP; //uncomment
	//tracker_status &= ~STATUS_INPUT_LOOP; //uncomment
		//}
		
		//*/
		if(out_uk > in_uk){
		  tracker_status |= STATUS_INPUT_LOOP; 
		  tracker_status &= ~STATUS_OUTPUT_LOOP; 
		}else{
		  tracker_status |= STATUS_OUTPUT_LOOP; 
		  tracker_status &= ~STATUS_INPUT_LOOP; 
		}
		
		
		
	}

	// We do any extra gumph for the rest of the system here  
	// atmega has poor adc integration
	//need to multiplex adc readings 
   
   
   
}





/*
//MSP
interrupt (ADC_VECTOR) ADC12ISR(void) {
  uint32_t uk = OUT_MIN, in_uk = OUT_MIN, out_uk=OUT_MIN;  
  int16_t vout = ADC12MEM_VOUT; 
  int16_t vin  = ADC12MEM_VIN1;

	if(vout > ADC_ABS_MAX_VOUT){
		tracker_panic(UNSWMPPTNG_ERROR_OUTPUT_OVER_VOLTAGE); 
	}else if(vin < ADC_ABS_MIN_VIN){
		tracker_panic(UNSWMPPTNG_ERROR_INPUT_UNDER_VOLTAGE);
	}else if((tracker_status & STATUS_TRACKING) == 0){
	        fpga_setpwm(PWM_MIN); 
	}else{
		// If we have a fault signal from the FPGA, panic
		if(fpga_nFS() == 0){
			tracker_panic(UNSWMPPTNG_ERROR_FPGA_SHUTDOWN); 
			return; 
		}

		// Run the output control loop 
		out_uk = pid_ctrl(vout - (int16_t)max_vout_adc, &out_pid_data, &config.out_pid_const);
		
		in_uk = pid_ctrl(vin-target, &in_pid_data, &config.in_pid_const);

		if(out_uk < in_uk)
		  uk = out_uk; 
		else
		  uk = in_uk; 
		
		fpga_setpwm(OUTPUT_TO_PWM(uk));
		output = OUTPUT_TO_PWM(uk); 
		control_error = out_uk; //vout - (int16_t)max_vout_adc;

		if(out_uk > in_uk){
		  tracker_status |= STATUS_INPUT_LOOP; 
		  tracker_status &= ~STATUS_OUTPUT_LOOP; 
		}else{
		  tracker_status |= STATUS_OUTPUT_LOOP; 
		  tracker_status &= ~STATUS_INPUT_LOOP; 
		}
	}

	// We do any extra gumph for the rest of the system here  
	DIGITAL_FILTER(samples[0], ADC12MEM0);
	DIGITAL_FILTER(samples[1], ADC12MEM1);
	DIGITAL_FILTER(samples[2], ADC12MEM2);
	DIGITAL_FILTER(samples[3], ADC12MEM3);
	DIGITAL_FILTER(samples[4], ADC12MEM4);
	DIGITAL_FILTER(samples[5], ADC12MEM0);

	ACCUMULATE_VALUE(0, ADC12MEM0)
	ACCUMULATE_VALUE(1, ADC12MEM1)
	ACCUMULATE_VALUE(2, ADC12MEM2)
	ACCUMULATE_VALUE(3, ADC12MEM3)
	ACCUMULATE_VALUE(4, ADC12MEM4)
	ACCUMULATE_VALUE(5, ADC12MEM5)
}
*/

//turn on (on=1) or turn off (on = 0) the PWM output buffer
volatile void uC_output_state(u08 on) 
{
	
	if(!on) { // shutdown the PWM	
		PORTC &= ~SHUTDOWN; //pull low (shutdown is active low) 
	} else {
		PORTC |= SHUTDOWN;
	}	
}

void adc_print( void ) 
{
	int32_t num = 1023;
	
	#if UART_DEBUG >= 1
	printf("CONTROL LOOP//////////////////////////////////////////////////////\r\n");
	
	printf("CONTROL LOOP: vout = %ld, pwm_duty_cycle = %ld,vout -max_vout_adc = %ld, max_vout_adc = %ld, %ld \r\n",vout, pwm_duty_cycle, vout -max_vout_adc, max_vout_adc, num);
	
	
	printf("CONTROL LOOP: max_vout_adc_copy = %ld, out_uk = %ld, in_uk = %ld, uk = %ld,OUTPUT_TO_PWM(uk) = %ld \r\n",max_vout_adc_copy, out_uk, in_uk, uk, OUTPUT_TO_PWM(uk));
// 	printf("OUT_MIN = %u, OUT_MAX = %d %ld \r\n",OUT_MIN, OUT_MAX, OUT_MAX);
// 	printf("PWM_MIN = %u, PWM_MAX = %u \r\n",PWM_MIN, PWM_MAX);
 	printf("CONTROL LOOP: Current PWM output = %ld, \r\n",output);
	
	//control paramaters 
	printf("OUTPUT LOOP//////////////////////////////////////////////////////\r\n");
	 printf("OUTPUTLOOP: ek_copy_1 = %ld, pid_const_kp = %ld, pid_data_ek_1 = %ld, pid_const_Ki = %ld \r\n", ek_copy_1, pid_const_kp, pid_data_ek_1,pid_const_Ki);
	 printf("OUTPUTLOOP: uk = %ld\r\n", uk_copy);
	 printf("OUTPUTLOOP: value_1 = uk + pid_data->integral = %ld\r\n, value_2 = pid_data->uk_1 + (OUT_MAX >> 3); = %ld \r\n", value_copy_pid_1, value_copy_pid_2);
	 printf("OUTPUTLOOP: pid_data_uk_1 = %ld, ek = %ld\r\n", pid_data_uk_1, ek_copy);
	
  printf("OUTPUTLOOP: value_3 = %ld, pid_data_integral = %ld\r\n", value_copy_pid, pid_data_integral);
  printf("INPUT LOOP//////////////////////////////////////////////////////\r\n");
  printf("INPUTLOOP: target_copy = %ld, ek_copy_1 = %ld, pid_const_kp = %ld, pid_data_ek_1 = %ld, pid_const_Ki = %ld \r\n", target_copy, ek_copy_1_1, pid_const_kp_1, pid_data_ek_1_1,pid_const_Ki_1);
	 printf("INPUTLOOP: uk = %ld\r\n", uk_copy_1);
	 printf("INPUTLOOP: value_1 = uk + pid_data->integral = %ld\r\n, value_2 = pid_data->uk_1 + (OUT_MAX >> 3); = %ld \r\n", value_copy_pid_1_1, value_copy_pid_2_1);
	 printf("INPUTLOOP: pid_data_uk_1 = %ld, ek = %ld\r\n", pid_data_uk_1_1, ek_copy_1);
	
  printf("INPUTLOOP: value_3 = %ld, pid_data_integral = %ld\r\n", value_copy_pid_1, pid_data_integral_1);
  
//   printf("out_Kd = %d,out_Ki = %d,out_Kp = %d\r\n ", config.out_pid_const.Kd, config.out_pid_const.Ki, config.out_pid_const.Kp );
// printf("in_Kd = %d,in_Ki = %d,in_Kp = %d\r\n ", config.in_pid_const.Kd, config.in_pid_const.Ki, config.in_pid_const.Kp );

	printf("Shutdown error= %d\r\n",error_copy);
	 #endif
}
