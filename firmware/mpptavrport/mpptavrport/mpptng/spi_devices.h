/* -------------------------------------------------------------------------- 
	SPI Devices 
	 
	File name: spi_devices.h
	Author: David Snowdon 
	Date: 12/04/02 
   ------------------------------------------------------------------------*/ 
  
#ifndef __SPIDEVICES__ 
#define __SPIDEVICES__ 

//FIXME for atmega
//#include <io.h>

#define BIT(x) (1<<x)

#define MCP2510			0              
#define SPI_NUM_DEVICES         1
#define SPI_DEVICE_NONE		SPI_NUM_DEVICES 

#define FPGA                    0
#define SPI0_NUM_DEVICES        1
#define SPI0_DEVICE_NONE        SPI0_NUM_DEVICES


/* MCP2510 */
#define ENABLE_MCP2510()        (P5OUT &= ~BIT(0))
#define DISABLE_MCP2510()       (P5OUT |= BIT(0))

/* FPGA */
#define ENABLE_FPGA_SPI()           (P3OUT &= ~BIT(4))
#define DISABLE_FPGA_SPI()          (P3OUT |= BIT(4))


#endif
