/* error.c 
 * David Snowdon, 28 March, 2008
 */

//FIXME for atmega
//#include <io.h>
//#include <signal.h>
//#include <iomacros.h>

#include "scandal_engine.h"
#include "scandal_led.h"
#include "scandal_timer.h"
#include "scandal_error.h"

#include "hardware.h"
#include "fpga.h"
#include "mpptng.h"

#include "error.h"

#include "psc.h"			// psc functions for atmega
#include "control.h" // output shutdown

int last_error = 0; 

void mpptng_do_errors(void){
  if(last_error != NO_ERR){
    scandal_do_user_err(last_error); 
    last_error = NO_ERR; 
  }
}

void mpptng_error(int error){
  last_error = error; 
}

/* This function doesn't return - we sit in here until 
   we're told to do something via CAN */ 

void mpptng_fatal_error(int error){
  int count = 0; 

   /* Disable the tracker */ 
   //fpga_enable(FPGA_OFF); 
   PSC_Enable(PSC_OFF); //these should never happen yet - testing.
   tracker_status &= ~STATUS_TRACKING; 
   //SHUTDOWN
   uC_output_state(0); //turn off PWM buffer.
   red_led(0); 

   while(1){
	   
      volatile uint32_t i; 

      handle_scandal(); 
    
      for(i=0; i<100000; i++)
      ; 
    
      count ++; 
      if(count <= (2 * error)) {
		   toggle_red_led();
	   }
       
      if(count > (2 * error + 4)) {
         scandal_do_user_err(error); 
         count = 0;
      }
   }
}
