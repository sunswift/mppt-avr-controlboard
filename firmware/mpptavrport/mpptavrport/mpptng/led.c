/* --------------------------------------------------------------------------
	LED Control
	File name: led.c
	Author: David Snowdon

	Date: 1/7/03
   -------------------------------------------------------------------------- */ 

//FIXME 
#include <avr/io.h>				// Define Port names for atmega64m1

//#include <io.h>
//#include <signal.h>
//#include <iomacros.h>

#include "scandal_led.h"

#include "hardware.h"
    
#define BIT(x) (1<<x)

// void yellow_led(u08 on){
// }
// 	
// void toggle_yellow_led(void){
// }
	
	
void green_led(u08 on){	
	if(!on)
		//P3OUT |= LED1;
		PORTB |= LED1;
	else
		//P3OUT &= ~LED1;	
		PORTB &= ~LED1;
}
 
void toggle_green_led(void){
	//P3OUT ^= LED1;
	PORTB ^= LED1;
}

void red_led(u08 on){
	
	if(!on)
		//P3OUT |= LED2;
		PORTD |= LED2;
	else
		//P3OUT &= ~LED2;	
		PORTD &= ~LED2;
	
}
 
void toggle_red_led(void){
	//P3OUT ^= LED2;
	PORTD ^= LED2;
}


void yellow_15V_led(u08 on){
	
	if(!on)
		//P3OUT |= LED2;
		PORTB |= ENABLE_15V;
	else
		//P3OUT &= ~LED2;	
		PORTB &= ~ENABLE_15V;
	
}


//atmega specific
void toggle_yellow_15V_led(void){
	//P3OUT ^= LED2;
	PORTB ^= ENABLE_15V;
}

//debug only LEDs until CAN is operational 
void toggle_CANTX_led(void){
	//P3OUT ^= LED2;
	PORTC ^= CAN_TX;
}

void toggle_CANRX_led(void){
	//P3OUT ^= LED2;
	PORTC ^= CAN_RX;
}

