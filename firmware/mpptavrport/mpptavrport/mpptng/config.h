#ifndef __MPPTNG_CONFIG_H__
#define __MPPTNG_CONFIG_H__

#include "mpptng.h"

void config_read(void);
int config_write(void);

#endif
