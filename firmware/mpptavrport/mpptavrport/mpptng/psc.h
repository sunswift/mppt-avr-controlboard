/*
 * IncFile1.h
 *
 * Created: 8/06/2013 12:04:29 AM
 *  Author: william
 */ 

#include "scandal_types.h" // typedefs

#include <avr/io.h>				// Define Port names for atmega64m1

// Using these values for testing 

//PSC Default signal on width values

//Start timing with diode On, then Main, then Aux
//Max value = 4095. Fpll = 64MHz, Fsw = 23kHz, 


//PSC constants 
#define F_PLL	64000000			//PLL frequency in Hz
#define F_PSC_out	60000			//PSC switching frequency in Hz

//PSC default switching register
#define RB_VAL 1067//(uint16_t)((F_PLL)/F_PSC_out) //3200 (20k) //2783 (23k)		// frequency = 64Mhz/RB_VAL = 23kHz, Max 12bit

//Aux calculations 
#define AUX_TIME 1680								//Aux signal time in ns.
#define NS_TO_S 1000000000
#define AUX_WIDTH_NS (uint16_t)(AUX_TIME*F_PLL) //convert to s as late as possible ~108
#define AUX_WIDTH 107//(uint16_t)(AUX_WIDTH_NS/1000000000)

//Overlap Deadtime calculations 
#define AM_OVERLAP_TIME 70 // in ns
#define DA_OVERLAP_TIME 100
#define MD_DEADBAND_TIME 200

#define AM_OVERLAP_WIDTH_NS (uint16_t)(AM_OVERLAP_TIME*F_PLL) //convert to s as late as possible ~108

//tried to do a calc, but hard coded in the end
#define AM_OVERLAP_WIDTH 4//(uint16_t)(AM_OVERLAP_WIDTH_NS/1000000000)

#define DA_OVERLAP_WIDTH_NS (uint16_t)(DA_OVERLAP_TIME*F_PLL) //convert to s as late as possible ~108
#define DA_OVERLAP_WIDTH 7//(uint16_t)(DA_OVERLAP_WIDTH_NS/1000000000)

#define MD_DEADBAND_WIDTH_NS (uint16_t)(MD_DEADBAND_TIME*F_PLL) //convert to s as late as possible ~108
#define MD_DEADBAND_WIDTH (uint16_t)(MD_DEADBAND_WIDTH_NS/1000000000)

//PSC0A = Diode
#define A_SA_VAL_DEFAULT 0					//deadtime
#define A_RA_VAL_DEFAULT 0					// on width
#define A_SB_VAL_DEFAULT RB_VAL			//2782	//arbitrary



#define A_SA_VAL 0									//deadtime
#define A_RA_VAL 1684								// on width
#define A_SB_VAL RB_VAL							//2782	//arbitrary

//PSC1A = Main
#define B_SA_VAL_DEFAULT 0					//deadtime
#define B_RA_VAL_DEFAULT 0					// on width
#define B_SB_VAL_DEFAULT RB_VAL			//2782	//arbitrary

#define B_SA_VAL 1781								//deadtime
#define B_RA_VAL 2770								// on width
#define B_SB_VAL RB_VAL							//2782	//arbitrary

//PSC2A = Aux
#define C_SA_VAL_DEFAULT 0					//deadtime these are hard limits - AUX never changes width
#define C_RA_VAL_DEFAULT 0					// on width
#define C_SB_VAL_DEFAULT RB_VAL			//2782	//arbitrary

#define C_SA_VAL 1678								//deadtime these are hard limits - AUX never changes width
#define C_RA_VAL 1786								// on width
#define C_SB_VAL RB_VAL							//2782	//arbitrary

#define OFFSET_PWM 0	//arbitrary main offset? 0 seems to work


/* Configure here your application */
#define PSC_PRESCALER PSC_NODIV_CLOCK								//set prescaler = 0
#define PSC_CLOCK_SOURCE PSC_CLOCK_SOURCE_EQ_PLL		//set psc clock to pll

#define PSC_MODE PSC_MODE_ONE_RAMP									//set mode to ramp mode

#define PSC_OUTPUT_B_POLARITY PSC_OUTPUT_HIGH				//set polarity of A Channels
#define PSC_OUTPUT_A_POLARITY PSC_OUTPUT_HIGH				//set polarity of B Channels


//PSC shutdown modes and PSC output definitions
#define PSC_ON      1
#define PSC_OFF     0

/* Signal definitions to be used in
   fpga_set_register(X, signal, X); */
// #define SIGNAL_RESTART       0
// #define SIGNAL_AUX_LENGTH    1
// #define SIGNAL_AUX_OVERLAP   2
// #define SIGNAL_PWM           3
// #define SIGNAL_DEADTIME      4 
 //PSC signal definitions 
#define SIGNAL_RESTART       0
#define SIGNAL_AUX_LENGTH    1
#define SIGNAL_MAIN_LENGTH   2
#define SIGNAL_DIODE_LENGTH  3
#define SIGNAL_PWM           4

 //PSC signal defaults
/* Default settings */ 
//units of these parameters? what is the resolution??
// not ms, could be us -> 32.74/us
//#define DEFAULT_PWM          0
// #define DEFAULT_AUX_LENGTH   55 // aux length = 1.680us
// #define DEFAULT_AUX_OVERLAP  8 //aux main overlap = 70ns -> =0.244us??
// #define DEFAULT_DEADTIME     25 //main - diode deadtime = 
// #define DEFAULT_RESTART      1300

#define DEFAULT_AUX		0 //FIXME: Defaults cant be zero?
#define DEFAULT_MAIN	0
#define DEFAULT_DIODE	0
#define DEFAULT_FREQUENCY	RB_VAL

/* PWM constants */ 
// #define PWM_MAX              ((DEFAULT_RESTART - DEFAULT_AUX_LENGTH) * 8 / 10) // = 1300-55*8/10 = 996
// #define PWM_MIN              0


//application specific macros
#define Psc_enable_psc_A_outputs() \
   POC = (1<<POEN2A)|(1<<POEN1A)|(1<<POEN0A);
       //! @{

#define Psc_clear_outputs() \
   POC = (0<<POEN2A)|(0<<POEN1A)|(0<<POEN0A);


//Prototypes
void PSC_Init (void); // setup the pulse widths 
void PSC_Reset(void); // reset the PSC, shutdown all signals

void PSC_Setpwm(int32_t value); // set the PSC output 

//static inline 
void PSC_Enable(u08 on); //enable the output buffers on the PSC


void PSC_print(void); //devug function