/* Copyright (C) David Snowdon, 2009 <scandal@snowdon.id.au> 
   Copyright (c) William Widjaja, 2013*/ 

/* 
 * This file is part of the UNSWMPPTNG firmware.
 * 
 * The UNSWMPPTNG firmware is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * The UNSWMPPTNG firmware is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 
 * You should have received a copy of the GNU General Public License
 * along with the UNSWMPPTNG firmware.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#define __ATmega64M1__ 1 //for the can_config.h and can_drv.h
//added atmega64M1 micro to mpptng supported list.

//FIXME
//#include "gcc_compiler.h"		// Define useful constants, macros and definitions
#include <avr/io.h>				// Define Port names for atmega64m1
#include <avr/interrupt.h> // Interrupt headers 
#include <avr/eeprom.h> // Interrupt headers 
#include <avr/wdt.h>				// Define Port names for atmega64m1

#include "pll_drv.h"			// Macros for PLL, Start_pll, Stop_pll
#include "psc.h"			// psc functions for atmega

#include "can_config.h"//CAN module FIXME merge with can_drv.h 
#include "can_drv.h"		//macros and definitions for CAN module 

#include "can.h"		//macros and definitions for CAN module 

//Uart debug stuff
#include "uart.h"		//macros and definitions for UART module

//#include "psc_drv.h"			// Macros for PSC, Start_pll, Stop_pll

//#include <io.h>
//#include <signal.h>
//#include <iomacros.h>

#include "scandal_types.h"

#include "scandal_timer.h"
#include "scandal_led.h"
#include "scandal_can.h"
#include "scandal_engine.h"
#include "scandal_spi.h"

#include "scandal_devices.h"
#include "scandal_utils.h"
#include "scandal_message.h"
#include "scandal_error.h"
#include "scandal_eeprom.h"
#include "scandal_adc.h"

//#include "adc.h"

#include "spi_devices.h"
#include "other_spi.h"
#include "fpga.h"
#include "mpptng.h"
#include "control.h"
#include "pv_track.h"
#include "error.h"
#include "config.h"

#include "hardware.h"

//EEPROM test
#define NUM_EXECUTIONS_ADDRESS 0x00
uint16_t num_executions = 0;
void read_EEPROM(void);

//FUSE definitions for clock 
// CKSEL: 8MHz external oscillator, Feeds 64MHz PLL, and system clock = 64/4= 16MHz 
// SUT: Slow start up time 
/*
__fuse_t __fuse __attribute__((section (".fuse"))) = 
  {
      .low = (FUSE_CKSEL3 & FUSE_CKSEL1), // 0xF5 = 0b11110101
			// FUSE_CKDIV8	= 1 UP	-> disable divide by 8, 
			// FUSE_CKOUT		= 1 UP	-> disable system clock to be output on PORTB0.
			// FUSE_SUT1		= 1 UP	-> slow start up,
			// FUSE_SUT0		= 1 UP	-> slow start up,
			// FUSE_CKSEL3  = 0 P		-> external osc 8MHz 
			// FUSE_CKSEL2  = 1 UP	-> external osc 8MHz			
			// FUSE_CKSEL1  = 0 P		-> external osc 8MHz
			// FUSE_CKSEL0	= 1 UP	-> external osc 8MHz
			
      .high = (FUSE_BOOTSZ0 & FUSE_BOOTSZ1 & FUSE_EESAVE & FUSE_SPIEN), //reset enable, maximum boot size, preserve eeprom, enable ISP 
      .extended = EFUSE_DEFAULT,
  };
*/

/* Switch to turn on debug information (via CAN) */ 
#define DEBUG           1

/* Switch to turn on debug information (via UART) */ 
//#define UART_DEBUG			1	//1
//Best way to use UART: Connect MOSI to RX of USB UART device. Connect GNDs together. Configure minicom to 9600 Baud rate, save as 

//#define CAN_DEBUG			1//1 // unused

/* Switch to enable or disable the watchdog timer */ 
#define USE_WATCHDOG    0	//disable for atmega now

/* Defines */ 
#define WDTCTL_INIT     WDTPW|WDTHOLD

/* Global variables */ 
volatile int tracker_status = 0;  
volatile mpptng_config_t    config; 

//Prototype
void doCAN();

#if USE_WATCHDOG  		//FIXME for atmega. overflow of 8s

static inline void 
init_watchdog(void){
	/* Set to use SMCLK 7.372800 MHz as the clock, 
		Divide by 512,
		Which gives an overflow of about 16s */ 
	//WDTCTL = WDT_ARST_1000;  //msp
	cli(); //disable interrupts
	wdr(); //Reset Watchdog Timer
	//__watchdog_reset();  
	/* Start timed equence */
	WDTCSR |= (1<<WDCE) | (1<<WDE); //allow Watchdog Change Enable
	//WDT oscillator - voltage dependent VCC = 3.0V, timeout 8.0s at scaler 1024K WDP[3:0] = 1001
	WDTCSR = (1<<WDE) | (1<<WDP3) | (1<<WDP0); //set to 1024K cycles ~ 8s
   sei(); //Enable global interrupts
}

/* Reset the timer to 0 so that we don't get reset */ 
static inline void kick_watchdog(void){
   //WDTCTL = WDT_ARST_1000 | WDTCNTCL;
   wdr(); //Reset Watchdog Timer
}

#endif

void WDT_off(void)
{
	cli(); //disable interrupts
	/* Clear WDRF in MCUSR */
	MCUSR &= ~(1<<WDRF);
	/* Write logical one to WDCE and WDE */
	/* Keep old prescaler setting to prevent unintentional time-out */
	WDTCSR |= (1<<WDCE) | (1<<WDE);
	/* Turn off WDT */
	WDTCSR = 0x00;
	sei(); //Enable global interrupts
}


void init_ports(void){
   //Atmega
      // Coz im dumb: Input = 0 Output = 1

   //PORT - IN/OUT - TYPE - FUNC

   //PORTB data direction
   /*
   DDB0 1 OUT	- PSC - Aux
   DDB1 0 IN		- NC - Switch
   DDB2 0 IN		- ADC - Vin_meas
   DDB3 1 OUT	- LED - Green
   DDB4 1 OUT	- LED - Yellow/15V Enable
   DDB5 0 IN		- ADC - Iin_meas
   DDB6 0 IN		- NC/ADC	- Iout_meas
   DDB7 0 IN		- NC/ADC	- Tamb_meas
   */
   //set the outputs to initial values 
   //set the data direction for inputs/outputs

   //AUX_SIG	SWITCH_MPPTNG  INCH_VIN1	 LED1	ENABLE_15V	 INCH_IIN1	INCH_IOUT	INCH_TAMBIENT	
   //P2OUT = FPGA_RESET | FPGA_ENABLE; 

   PORTB = 0x00; // Values:  Turn off Aux, turn on all LED
   DDRB = 0b00011000;//I/O control:  disable AUX
   //DDRB = 0b00011001; //Set I/O DDRB = 0x19
   // set bit to output, clear for input


   //PORTC data direction
   /*
   DDC0 1 OUT	- PSC - Main
   DDC1 0 IN		- PSCINT - nVoutFault
   DDC2 1 OUT	- CAN - CAN Tx
   DDC3 0 IN		- CAN - CAN Rx
   DDC4 0 IN		- PCINT - n15VFault
   DDC5 0 IN		- PCINT - n5VFault
   DDC6 0 IN		- ADC - T_heatsink
   DDC7 1 OUT	- PCINT - Shutdown
   */
   PORTC = 0x00;// Values: turn off PWM (keep in shutdown mode)0x80; // Turn off Main, pull up pin 7 for shutdown
   //DDRC = 0b10000101; //DDRC = 0x85
   DDRC = 0b10001101; //I/O control: DDRC = 0x85

   //PORTD data direction
   /*
   DDD0 1 OUT - PSC - Diode
   DDD1 0 IN - PSCINT - nCurrFault
   DDD2 0 IN - ISP - MISO
   DDD3 0 IN - ISP  - MOSI
   DDD4 0 IN - ISP  - SCK
   DDD5 0 IN - ADC - 15V_meas
   DDD6 0 IN - ADC - Vout_meas
   DDD7 1 OUT - LED - Red
   */
   PORTD = 0x00; // Values: Turn off diode, RED led on
   //DDRD = 0b10000001; //DDRD = 0x81 // set nCurr to 0 to input. 
   DDRD = 0b10000000; //I/O control: DDRD = 0x81 // set nCurr to 0 to input. Disable diodes

   //PORTE data direction
   /*
   DDE0 0 IN - RESET 
   DDE1 0 IN - XTAL 
   DDE2 0 IN - XTAL 
   */
   PORTE = 0b000; // Values: init
   DDRE = 0b000; //I/O control: DDRE

   /*
   //MSP
   P1OUT = 0x00;
   P1SEL = 0x00;
   P1DIR = 0x00;
   P1IES = CAN_INT;
   P1IE  = 0x00; // CAN_INT;  // CAN Interrupt disabled by default

   P2OUT = FPGA_RESET | FPGA_ENABLE; 
   P2SEL = 0x00;
   P2DIR = FS_RESET | FPGA_RESET | FPGA_ENABLE;
   P2IES = 0x00;
   P2IE  = 0x00;

   P3OUT = 0x00;
   P3SEL = SIMO0 | SOMI0 | UCLK0;
   P3DIR = SIMO0 | UCLK0 | FPGA_CS | LED1 | LED2;

   P4OUT = 0x00;
   P4SEL = 0x00;
   P4DIR = 0x00;

   P5OUT = CAN_CS;
   P5SEL = SIMO1 | SOMI1 | UCLK1;
   P5DIR = CAN_CS | SIMO1 | UCLK1;

   P6SEL = MEAS15_PIN | IMEAS_IN1_PIN | 
   UMEAS_OUT1_PIN | UMEAS_IN1_PIN | THEATSINK_PIN;  
   */
}

void init_clock(void){
	
	//Configure the PLL (same as above)
   PLLCSR = 0b00000110;				// Bit 7..3 � Reserved:
															// Bit 2 � PLLF			= 1			->	Set the PLL output to 64MHz. 
															// Bit 1 � PLLE:		= 1			->	PLLE is set, the PLL is startedn 
															// Bit 0 � PLOCK:		= 0			->	PLOCK bit is set, the PLL is locked to the reference clock,
															//!< Start the PLL at 64MHz, using external OSC , disable lock 0b00000110
  
   while (!(PLLCSR & (1<<PLOCK))); //wait until the PLL locks onto 64Mhz
  //Wait_pll_ready();			//wait until the PLL locks onto 64Mhz
	
	//configure clock register settings->fuses?
	
   //MSP
   //volatile unsigned int i;

   /* XTAL = LF crystal, ACLK = LFXT1/1, DCO Rset = 4, XT2 = ON */
         //BCSCTL1 = 0x04;

   /* Clear OSCOFF flag - start oscillator */
         //_BIC_SR( OSCOFF );
         
         /*
   do{
    // Clear OSCFault flag 
            //IFG1 &= ~OFIFG; 
    // Wait for flag to set
    for( i = 255; i > 0; i-- )
      ;
   } while(( IFG1 & OFIFG ) != 0);
         */

   /* Set MCLK to XT2CLK and SMCLK to XT2CLK */
         //BCSCTL2 = 0x88; 
}


/*--------------------------------------------------
  Interrupt handling for CAN stuff 
  --------------------------------------------------*/
void enable_can_interrupt(){
	//FIXME
   /*  P1IE = CAN_INT; */
   /* CAN interrupt disabled for now, since it seems to 
     cause problems with the control loops. I suspect
     this is because we are accessing the MCP2510 from 
     both inside and outside an interrupt context, 
     which probably leads to badness */ 
}

void disable_can_interrupt(){
	//FIXME
	//P1IE = 0x00;
}

/* Interrupt handler associated with internal RTC */
/* Timer A overflow interrupt */

//AVR internal handler for CAN?

		/*
interrupt (PORT1_VECTOR) port1int(void) {
   can_interrupt();
   P1IFG = 0x00;
}
		*/


void read_EEPROM() {
	
   uint16_t num_executions = eeprom_read_word((uint16_t*)NUM_EXECUTIONS_ADDRESS);
   
   printf("EEPROM: num_executions = %x\r\n", num_executions);
   
   if (num_executions==0xFFFF)
      num_executions=0;
	   
   num_executions++;
   
}


/* Main function */
int main(void) {
   sc_time_t     my_timer;  
   int32_t value; 

   WDT_off();		//disable watchdog
   cli(); //Global Interrupt Disable
   //dint(); //MSP

#if USE_WATCHDOG
   init_watchdog(); 
#endif

   init_clock();		//setup PLL and system clock  
   init_ports();		//define port direction
   init_uart();
   sc_init_timer(); //setup 1ms timer FIXME set,enable and get timer 

   can_init(); // setup CAN controller
   can_buffer_init(); // setup CAN buffers initialise variables 
   scandal_init(); //FIXME eeprom and CAN FIXME cant send CAN

	
#if UART_DEBUG//debug UART
	
#endif

   config_read();  //scandal eeprom, checksum FIXME cant send CAN, will give fatal error and turn off PSC

   //delay? to make sure everthing is set.
   //is this necessary aswell?
   {volatile int i; 
      for(i=0; i<100; i++)
         ;
   }

   /* Below here, we assume we have a config */ 

   /* Send out the error that we've reset -- it's not fatal obviously, 
     but we want to know when it happens, and it really is an error, 
     since something that's solar powered should be fairly constantly 
     powered */ 
   scandal_do_user_err(UNSWMPPTNG_ERROR_WATCHDOG_RESET); 
    
    /* Make sure our variables are set up properly */ 
   tracker_status = 0;     
    
   /* Initialise FPGA (or, our case, CPLD) stuff */ 
   //FIXME for PSC
   //fpga_init(); 

   /* Starts the ADC and control loop interrupt */
   control_init(); //init ADC interrupts, need to toggle multiplexer by writing to register. 

   /* Initialise the PV tracking mechanism */ 
   pv_track_init(); 

   //set control target
   //control_set_voltage(5000); // in mV DEBUG!!

   sei();  // enable interrupt system globally 
      //Assume interrupt enabled timer 0 and 1 and ADC?
   //eint();

   PSC_Enable(PSC_OFF);	
   uC_output_state(0); //keep in shutdown mode //PORTC &= ~SHUTDOWN;

   PSC_Init(); //FIXME: this currently turns on all outputs, without checking
   //PSC_Enable(PSC_ON);

   my_timer = sc_get_timer();  //get time before loop
#if UART_DEBUG 
	printf("//START OF WHILE////////////////////////////////////////////////////////////////////////\r\n");
#endif
//////////////MAIN LOOP/////////////////////////////////////////////////////////////////////////
   
   while (1) {				// scandal telemetry main loop 
      sc_time_t timeval; 

      timeval = sc_get_timer();		// always get new time
      //scandal_do_user_err(UNSWMPPTNG_ERROR_WATCHDOG_RESET); // debug equivalent doCAN test for CAN
      //doCAN(); //works	
      handle_scandal(); //FIXME cant send CAN, will cause PSC shutdown

      /* pv_track sends data when it feels like it */ 
      pv_track_send_data();  //FXIME, cant send CAN
      pvtrack_uart();
      //read_EEPROM();
	   //printf("EEPROM after\r\n");    
	     
#if USE_WATCHDOG
      kick_watchdog(); 
#endif
   
      /* Periodically send out the values recorded by the ADC */ 
      if(timeval >= my_timer + TELEMETRY_UPDATE_PERIOD){ //check if we need to update telemetry
         my_timer = timeval; 
         toggle_green_led(); //toggle green led on Atmega
         //toggle_15V_led();
         mpptng_do_errors();

         //debug uart prints
#if UART_DEBUG >= 1
         printf("Main LOOP: Telemetry///////////////////////////////////// \r\n");
#endif
         pvtrack_debug();
         printf("After debug print \r\n");
         adc_print(); 
         PSC_print();

         {volatile int i; 
         for(i=15; i>0; i--)
               ; 
         }
         //defunct
         //pv_track_send_telemetry(); //FIXME cant send CAN
         /* Wem send the Input current and voltage from 
         within the pvtrack module */ 

         scandal_send_scaled_channel(TELEM_LOW, UNSWMPPTNG_IN_VOLTAGE, 
                        sample_adc(MEAS_VIN1));
						
         //FIXME!!! Why do these counters need to be here?
         {volatile int j; 
         for(j=10000; j>0; j--)
               ; 
         }
						
         scandal_send_scaled_channel(TELEM_LOW, UNSWMPPTNG_IN_CURRENT, 
                        sample_adc(MEAS_IIN1));
	
// 	        value = sample_adc(MEAS_VIN1); 
// 				//get rid of scaling
//         scandal_get_scaled_value(UNSWMPPTNG_IN_VOLTAGE, &value); // FIXME for scaling eeprom
//          
// 				#if UART_DEBUG >= 1
// 					printf("vin = %ld", value);
// 				#endif
// 				
// 		 //debug 
//           scandal_send_scaled_channel(TELEM_LOW, UNSWMPPTNG_IN_VOLTAGE, value); // debug just raw values out of ADC
//                         //sample_adc(MEAS_VIN1));
				
         {volatile int j; 
         for(j=10000; j>0; j--)
               ; 
         }
	
         scandal_send_scaled_channel(TELEM_LOW, UNSWMPPTNG_OUT_VOLTAGE, 
                   sample_adc(MEAS_VOUT));
         {volatile int j; 
            for(j=10000; j>0; j--)
               ; 
         }
				   
        scandal_send_scaled_channel(TELEM_LOW, UNSWMPPTNG_HEATSINK_TEMP, 
                    sample_adc(MEAS_THEATSINK));
         {volatile int j; 
         for(j=10000; j>0; j--)
               ; 
         }

         scandal_send_scaled_channel(TELEM_LOW, UNSWMPPTNG_15V, 
                    sample_adc(MEAS_15V));
         {volatile int j; 
         for(j=10000; j>0; j--)
               ; 
         }
         scandal_send_channel(TELEM_LOW, UNSWMPPTNG_STATUS, 
                    tracker_status); 

        /* Pre-scale for the temperature */ 
        //{
//             int32_t degC = sample_adc(MEAS_TAMBIENT); 
//             degC = (((degC - 1615)*704*1000)/4095); //FIXME magic numbers...
//             scandal_send_scaled_channel(TELEM_LOW, UNSWMPPTNG_AMBIENT_TEMP, 
//                                         degC);
        //}

#if DEBUG
//         scandal_send_channel(TELEM_LOW, 134, output);	
//         scandal_send_channel(TELEM_LOW, 136, fpga_nFS()); 
#endif
      }  /*END TELEMETTY*/

   /*  If we're not tracking, 
     check to see that our start-up criteria are satisfied, and then
     initialise the control loops and restart tracking */ 
				
#if UART_DEBUG
      printf("MAIN LOOP: tracker_status = %d\r\n", tracker_status); //debug 0b00 = no tracking, 0b10= input tracking, 0b100 = output tracking
#endif
				
      if((tracker_status & STATUS_TRACKING) == 0){
         
#if UART_DEBUG
         printf("STARTUP TRACKING///////////////////////////////////// \r\n");
#endif
         /* Check the input voltage */
            //toggle_CANTX_led();
         //printf("sample_adc(MEAS_VIN1) = %d\r\n", sample_adc(MEAS_VIN1));
         value = sample_adc(MEAS_VIN1); 
               
         //get rid of scaling
         scandal_get_scaled_value(UNSWMPPTNG_IN_VOLTAGE, &value); // FIXME for scaling eeprom
            
#if UART_DEBUG
         printf("Main LOOP: vin = %ld\r\n", value);
#endif

   //debug 
#if DEBUG
         //scandal_send_scaled_channel(TELEM_LOW, UNSWMPPTNG_IN_VOLTAGE, value); // debug just raw values out of ADC
#endif
         //sample_adc(MEAS_VIN1));
   // 	
   //uncomment for debug only				
         if(value < config.min_vin) { continue; } //if the input is lower than minimum, continue past and skip
   // 							
               
         /* Check the output voltage */
         value = sample_adc(MEAS_VOUT);
         scandal_get_scaled_value(UNSWMPPTNG_OUT_VOLTAGE, &value); 

#if UART_DEBUG
         printf("Main LOOP: vout = %ld\r\n", value);
#endif

#if DEBUG
         //scandal_send_scaled_channel(TELEM_LOW, UNSWMPPTNG_OUT_VOLTAGE, value);
#endif
         //                    sample_adc(MEAS_VOUT));

         if(value > config.max_vout) { continue; }//if the output is higher than maximum, continue past and skip
                  
         //debug only recomment after		
         tracker_status |= STATUS_TRACKING; //set status to tracking

         /* Initialise the tracking algorithm */ 
         //testing     
         pv_track_init(); //not used 

         /* Reset the FPGA */ 	 
         //fs_reset(); //FIXME reset PSC flag...
         PSC_Reset();

         /* Initialise the control loop */ 
         control_start(); //does this get started first of the ADC interrupt? Nope
                                    // check status tracking flag...
                                    // set starting PWM for PSC 

           /* Enable the FPGA */ 
               /*Enable the PSC output*/
           //fpga_enable(FPGA_ON); 
         
         // print out the internal states"
#if UART_DEBUG >= 1
         printf("Main LOOP: PSC_Enable = ON\r\n");
#endif
         
         PSC_Enable(PSC_ON);
         
         uC_output_state(1); //turn on PWM buffer.
                
      }
   }
}


