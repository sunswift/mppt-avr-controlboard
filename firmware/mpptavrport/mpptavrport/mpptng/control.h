/* Run the control loop */ 

#ifndef __CONTROL__
#define __CONTROL__

//FIXME
#include "scandal_types.h"

void control_init();
void control_start(void);
void control_set_voltage(int32_t mvolts);	
void control_set_raw(int16_t raw);
int32_t control_get_target(void);
int control_is_saturated(void);

/*void*/ volatile int set_max_vout_adc(int32_t new_vout_adc);//uint16_t new_vout_adc);
/*void*/ volatile int set_min_vin_adc(int32_t new_vin_adc);//uint16_t new_vin_adc);
/*void*/ volatile int update_control_maxmin(void);

void adc_acc_read_and_zero(int i, uint32_t* value, uint16_t* num);
uint32_t adc_acc_read_zero_divide(int i);

//debug adc function to print
void adc_print( void );
//software output shutdown
volatile void uC_output_state(u08);

typedef struct pid_data_t {
	/* Variables */
  int32_t uk_1; /* Previous PI output */
  int32_t ek_1; /* Previous error */ 
  int32_t integral; /* Accumulated integral component */ 
} pid_data_t;

typedef struct pid_const_t {
        int32_t Kp; 
        int32_t Ki; 
        int32_t Kd; 
} pid_const_t; 

/* Active loop constants */ 
#define INPUT_LOOP      0
#define OUTPUT_LOOP     1

//ADC constants 
#define HIGH_BIT			0x03FC
#define FULL_BIT			0x03FF
#define LOW_BIT				0x0003
#define OFFSET				2
//#define OFFSET				8

extern volatile int     active_loop; 
extern volatile int32_t output; 
extern volatile int32_t control_error; /* Get rid of this! */ 

#endif
