/* Customised SPI functions for communicating with the FPGA on the 
    MPPTNG board. These have been hacked for speed */ 

#ifndef __OTHER_SPI__
#define __OTHER_SPI__

//FIXME for atmega
//#include <io.h>

#include "spi_devices.h"

/* Functions -- static inline for speed */
static inline uint8_t 
init_spi0(){
	
	//FIXME for atmega
	/*
    ME1 |= USPIE0;
  
    U0CTL  = SYNC+MM+CHAR;
    U0TCTL = STC | SSEL1|SSEL0 | CKPL ;// Not sure about the clock polarity 
    UBR00 = 0x04;
    UBR10 = 0x00;
    UMCTL0 = 0x00;

    DISABLE_FPGA_SPI();
	
	*/
    return(0); 
}

static inline void
spi0_send(uint8_t out_data){
	/*
    IFG1 &= ~URXIFG0;

    while((IFG1 & UTXIFG0) == 0)
        ;
  
    TXBUF0 = out_data;
	*/
}

static inline uint8_t 
spi0_transfer(uint8_t out_data){ 
    uint8_t	value; 
	/*

    IFG1 &= ~URXIFG0;

    while((IFG1 & UTXIFG0) == 0)
        ;
  
    TXBUF0 = out_data;

    while((IFG1 & URXIFG0) == 0)
        ;

    value = RXBUF0;
*/
    return(value); 
	
} 

#endif
