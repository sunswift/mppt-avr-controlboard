/*
 * main.c
 *
 *  Created on: Jan 18, 2013
 *      Author: tosis
 *			From:http://www.avrfreaks.net/index.php?name=PNphpBB2&file=viewtopic&t=125272
 */


/* -----------------------------------------------------------------------
 * Title:    Configuration USART Rx (8 bit + 1 stop bit 34800)
 * Hardware: ATmega68C1
 * Software: Eclipse
 -----------------------------------------------------------------------*/

#define F_CPU 16000000UL                  // Clock frequency
//Includes ...Maybe you need to change this path!!!
// #include <avr/io.h>				// Define Port names for atmega64m1
// 
// // #include <io.h>
//  #include "util/delay.h"
//  //#include "avr/delay.h"
//  #include "stdlib.h"
//  
//  #include <avr/interrupt.h>
//  #include "stdio.h"
#include "uart.h"

#define BAUD 9600//38400                        // Baud rate


// ********************************* Initialisation USART *********************************

unsigned char String[] = "Hello world!!!";

void USART_Init( void)
{
LINCR = (1 << LSWRES);
LINBRRH = (((F_CPU/BAUD)/16)-1)>>8;
LINBRRL = (((F_CPU/BAUD)/16)-1);
LINBTR = (1 << LDISR) | (16 << LBT0);
LINCR = (1<<LENA)|(1<<LCMD2)|(1<<LCMD1)|(1<<LCMD0);
}


 //* ************************************* USART ATMEGA64C1 Tx**********************************
 int at64c1_transmit (unsigned char byte_data) {
    while (LINSIR & (1 << LBUSY));          // Wait while the UART is busy.
    LINDAT = byte_data;
    return 0;
}
 int at64c1_receive (void) {
     while (LINSIR & (1 << LBUSY));          // Wait while the UART is busy.

     return LINDAT;
 }
 FILE uart_str = FDEV_SETUP_STREAM(at64c1_transmit,at64c1_receive,_FDEV_SETUP_RW);

// ************************************* MAIN LOOP **************************************

int init_uart( void )
{
stdout = stdin = &uart_str;
USART_Init();                    // initialisation
//int turn=5;
//FIXME so that it doesnt re init everytime
//volatile int x;

//     for(;;)                               // super loop
//     {

/*        for (x = 0 ; x < 13 ; x++)   //If you uncomment this section you can see te message Hello World
        {

        at64c1_transmit(String[x]);                   // Send characters
        }


        at64c1_transmit(13);      */                    // Enter
//       turn++;  
// 			printf("%d\n",turn);
       // _delay_ms(500);
    //}

return 0;
} 