#ifndef __MPPTNG_ERROR_H__
#define __MPPTNG_ERROR_H__

void mpptng_error(int error); 
void mpptng_fatal_error(int error); 
void mpptng_do_errors(void);

#endif
