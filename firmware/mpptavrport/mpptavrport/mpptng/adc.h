/* get interrupts from ADC input 9 */
#define ADC_INTERRUPT_ENABLE() (ADC12IE |= (1 << 9))
#define ADC_INTERRUPT_DISABLE() (ADC12IE &= ~(1 << 9))
