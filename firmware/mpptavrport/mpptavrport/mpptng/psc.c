/*
 * CProgram1.c
 *
 * Created: 8/06/2013 12:02:22 AM
 *  Author: william
 */ 
#include <avr/io.h>				// Define Port names for atmega64m1
#include <avr/interrupt.h> // Interrupt headers 

#include "scandal_types.h" // typedefs

#include "psc.h"
#include "psc_drv.h"			// Macros for PSC, Start_pll, Stop_pll

#include "mpptng.h" // PWM defines

//Uart debug stuff
#include "uart.h"		//macros and definitions for UART module

//Prototypes (not in psc.h)
//static inline void PSC_Enable(u08 on); //enable the output buffers on the PSC

volatile uint32_t value_copy;

static inline void 
PSC_transfer(u08 signal, u16 value); //choose signal and PWM value


static inline void 
PSC_transfer(u08 signal, u16 value){
	//FIXME use value to determine/ calculate SA, RA, SB
	if(signal == SIGNAL_RESTART ) {//| (signal == SIGNAL_PWM & value == 0 )) {
		Psc_set_register_RB(value); //load frequency register
		
		//FIXME: zero out the modules...
		Psc_set_module_C(C_SA_VAL_DEFAULT,C_RA_VAL_DEFAULT,C_SB_VAL_DEFAULT);	
		Psc_set_module_B(B_SA_VAL_DEFAULT,B_RA_VAL_DEFAULT,B_SB_VAL_DEFAULT);
		Psc_set_module_A(A_SA_VAL_DEFAULT,A_RA_VAL_DEFAULT,A_SB_VAL_DEFAULT);
	
	
	} else if	(signal == SIGNAL_PWM) {	
		//use 'value' and calculate SA, RA, SB
		//calculate PSC stuff.
		///////////////////////////////////////////////////////////////////////////////////////It's so hot in here. Why do I even have pants on?
		
		//if (value < PWM_MIN) { //FIXME clean this up!!
			//Psc_set_module_C(C_SA_VAL_DEFAULT,C_RA_VAL_DEFAULT,C_SB_VAL_DEFAULT);	
		//	Psc_set_module_B(B_SA_VAL_DEFAULT,B_RA_VAL_DEFAULT,B_SB_VAL_DEFAULT);
			//Psc_set_module_A(A_SA_VAL_DEFAULT,A_RA_VAL_DEFAULT,A_SB_VAL_DEFAULT);
		//} else {
		//Psc_set_module_A(A_SA_VAL_DEFAULT,PWM_MAX-value,A_SB_VAL_DEFAULT); //Diode (0, MAX-Mainwidth, period)
		//Diode (0, MAX-Mainwidth, period)

		Psc_set_module_B(PWM_MAX-value-DA_OVERLAP_WIDTH+AUX_WIDTH-AM_OVERLAP_WIDTH,PWM_MAX-DA_OVERLAP_WIDTH+AUX_WIDTH-AM_OVERLAP_WIDTH-OFFSET_PWM,B_SB_VAL_DEFAULT); 
		//MAIN (Mainwidth, End, Period)
		//stuffed truncated integer to unsigned type
		
		//Psc_set_module_C(PWM_MAX-value-DA_OVERLAP_WIDTH,PWM_MAX-value-DA_OVERLAP_WIDTH+AUX_WIDTH,C_SB_VAL_DEFAULT); 
		//Aux
		//}
		
	} else if	(signal == SIGNAL_AUX_LENGTH) { //set signal to default	
		//use value and calculate SA, RA, SB
		Psc_set_module_C(C_SA_VAL_DEFAULT,C_RA_VAL_DEFAULT,C_SB_VAL_DEFAULT);
		
  } else if	(signal == SIGNAL_MAIN_LENGTH) {	//set signal to default	
		Psc_set_module_B(B_SA_VAL_DEFAULT,B_RA_VAL_DEFAULT,B_SB_VAL_DEFAULT);
		
  } else if	(signal == SIGNAL_DIODE_LENGTH) {	//set signal to default	
		Psc_set_module_A(A_SA_VAL_DEFAULT,A_RA_VAL_DEFAULT,A_SB_VAL_DEFAULT); //load register with deadtime, on, off times -> 12 bit value
  
  }	
  

}


void PSC_Init (void)
{
   //Implement this functionality 
   //Usage: signal parameter, value
		//SIGNAL_RESTART, DEFAULT_RESTART)
		//SIGNAL_AUX_LENGTH, DEFAULT_AUX_LENGTH
		//SIGNAL_AUX_OVERLAP, DEFAULT_AUX_OVERLAP);
		
		//Removed the enable cos thats crazy.
	
		//these should be set to default values (minimums) first, then enabled.
		///////////////////////////////////////////////////////////////////////////////////////default clothes value = 0. Only increase under extreme cold weather conditions. 
		//////////////////////////////////////////////////////////////////////////////////////under NO circumstance should pants be on.  
	
	PSC_transfer(SIGNAL_RESTART, DEFAULT_FREQUENCY);
	PSC_transfer(SIGNAL_AUX_LENGTH, DEFAULT_AUX);
	PSC_transfer(SIGNAL_MAIN_LENGTH, DEFAULT_MAIN);
	PSC_transfer(SIGNAL_DIODE_LENGTH, DEFAULT_DIODE);


  Psc_config(); //psc config outputs
  Psc_run(); //run psc

}

void PSC_print(void) 
{
	#if UART_DEBUG >= 1
	printf("PSC SET OUT: PWM value = %d\r\n",value_copy);
	#endif
}

void PSC_Reset(void) {
	
	//FIX ME. Reset the PSC!!
	
	//Shutdown all signals, set them to appropiate default values. 
	//Aux: off, logic 0
	//Main: off, logic 0
	//Diode: off, logic 0 -> buffer may have to be logic '1'
	//
	PSC_Init();
	
}
// error checking
void PSC_Setpwm(int32_t value){
    if(value > PWM_MAX-1) {
        value = PWM_MAX-1;
    } else if(value < PWM_MIN) {
        value = PWM_MIN; 
	}	
		value_copy = value;
		PSC_transfer(SIGNAL_PWM, value); 
    //fpga_transfer(1, SIGNAL_PWM, value); 
}

//static 
inline void PSC_Enable(u08 mode){
	
	if(mode == PSC_ON) {
		//P2OUT |= FPGA_ENABLE;
		//Set the PSC enable flag
			//need a way to enable outputs after.
		///////////////////////////////////////////////////////////////////////////////////////Why is it socially unacceptable to have no pants on? :(
		Psc_enable_psc_A_outputs(); // ENABLE output for 3 PSC channels

	}	else if (mode == PSC_OFF) {
    //P2OUT &= ~FPGA_ENABLE; //disable
		// Clear the PSC enable flag
		Psc_clear_outputs();
	}	
}

