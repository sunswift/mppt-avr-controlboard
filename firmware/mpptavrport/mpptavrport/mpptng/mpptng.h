/* Copyright (C) David Snowdon, 2009 <scandal@snowdon.id.au> 
   Copyright (C) William Widjaja, 2015
*/ 

/* 
 * This file is part of the UNSWMPPTNG firmware.
 * 
 * The UNSWMPPTNG firmware is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * The UNSWMPPTNG firmware is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 
 * You should have received a copy of the GNU General Public License
 * along with the UNSWMPPTNG firmware.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __MPPTNG__
#define __MPPTNG__

#include "control.h"
#include "psc.h"

//Channel defs
//UNSWMPPTNG_IN_VOLTAGE				0
//UNSWMPPTNG_IN_CURRENT				1
//UNSWMPPTNG_OUT_VOLTAGE			2
//UNSWMPPTNG_15V	               3
//UNSWMPPTNG_HEATSINK_TEMP			4
//UNSWMPPTNG_AMBIENT_TEMP			5
//Status                         6
//Sweep in voltage               7
//Sweep out voltage              8
//Pando power                    9


/* For use as absolute max/min in the control loop */ 
//Tuned for the utracker.
#define VOUT_BOTTOM_RESISTANCE (1.0 / ( (1.0 / 249000.0) + (1.0 / (680000.0 + 10000.0))))// 249k power, vout amp 10k, 680k pull down.
#define VOUT_TOP_RESISTANCE 2*8060000

#define VIN_BOTTOM_RESISTANCE 1000000
#define VIN_TOP_RESISTANCE 2*8060000

//based on controller
#define REF_VOLTAGE      (3.0) //(2.5) //MSP reference maxed before ABS_MAX_VOUT
#define ADC_BITS         (10) //(12)
#define ADC_READING(x)   ((((float)x) / REF_VOLTAGE) * ((float)(1<<ADC_BITS))) //scale voltage to register value

#define VOUT_TO_ADC(x)   (int32_t)ADC_READING( ((float)x) *VOUT_BOTTOM_RESISTANCE/(VOUT_BOTTOM_RESISTANCE + VOUT_TOP_RESISTANCE)) //output divider//(uint16_t)ADC_READING( ((float)x) *VOUT_BOTTOM_RESISTANCE/(VOUT_BOTTOM_RESISTANCE + 3000000.0)) //output divider
#define VIN_TO_ADC(x)    (int32_t)ADC_READING( ((float)x) *VIN_BOTTOM_RESISTANCE/(VIN_BOTTOM_RESISTANCE + VIN_TOP_RESISTANCE))  //(uint16_t)(ADC_READING((float)x * 47.0 / (47.0 + 3000.0)))

//based on controller
#define V15V_BOTTOM_RESISTANCE 220000
#define V15V_TOP_RESISTANCE 1500000

#define FIFTEEN_TO_ADC(x) (uint16_t)(x) /* FIXME! */ 
#define TEMP_TO_ADC(x)    (uint16_t)(x) /* FIXME! */ 
//Iin to ADC?

#define UART_DEBUG			1	//1

/* Tracker status */ 
#define STATUS_TRACKING         BIT(0)
#define STATUS_INPUT_LOOP       BIT(1)   /* Input loop active -- control.c */ 
#define STATUS_OUTPUT_LOOP      BIT(2)   /* Output loop active -- control.c */ 

/* Tracking algorithms */ 
#define MPPTNG_OPENLOOP        0
#define MPPTNG_PANDO           1
#define MPPTNG_INCCOND         2
#define MPPTNG_IVSWEEP         3
#define MPPTNG_MANUAL          4
#define MPPTNG_NUM_ALGORITHMS  5

/* Absolute limits of the tracker */ 
#define ABS_MAX_VOUT     120000//160000		//in mV
#define ABS_MIN_VIN      5000//10000 // hack for debug
#define ABS_MAX_VIN      50000//150000//30000//150000 //debug
#define ABS_MIN_15V      12000  //utracker mode?
#define ABS_MAX_HS_TEMP  100000
#define ABS_MAX_VOUT_OVERSHOOT 1000

#define DEFAULT_MAX_VOUT 100000 // only for debug - overridden in control loop
#define DEFAULT_MIN_VIN  15000//11000 //17000 // hack for debug 
#define DEFAULT_ALGORITHM MPPTNG_PANDO//MPPTNG_OPENLOOP
#define DEFAULT_IN_KP    7000//14000//with N = 11 //1000000//-1000000//7000 // OLD:7000 //settings dont appear to work
#define DEFAULT_IN_KI    10//10//400 
#define DEFAULT_IN_KD    0//0

//Tuned Output first (turn off input control loop) - Working with test board (not MPPT)
#define DEFAULT_OUT_KP   -10000// (1) Choose a Kp
#define DEFAULT_OUT_KI   -20// increase Ki until low enought output error. Too high = instability
#define DEFAULT_OUT_KD   -5//-10

//Manually tuned
/*
#define DEFAULT_OUT_KP   -500//-960 // WORKING? super high resolution (based off e(k) = -1023)//-900000 //crappy resolution based off e(k) = -1//-10000//-14000//-10000 //with N = 11//-1000000//-1000000//-100000//-50000//-5000 not working//-500000 not working//-800000//-1500000 works around 5V//-1000000 working at 27Vout, Vout control loop//1-1//100000//-10000
#define DEFAULT_OUT_KI   -50//-30 //-5//-10//-10//-30//0 does not work
#define DEFAULT_OUT_KD   -200//-10
*/
/* Tracking algorithm stuff */ 
#define DEFAULT_OPENLOOP_RATIO  800
#define DEFAULT_OPENLOOP_RETRACK_PERIOD 10000
#define DEFAULT_PANDO_INCREMENT VIN_TO_ADC(0.5)
#define DEFAULT_IVSWEEP_STEP_SIZE 300 // units? mV?
#define DEFAULT_IVSWEEP_SAMPLE_PERIOD 30
 
/* Frequency constants */ 
#define CONTROL_FS       1160L // seem to be unused
#define twoFs            (2 * 1160)

/* Other constants */ 
#define TELEMETRY_UPDATE_PERIOD  800 /* Note: must be less than 1s, 
					because the watchdog timer must
					be kicked at least every second */ 

/* Default settings */ 
//units of these parameters? what is the resolution??
// not ms, could be us -> 32.74/us
#define DEFAULT_PWM          0
#define DEFAULT_AUX_LENGTH   55 // aux length = 1.680us
#define DEFAULT_AUX_OVERLAP  8 //aux main overlap = 70ns -> =0.244us??
#define DEFAULT_DEADTIME     25 //main - diode deadtime = 
#define DEFAULT_RESTART      1300

/* PWM constants */ 
//#define PWM_MAX              ((DEFAULT_RESTART - DEFAULT_AUX_LENGTH) * 8 / 10) // = 1300-55*8/10 = 996

//FIXME: need proper calcs 
#define PWM_MAX              960//3000//2800//3000 //2474//(3200-107)*8/10 //2474//(uint16_t)((RB_VAL - AUX_WIDTH) * 8 / 10) // = 2474
#define PWM_MIN              0 //1000

/* Default scaling factors - should be re-calibrated */ 

//COnfig: x,b , (0)
//Atmega initial calibration
//set all scaling values to 1:1 linear, no offset 
#define DEFAULT_VIN_M                  49// umppt  //185//working for drivetek   1//703//40522 
#define DEFAULT_VIN_B                  587//913//813//-370615

//(1)
#define DEFAULT_IIN_M                  9//1//9//7//2580
#define DEFAULT_IIN_B                  10//0//184//84//10//-8000

//(2)
#define DEFAULT_VOUT_M                 173//168//198//185//1//43376  
#define DEFAULT_VOUT_B                 3500//2500//1880//740//404//-1401352

//(3)
#define DEFAULT_15V_M                  15//15//16//1//5208
#define DEFAULT_15V_B                  698//-82//698//98//414//0//-526772
//rough calcs: vin scales by 185, vout scales by 198. Div ratio was 47k/3.047k = 1/64.
//15V:1.5M/220k= 1/6.8 Guess M = 17

//Not implemented.
//(4)
#define DEFAULT_HEATSINK_TEMP_M        1//425354         /* FIXME: VERY rough and doesn't work */ 
#define DEFAULT_HEATSINK_TEMP_B        0//-45371077

//Thermally laggy calibrated with K type probe and heat gun.
//Accurate to 40 to 100C Very accurate around 80C.
// 1499000
// -110705625


//(5)
#define DEFAULT_AMBIENT_TEMP_M         1//1024
#define DEFAULT_AMBIENT_TEMP_B         0//0

/* Other constants */ 
#define OUTPUT_LOOP_CHANGEOVER_HYSTERESIS    2000     /* 2V */ 

/* Rough limits defined in terms of the default scaling values */ 
#define ADC_ABS_MAX_VOUT			VOUT_TO_ADC(ABS_MAX_VOUT / 1000.0) // (838)
#define ADC_ABS_MIN_VIN				VIN_TO_ADC(ABS_MIN_VIN / 1000.0)	//(52)
#define ADC_ABS_MAX_VIN				VIN_TO_ADC(ABS_MAX_VIN / 1000.0)
#define ADC_ABS_MIN_15V				2000 /* 12.0V  -- NOT CORRECT, NEEDS UPDATE */
#define ADC_ABS_MAX_HS_TEMP			3000 /* 100 Degrees -- NOT CORRECT, NEEDS UPDATE */
#define ADC_OUTPUT_LOOP_CHANGEOVER_HYSTERESIS   100     /* 2V -- NOT CORRECT, NEEDS UPDATE */ 


typedef struct mpptng_config{
  int32_t max_vout; 
  int32_t min_vin; 
  uint8_t algorithm;

  /* Control loop parameters */ 
  pid_const_t   in_pid_const; 
  pid_const_t   out_pid_const; 

  /* PV tracking parameters */ 
  uint16_t openloop_ratio; 
  uint16_t pando_increment; 
  uint16_t openloop_retrack_period; 
  uint16_t ivsweep_sample_period; 
  uint16_t ivsweep_step_size; 
  
  /* Checksums */ 
  uint8_t magic; 
  uint8_t checksum; 
  uint8_t checkxor; 
}mpptng_config_t; 

extern volatile mpptng_config_t config; 
extern volatile int tracker_status; 

#endif
