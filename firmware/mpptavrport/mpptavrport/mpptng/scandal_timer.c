/*! ------------------------------------------------------------------------- 
    \file scandal_timer.c
        Scandal Timer for AVR as used in mpptng
	 
	File name: scandal_timer.c 
	Author: William Widjaja
	Date: 12/6/13
    -------------------------------------------------------------------------- */  


/*! ------------------------------------------------------------------------- 
    \file scandal_timer.c
        Scandal Timer for AVR as used in CANRefNode
	 
	File name: scandal_timer.c 
	Author: David Snowdon 
	Date: 1/7/03
    -------------------------------------------------------------------------- */  

//FIXME

#include <avr/io.h>				// Define Port names for atmega64m1
#include <avr/interrupt.h> // Interrupt headers 

//#include <io.h>
//#include <signal.h>

#include "scandal_timer.h"
#include "scandal_led.h"

#define TIMER1_MATCH 15625 //match value for interrupt on timer 1

static volatile u32 ms; // true ms 
static volatile u32 timer1_count; // counter for timer 1 ms

/* Interrupt handler associated with internal RTC */
/* Timer 1 overflow interrupt */
		
		//FIXME for atmega ->dont forget interrupt header

ISR(TIMER1_COMPA_vect){   // handler for Timer 1 compare interrupt
	timer1_count += 1;
	if (timer1_count >= TIMER1_MATCH) {
		ms += 1000; // increment time in ms
		timer1_count = 0;
		//toggle_red_led(); //also toggled for pv tracking
		//clear timer register
// 		TCNT1H = 0x00;				//initialise timer higher bits
// 		TCNT1L = 0x00;				//initialise timer lower bits
		//TCNT1 = 0x0000;
	}
		
	//automatically clears flag
}
		
		
		/*
interrupt (TIMERA0_VECTOR) timera_int(void) {
  ms += 1000;
}
		*/

//FIXME for atmega Timer 0 is 8 bit only swap over to Timer 1 16 bit
//timer in ms since start up.
void sc_init_timer(void){
  /* Set ms to zero */
  ms = 0;
  timer1_count = 0;
  //Timer1 setup 16 bit timer.
  
  //Runs at 8Mhz, Prescaler 1, Compare Match 512, Freq = 15625Hz,
  //ms is incremented every 15625 interrupts. Would be better if a power of 2 interrupts was achieved
  // 1ms = 512*15625/8Mhz/1000
     
// 	TCCR1A = 0b00000000;	// COMnA1:0 = 0b00 -> Normal port operation, OCnA/OCnB disconnected.
// 												// COMnB1:0 = 0b00 -> Normal port operation, OCnA/OCnB disconnected.
// 												// WGMn1:0 = 0b00  -> clear timer on compare match 
// 	
// 	TCCR1B = 0b00001001;	// ICNCn: = 0				-> no filter, 
// 												// ICESn: = 0				->falling edge trigger,  
// 												// RTGEN = 0				-> for compatability, dont set it to retrigger 
// 												// WGMn3:2 = 0b01		-> clear timer on compare match OCR1A
// 												// CSn2:0 = 0b001		-> prescaler 1 , internal 8MHz clock
// 										
// 											
// 	TCCR1C = 0x00;				// FOCnA: = 0
// 												// FOCnB: = 0;
// 												
// 	TCNT1H = 0x00;				//initialise timer higher bits
// 	TCNT1L = 0x00;				//initialise timer lower bits
// 	
// 	OCR1AL = 0xFF; //0x00;//0x0F;				//set output compare value to 2^16  = 65536 -> 8mhz/512 = 15625Hz
// 	OCR1AH = 0x01;
// 	
// 	TIMSK1  = 0b00000010; // ICIE1: 0		-> Timer/Counter1 Input Capture interrupt is disabled.
// 												// OCIE1B: 0	-> Timer/Counter1 Output Compare B Match interrupt is disabled.
// 												// OCIE1A: 1	-> Timer/Counter1 Output Compare A Match interrupt is enabled 
// 												// TOIE1: 0		-> Timer 1 overflow interrupt disable
												
	//Runs at 16Mhz, Prescaler 1, Compare Match 1024, Freq = 15625Hz,
  //ms is incremented every 15625 interrupts. Would be better if a power of 2 interrupts was achieved
  // 1ms = 512*15625/8Mhz/1000
     
	TCCR1A = 0b00000000;	// COMnA1:0 = 0b00 -> Normal port operation, OCnA/OCnB disconnected.
												// COMnB1:0 = 0b00 -> Normal port operation, OCnA/OCnB disconnected.
												// WGMn1:0 = 0b00  -> clear timer on compare match 
	
	TCCR1B = 0b00001001;	// ICNCn: = 0				-> no filter, 
												// ICESn: = 0				->falling edge trigger,  
												// RTGEN = 0				-> for compatability, dont set it to retrigger 
												// WGMn3:2 = 0b01		-> clear timer on compare match OCR1A
												// CSn2:0 = 0b001		-> prescaler 1 , 16MHz or 8MHz clock
										
											
	TCCR1C = 0x00;				// FOCnA: = 0
												// FOCnB: = 0;
// 												
// 	TCNT1H = 0x00;				//initialise timer higher bits
// 	TCNT1L = 0x00;				//initialise timer lower bits
	TCNT1 = 0x0000;
	
	//For 16Mhz clock (PLL 64MHz/4)
// 	OCR1AL = 0x00; //400;				//set output compare value to 2^16  = 65536 -> 16Mhz/1024 = 15625Hz
// 	OCR1AH = 0x02; //??? dosent see high register
	OCR1A = 0x0400;
// 	//For 8Mhz clock (int RC)
// 	OCR1AL = 0xFF; //0x00;//0x0F;				//set output compare value to 2^16  = 65536 -> 8mhz/512 = 15625Hz
// 	OCR1AH = 0x01;
	
	TIMSK1  = 0b00000010; // ICIE1: 0		-> Timer/Counter1 Input Capture interrupt is disabled.
												// OCIE1B: 0	-> Timer/Counter1 Output Compare B Match interrupt is disabled.
												// OCIE1A: 1	-> Timer/Counter1 Output Compare A Match interrupt is enabled 
												// TOIE1: 0		-> Timer 1 overflow interrupt disable
																							
		//MSP
  /* Clear counter, input divider /1, ACLK */
			//TACTL = /*TAIE |*/ TACLR | ID_DIV1 | TASSEL_ACLK;

  /* Enable Capture/Compare interrupt */
			//TACCTL0 = CCIE;
			//TACCR0 = 32767; /* Count 1 sec at ACLK=32768Hz */
  
  /* Start timer in up to CCR0 mode */
			//TACTL |= MC_UPTO_CCR0;
}

	//FIXME - not used in mpptng
	// set the timer to any time period/frequency.
	
void sc_set_timer(sc_time_t time){
  
  //TIMSK0 &= ~(1<<OCIE0A); //disable interrupt
  TIMSK1 &= ~(1<<OCIE1A); //disable interrupt
  
  //timing register needs to be high resolution for offset.
  //calculate offset to 16bit timing register
  //eg time = 10500ms. 
  // time %1000 = 500ms
  // /1000 = 0.5
  // << 15 = 0.5*32768 =  16384
  //
  // New time ms = 10
  //recalculate value of TACCR0 time
  
  //because of rounding effects with integers, write to timer1_count & TCNT1
  // ms = (time/1000)*1000
  //fractions_ms = (time%1000)/1000
  //software value fraction = (time%1000)/1000*15625
  //software value fraction = ((time%1000)*15625)/1000
 
  //register value fraction = ((((time%1000)*15625)%1000)*512)/1000;
	ms = (time/1000)*1000;
	timer1_count = ((time%1000)*15625)/1000;
  TCNT1 = ((((time%1000)*15625)%1000)*512)/1000;
  
  TIMSK1 |= (1<<OCIE1A); //enable timer 1 interrupt
  
  
//   TACCTL0 &= ~CCIE; // disable interrupt
//   TACCR0 = ((time % 1000) << 15) / 1000; //recalculate time, convert to ms,  
//   ms = time / 1000;
//   
//   TACCTL0 |= CCIE;
  
  
}

	
sc_time_t sc_get_timer(void){
  sc_time_t 	time;
  u32           timer1_reg_copy; // 9 bit max
	u32           timer1_copy; // 14bit +8 bit max
	
  /* Work out what the time in ms is */

  /* Turn off the timer interrupt */ 
			//FIXME
	TIMSK1 &= ~(1<<OCIE1A); //disable interrupt
	
			//TACCTL0 &= ~CCIE; //MSP timer A control

  /* Short delay so that we're sure the interrupt is off */ 
  //MSP
  {
    volatile int i; 
    for(i=0; i<15; i++)
      ;
  }

  /* Copy the relevant numbers */ 
  time = ms; // software current time before including register 
			//tar_copy = TAR; //copy timer A register
			
	timer1_reg_copy = TCNT1; //copy timer 1 register 16bit
	timer1_copy = timer1_count; //copy total time timer 1 software, 14bit max
	
  /* Turn the timer interrupt back on */ 
			// TACCTL0 |= CCIE; //MSP
		TIMSK1 |= (1<<OCIE1A); //enable timer 1 interrupt
	
// 		time += ((tar_copy * 1000) >> 15); //MSP  convert register A to milliseconds, then divide by 32768 to work out time in ms
	//ATmega:
		//remainder_in_ms = ratio_in_software + ratio_in_register_1
		//remainder_in_ms = (timer1_count/15625)*1000 + (TCNT1/512)/15625*1000	
		//Dividing as late as possible 'calculatable' 
		//remainder_in_ms = (timer1_count*1000 +(TCNT1*1000)/512)/15625

		time += ((timer1_copy*1000 +(timer1_reg_copy*1000)/512)/15625); //work out remainder in register

  return time;
}

//FIXME
void sc_enable_timer(void){
  		TIMSK1 |= (1<<OCIE1A); //enable timer 1 interrupt
}
