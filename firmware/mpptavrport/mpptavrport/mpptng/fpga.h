#ifndef __FPGA__
#define __FGPA__

#include "hardware.h"
#include "scandal_types.h"
#include "other_spi.h"
#include "spi_devices.h"
#include "mpptng.h"

#define FPGA_ON      1
#define FPGA_OFF     0

/* Signal definitions to be used in
   fpga_set_register(X, signal, X); */
#define SIGNAL_RESTART       0
#define SIGNAL_AUX_LENGTH    1
#define SIGNAL_AUX_OVERLAP   2
//#define SIGNAL_PWM           3
#define SIGNAL_DEADTIME      4

/* Prototypes */ 
void fpga_init(void);
static inline void fpga_transfer(u08 board, u08 signal, u16 value);

/* Static functions you should use */ 
static inline void 
fpga_setpwm(uint16_t value){ //uint16_t value
    if(value > PWM_MAX)
        value = PWM_MAX;
    else if(value < PWM_MIN)
        value = PWM_MIN; 

    fpga_transfer(1, SIGNAL_PWM, value); 
}

static inline void 
fpga_enable(u08 on){
	/*
	if(on == FPGA_ON)
		P2OUT |= FPGA_ENABLE;
	else
        P2OUT &= ~FPGA_ENABLE; 
		*/
}

/* FPGA_RESET is active low */ 
static inline void 
fpga_reset(void){
	/*
	P2OUT &= ~FPGA_RESET; 
	P2OUT |= FPGA_RESET; 
	*/
}

static inline void 
fs_reset(void){
	/*
	P2OUT |= FS_RESET; //toggle reset line 
	P2OUT &= ~FS_RESET; 
	*/
}

static inline uint8_t 
fpga_nFS(void){
	
	//return P2IN & FS; 
	return 0;
}


/* -------------------------------------------------- */ 
/* Static inline functions you shouldn't use directly, 
   but are here for speed */ 
/* -------------------------------------------------- */ 

static inline void 
fpga_transfer(u08 board, u08 signal, u16 value){
    //u16 transfer = 0; 
/*
	transfer |= ((u16)board & 0x03) << 14;
	transfer |= ((u16)signal & 0x07) << 11; 
	transfer |= (value & 0x07FF) << 0; 

    ENABLE_FPGA_SPI(); 
    spi0_transfer((transfer >> 8) & 0xFF); 
	spi0_transfer(transfer & 0xFF); 
	DISABLE_FPGA_SPI(); 

	spi0_transfer(0x00); */
}

#endif

