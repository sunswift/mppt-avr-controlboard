/*! ------------------------------------------------------------------------- 
   Photovoltaics tracking code.
    
   File name: pv_track.c
   Author: David Snowdon 
   Date: 4th June, 2005
   ----------------------------------------------------------------------- */ 


//FIXME
#include <avr/io.h>				// Define Port names for atmega64m1
#include <avr/interrupt.h> // Interrupt headers 

//#include <io.h>
//#include <signal.h>

#include "mpptng.h"
#include "hardware.h"
#include "control.h"
#include "pv_track.h"
#include "scandal_led.h"
#include "scandal_adc.h"
#include "scandal_utils.h"
#include "scandal_devices.h"
#include "scandal_message.h"

//Uart debug stuff
#include "uart.h"		//macros and definitions for UART module


/* Different pieces of data to be sent */ 
#define NO_DATA          0
#define IVSWEEP_DATA     1

volatile uint32_t pv_counter; 
volatile int      pv_algorithm;
volatile int      senddata_flag; 

sc_time_t         last_debug_data; 
sc_time_t         last_pvtrack_data; 

int32_t vin_raw;  
int32_t iin_raw;  

 int32_t output_control_set_raw_copy;
	int32_t output_V_copy_pando;

/* Prototypes */ 
static inline void pv_track(); 

static inline void pvtrack_openloop_start(void); 
static inline void pvtrack_openloop(void); 

static inline void pvtrack_pando_start(void);
static inline void pvtrack_pando(void); 

static inline void pvtrack_ivsweep_start(void);
static inline void pvtrack_ivsweep(void);

static inline void pvtrack_manual_start(void);
static inline void pvtrack_manual(void);

void pvtrack_debug(void);


/* Algorithm data storage */ 
volatile union {
  /* Data for the openloop algorithm */
  struct {
    int mode; /* Whether we are waiting for a sample or converting */ 
    int previous; /* Stores the previous measurement so we can work out the gradient */ 
  } openloop; 

  /* Data for the P & O algorithm */ 
  struct {
    int direction; 
    uint32_t lastpower; 
    int mode;
  } pando;

  /* Data for the Incremental conductance algorithm */ 
  struct {
    
  } inccond;

  /* Data for the IV sweep algorithm */ 
  struct { 
    int last_algorithm; 
    int32_t vin, iin; 
    int phase; 
  } ivsweep; 
} pvdata; 
 
 
 #define TIMER0_MATCH 125//15625 //match value for interrupt on timer 1

static volatile u32 timer0_count; // counter for timer 0 ms

//for uart printf 
uint64_t power_raw_copy;

/* Interrupt handler associated with internal RTC */
/* Timer 0 overflow interrupt */

//FIXME for atmega
//interupt period is too frequent 15k interrupts per seconds 


ISR(TIMER0_COMPA_vect){   // handler for Timer 0 compare interrupt
	timer0_count += 1;
	if (timer0_count >= TIMER0_MATCH) {
		pv_track(); 
		//toggle_CANRX_led();
		timer0_count = 0;
	}
		
	//automatically clears flag
}

/*
interrupt (TIMERB0_VECTOR) timerb0(void) {
  pv_track(); 
}
*/


void pv_track_init(void){
	//timer B for pv tracking?
	
	//FIXME
	
  //Timer0 setup 8 bit timer.
  //Runs at 8Mhz, Prescaler 8, Compare Match 128, Freq = 15625Hz,
  //ms is incremented every 15625 interrupts. Would be better if a power of 2 interrupts was achieved
  // 1ms = 512*15625/8Mhz/1000
  //What do
  
	TCCR0A = 0b00000010;	// COM0A1:0 = 0b00 -> Normal port operation 
												// COM0B1:0 = 0b00 -> Normal port operation 
												// WGM01:0 = 0b10  -> clear timer on compare match
	// 8MHz
  //TCCR0B = 0b00000010;	// FOC0A	 = 0		 -> must set to zero for compatability 
												// FOC0B	 = 0		 -> must set to zero for compatability
												// WGM02		= 0		 -> Normal
												// CS02:0: = 0b010 -> clkI/O/ (prescaling = 8) 8Mhz/8 = 1MHz
	// 16MHz
	TCCR0B = 0b00000101;	// FOC0A	 = 0		 -> must set to zero for compatability 
												// FOC0B	 = 0		 -> must set to zero for compatability
												// WGM02		= 0		 -> Normal
												// CS02:0: = 0b101 -> clkI/O/ (prescaling = 1024) 16Mhz/8 = 2MHz

	OCR0A = 0x7D;          // overflows every 16Mhz/1024/125 = 125Hz Compare value 8bit = 255 max.

	// Crappy 15kHzinterrupt   
  //OCR0A = 0x80;          // overflows every 16Mhz/8/128 = 15625Hz Compare value 8bit = 255 max.

  TCNT0 = 0x00;					// Initialise counter
  TIMSK0 = 0b00000010;  // OCIE0B: = 0			-> disable Timer/Counter Compare Match B interrupt 
												// OCIE0A: = 1			-> Timer/Counter0 Compare Match A interrupt is enabled
												// TOIE0: = 0					-> Timer/Counter0 Overflow interrupt enable
	
	//MSP
  /* Clear counter, input divider /1, ACLK */
			//TBCTL = /*TBIE |*/ TBCLR | ID_DIV1 | TBSSEL_ACLK;

  /* Enable Capture/Compare interrupt */
			//TBCCTL0 = CCIE;
			//TBCCR0 = 32768 / PV_HZ; /* Count 1/PV_HZ sec at ACLK=32768Hz */
  
  /* Start timer in up to CCR0 mode */
			//TBCTL |= MC_UPTO_CCR0;

  /* Initialise variables */ 
  pv_counter = 0; 
  
  /* Initialise with default algorithm */ 
  pv_track_switchto(config.algorithm); 

  /* Initialise the send data flags */ 
  senddata_flag = NO_DATA; 
  last_debug_data = sc_get_timer(); 
  last_pvtrack_data = sc_get_timer(); 
}



void pv_track_switchto(int algorithm){
  switch(algorithm){
  case MPPTNG_PANDO:
    pvtrack_pando_start(); 
    break; 

  case MPPTNG_INCCOND:
    break; 

  case MPPTNG_IVSWEEP:
    pvtrack_ivsweep_start(); 
    break; 
    
  case MPPTNG_MANUAL:
    pvtrack_manual_start(); 
    break; 
    
  case MPPTNG_OPENLOOP:
  default:
    pvtrack_openloop_start(); 
    break; 
  }

  pv_algorithm = algorithm; 
}

static inline void pv_track(void){
	toggle_yellow_15V_led();
  vin_raw = adc_acc_read_zero_divide(MEAS_VIN1);
  iin_raw = adc_acc_read_zero_divide(MEAS_IIN1);

  switch(pv_algorithm){
  case MPPTNG_PANDO:
    pvtrack_pando(); 
    break; 

  case MPPTNG_INCCOND:
    break; 

  case MPPTNG_IVSWEEP:
    pvtrack_ivsweep(); 
    break; 

  case MPPTNG_MANUAL:
    pvtrack_manual(); 
    break; 

  case MPPTNG_OPENLOOP:
  default:
    pvtrack_openloop(); 
    break;

  }
}

void pv_track_send_telemetry(void){
  scandal_send_scaled_channel(TELEM_LOW, UNSWMPPTNG_IN_VOLTAGE, vin_raw);
  scandal_send_scaled_channel(TELEM_LOW, UNSWMPPTNG_IN_CURRENT, iin_raw);  
}

void pv_track_send_data(void){
  sc_time_t thetime = sc_get_timer();
  if(thetime > last_pvtrack_data + TELEMETRY_UPDATE_PERIOD){
    last_pvtrack_data = thetime; 
    scandal_send_channel(TELEM_LOW, UNSWMPPTNG_PANDO_POWER, \
			 pvdata.pando.lastpower); // send scandal message, timestamp
  }

  //#define DEBUG
//#ifdef DEBUG
#if DEBUG >= 1
printf("DEBUG BEFORE timer\n");

  if(sc_get_timer() > last_debug_data + TELEMETRY_UPDATE_PERIOD){
	  printf("DEBUG AFTER timer IF\n");
	  {volatile int j; 
		for(j=70000; j>0; j--)
				; 
		}
    scandal_send_channel(TELEM_LOW, 141, control_get_target()); 
	{volatile int j; 
		for(j=70000; j>0; j--)
				; 
		}
    scandal_send_channel(TELEM_LOW, 142, config.out_pid_const.Kp); 
	{volatile int j; 
		for(j=70000; j>0; j--)
				; 
		}
    scandal_send_channel(TELEM_LOW, 143, config.out_pid_const.Ki); 
	{volatile int j; 
		for(j=70000; j>0; j--)
				; 
		}
    scandal_send_channel(TELEM_LOW, 144, config.out_pid_const.Kd);
	{volatile int j; 
		for(j=70000; j>0; j--)
				; 
		}
    scandal_send_channel(TELEM_LOW, 148, ADC12MEM3);
	{volatile int j; 
		for(j=70000; j>0; j--)
				; 
		}
    scandal_send_channel(TELEM_LOW, 145, output); 
	{volatile int j; 
		for(j=70000; j>0; j--)
				; 
		}
    scandal_send_channel(TELEM_LOW, 146, control_error); 
	{volatile int j; 
		for(j=70000; j>0; j--)
				; 
		}
    scandal_send_channel(TELEM_LOW, 147, pv_algorithm); 
	{volatile int j; 
		for(j=70000; j>0; j--)
				; 
		}
	
    last_debug_data = sc_get_timer(); 
  }
#endif

  if(senddata_flag == IVSWEEP_DATA){
    scandal_send_channel(TELEM_HIGH, 
			 UNSWMPPTNG_SWEEP_IN_VOLTAGE, 
			 pvdata.ivsweep.vin); 

    scandal_send_channel(TELEM_HIGH, 
			 UNSWMPPTNG_SWEEP_IN_CURRENT, 
			 pvdata.ivsweep.iin); 
    senddata_flag = 0; 
  }
}

static inline void pvtrack_openloop_start(void){
  pvdata.openloop.mode = OL_SAMPLING;  /* Start out by taking a sample */ 
  pv_counter = 0;                      /* Start counter again */ 
  control_set_voltage(ABS_MAX_VIN);    /* Set the control loop to the absolute maximum input V */ 
}

//every period, checks the open circuit voltage and holds the input to a ratio of it.
static inline void pvtrack_openloop(void){
  switch(pvdata.openloop.mode){
  case OL_SAMPLING:
    if(pv_counter++ >= OL_SAMPLING_COUNT){
      scandal_get_scaled_value(UNSWMPPTNG_IN_VOLTAGE, &vin_raw);               /* Scale the voltage to the real values */ 
      control_set_voltage(((int32_t)config.openloop_ratio * vin_raw) / 1000);  /* Set the output voltage */ 

      pvdata.openloop.mode = OL_CONVERTING;                                    /* Get ready for the next sample */ 

      pv_counter = 0; 
    }
    break; 

  case OL_CONVERTING:
  default: 
    if((pv_counter++) > config.openloop_retrack_period){
      control_set_voltage(ABS_MAX_VIN); 
      pvdata.openloop.mode = OL_SAMPLING; 
      pv_counter = 0; 
    }
    //toggle_red_led(); //uncomment later for debug
    break; 
  }
}




static inline void pvtrack_pando_start(void){
  pvdata.pando.mode = PANDO_SAMPLING;  /* Start out by taking a sample */ 
  pv_counter = 0;                      /* Start counter again */ 
  pvdata.pando.direction = config.pando_increment;          /* Start positively. :-). */ 
  pvdata.pando.lastpower = 0;          /* This will get update on our first trip through the loop */ 

  control_set_voltage(ABS_MAX_VIN);    /* Set the control loop to the absolute maximum input V */ 
}

static inline void pvtrack_pando(void){
  uint64_t power_raw;

  switch(pvdata.pando.mode){
  case PANDO_SAMPLING:
    if((pv_counter++) >= PANDO_SAMPLE_COUNT){
      scandal_get_scaled_value(UNSWMPPTNG_IN_VOLTAGE, &vin_raw);               /* Scale the voltage to the real values */ 
      control_set_voltage(((int32_t)config.openloop_ratio * vin_raw) / 1000);  /* Set the output voltage */ 
      
			output_V_copy_pando = ((int32_t)config.openloop_ratio * vin_raw) / 1000;  /* Set the output voltage */
      pvdata.pando.mode = PANDO_TRACKING;                                    /* Get ready for the next sample */ 

      pv_counter = 0; 
    }
    break; 

  case PANDO_TRACKING:
    if((pv_counter++) >= PANDO_UPDATE_COUNT){
      power_raw = (uint64_t)vin_raw * (uint64_t)iin_raw; 
      
			power_raw_copy = power_raw;
      /* If we got less power than last time, switch directions */ 
      if(power_raw < pvdata.pando.lastpower)
	pvdata.pando.direction = -pvdata.pando.direction; 
      
      /* Take an offset from the present value */ 
      control_set_raw(vin_raw + pvdata.pando.direction);
      output_control_set_raw_copy = vin_raw + pvdata.pando.direction;
      pvdata.pando.lastpower = power_raw; 
      
      //toggle_red_led(); //also toggled LED for scandal 1s timer
      pv_counter = 0;
    } 
    break; 
  }
}



void pvtrack_uart(void) {
	
#if UART_DEBUG
	printf("PV_TRACKING PANDO voltages: vin_raw = %d, iin_raw = %d, power_raw = %d\r\n", vin_raw, iin_raw, power_raw_copy);
	printf("PV_TRACKING PANDO voltages: output_control_set_raw_copy = %d, output_V_copy_pando = %d\r\n", output_control_set_raw_copy, output_V_copy_pando);
#endif
		
	
}

 void pvtrack_debug(void) {
// 	printf("Inside debug print \r\n");
// 	//#if DEBUG >= 1
// 	printf("DEBUG BEFORE timer");
// 
// 	  //if(sc_get_timer() > last_debug_data + TELEMETRY_UPDATE_PERIOD){
// 		  printf("DEBUG AFTER timer IF");
// 		  {volatile int j; 
// 			for(j=70000; j>0; j--)
// 					; 
// 			}
		scandal_send_channel(TELEM_LOW, 141, control_get_target()); 
		{volatile int j; 
			for(j=70000; j>0; j--)
					; 
			}
		scandal_send_channel(TELEM_LOW, 142, config.out_pid_const.Kp); 
		{volatile int j; 
			for(j=70000; j>0; j--)
					; 
			}
		scandal_send_channel(TELEM_LOW, 143, config.out_pid_const.Ki); 
		{volatile int j; 
			for(j=70000; j>0; j--)
					; 
			}
		scandal_send_channel(TELEM_LOW, 144, config.out_pid_const.Kd);
		{volatile int j; 
			for(j=70000; j>0; j--)
					; 
			}
		//scandal_send_channel(TELEM_LOW, 148, ADC12MEM3);
		{volatile int j; 
			for(j=70000; j>0; j--)
					; 
			}
		scandal_send_channel(TELEM_LOW, 145, output); 
		{volatile int j; 
			for(j=70000; j>0; j--)
					; 
			}
		scandal_send_channel(TELEM_LOW, 146, control_error); 
		{volatile int j; 
			for(j=70000; j>0; j--)
					; 
			}
 		scandal_send_channel(TELEM_LOW, 147, pv_algorithm); 
		{volatile int j; 
			for(j=70000; j>0; j--)
					; 
			}
		scandal_send_channel(TELEM_LOW, 148, config.in_pid_const.Kp); 
		{volatile int j; 
			for(j=70000; j>0; j--)
					; 
			}
		scandal_send_channel(TELEM_LOW, 149, config.in_pid_const.Ki); 
		{volatile int j; 
			for(j=70000; j>0; j--)
					; 
			}
		scandal_send_channel(TELEM_LOW, 150, config.in_pid_const.Kd);
		{volatile int j; 
			for(j=70000; j>0; j--)
					; 
			}	
			
// 	
// 		last_debug_data = sc_get_timer(); 
// 	  //}
// 	//#endif	
 }

static inline void pvtrack_inccond(void){
  
}

static inline void pvtrack_ivsweep_start(void){
  /* Don't start a new IV sweep mid-sweep */ 
  if(pv_algorithm != MPPTNG_IVSWEEP){
    /* set the phase to settling */ 
    pvdata.ivsweep.phase = IVSWEEP_PHASE_SETTLE; 

    /* Record what the previous algorithm was */ 
    pvdata.ivsweep.last_algorithm = pv_algorithm; 
    control_set_voltage(ABS_MAX_VIN);
    pv_counter = 0; 
  }
}

static inline void pvtrack_ivsweep(void){
  switch(pvdata.ivsweep.phase){
  case IVSWEEP_PHASE_SETTLE:
    if(pv_counter > PVTRACK_PERIOD_TO_COUNT(IVSWEEP_SETTLE_MS)){
      pvdata.ivsweep.phase = IVSWEEP_PHASE_SWEEP; 
      pv_counter = 0; 
    }else
      pv_counter++;
    break; 
    
  case IVSWEEP_PHASE_SWEEP:
    if((pv_counter++) >= config.ivsweep_sample_period){
      uint32_t vin = vin_raw, iin=iin_raw; 

      scandal_get_scaled_value(UNSWMPPTNG_IN_VOLTAGE, (s32*)&vin); 
      scandal_get_scaled_value(UNSWMPPTNG_IN_CURRENT, (s32*)&iin); 
      
      pvdata.ivsweep.vin = vin; 
      pvdata.ivsweep.iin = iin; 
      senddata_flag = IVSWEEP_DATA; 
      
      vin -= config.ivsweep_step_size; 
      /* Once we hit the lowest voltage, switch
	 back to the original algorithm */ 
      if((vin < config.min_vin) || (control_is_saturated()))
	pv_track_switchto(pvdata.ivsweep.last_algorithm); 
      
      control_set_voltage(vin); 
      
      pv_counter = 0; 
    }
    break;
  }
}

static inline void pvtrack_manual_start(void){
    control_set_voltage(ABS_MAX_VIN);
}

static inline void pvtrack_manual(void){
}
