/* --------------------------------------------------------------------------
	Modifed for Atmega64M1 1/5/14

	Scandal EEPROM Code for MSP
	File name: scandal_eeprom.c
	Author: David Snowdon

    Copyright (C) David Snowdon, 2003. 
	Copyright (C) William Widjaja, 2015. 
 
    Implementation of Scandal EEPROM functions for MSP430. 
 
	Date: 4/7/03
   -------------------------------------------------------------------------- */ 
   
/* 
 * This file is part of Scandal.
 * 
 * Scandal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * Scandal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 
 * You should have received a copy of the GNU General Public License
 * along with Scandal.  If not, see <http://www.gnu.org/licenses/>.
 */
 
//FIXME   
//#include <io.h>
//#include <signal.h>
#include <avr/interrupt.h> // Interrupt headers 
#include <avr/eeprom.h> // Interrupt headers 

#include "scandal_eeprom.h"
#include "scandal_led.h"

#define FLASH_ADDRESS_OFFSET 128


/* Notes on flash programming: 
   David Snowdon, 5/4/2008
   There seems to be a problem in here with the system resetting just after
   a flash operation. It may make it half way through programming, or it 
   may not make it through at all. 
   In either case, we have problems with the flash not being completely
   written. 
   To rectify this, we should a) do checksums and b) go through this code
   again with a fine-toothed comb. 
*/ 

/* Segment A is assigned to Scandal configuration, segment B is assigned to user configuration */

/* ATmega64m1 has 2K Bytes of EEPROM. MSP430 has ...

	Choose 0x0000 as Segment A. 
	Chooses0

*/

//hacked up shit
//volatile scandal_config		               * hack_config;

//seems to break 1s timer green LED
//eeprom is 2048 bytes, 2Kb
void 	sc_init_eeprom(void){
			//FIXME
			//FCTL3 = FWKEY + LOCK; 
			//FCTL2 = FWKEY + FSSEL0 + (40 - 1); 
  /* MCLK/20 for Flash Timing Generator, assuming a 7.3728 MHz crystal
     gives us a 368kHz clock */ 
  /* Should be between 257 and 476 kHz according to section 5.3.1 
     of the user manual */ 
  
  /*
  //atmega
  
  //no inits yet
  EEARH= 0b00000000; //EEPROM Address Registers
  EEARL= 0b00000000;
  //Bits 9..0 � EEAR10..0: EEPROM Address
  
  EEDR =0b00000000;//EEPROM Data Register
  //Bits 7..0 � EEDR7.0: EEPROM Data
  
  EECR =0b00001100; //The EEPROM Control Register
	//Bits 7..6 � Reserved Bits										=0b00
	//Bits 5..4 � EEPM1 and EEPM0: EEPROM Programming Mode Bits		=0b00 Erase and write in one operation (atomic operation)
	// Bit 3 � EERIE: EEPROM Ready Interrupt Enable					=0b1 Enables the EEPROM ready Interrupt
	// Bit 2 � EEMWE: EEPROM Master Write Enable					=0b1 Allow writing on EEWE=1
	// Bit 1 � EEWE: EEPROM Write Enable							=0b0 Write strobe
	// Bit 0 � EERE: EEPROM Read Enable								=0b0 Read enable
  
  */
}

//backup.
/*
while(EECR & (1<<EEWE))
		;
// Set up address register 
EEAR = uiAddress;
// Start eeprom read by writing EERE 
EECR |= (1<<EERE);
// Return data from data register 
return EEDR;
*/

//atmega mod
void sc_read_conf(scandal_config*	conf){
	
	//	Usage: eeprom_read_block (void *pointer_ram, const void *pointer_eeprom, size_t n)
	//void *pointer_ram				where the read eeprom is going locally into a variable		
	//const void *pointer_eeprom	address to start
	//size_t n						number of bytes to be read
	
	
	//uint8_t eeprom_read_byte (const uint8_t *addr) 
	
	u16	i;
	u08*	ptr;
	u08*  flash_ptr;
	uint16_t num;
	ptr = (u08*)conf;	//pointer to address of config to write to.
	
	//flash_ptr = (const void*)0;   // Segment A (Scandal Config)

	//Scandal configs
	//size of scandal_config is 50 Bytes (6 outchannels* 8bytes). 8bytes = 2 32bit values.
	// Read the flash segment 
	//Segment A (Scandal configuration)  
	printf("EEPROM start\r\n");
	
	for(i=0; (i<(sizeof(scandal_config))) && (i < 128); i++) {
		num = i+FLASH_ADDRESS_OFFSET;
	   *ptr = eeprom_read_byte((uint16_t*)num);			//basically copying the value of Segment A to the value at conf's address
	   printf("EEPROM at %d, address: %d is: %x\r\n", i, num,  *ptr);
	   ptr++;
	}   
      //eeprom_read_word((uint16_t*)NUM_EXECUTIONS_ADDRESS);
	  
	// User configs 
	// Read Segment B (User config)
	if(i < sizeof(scandal_config)){ // make sure that scandal config isnt bigger than expected and overflowed.
	  //flash_ptr = (u08*)0x1000;
		for(; (i<(sizeof(scandal_config))); i++){
			printf("EEPROM at %d, address: %d is: %x\r\n", i, i,  *ptr);
			*ptr++ = eeprom_read_byte((uint8_t*)i);	//*flash_ptr++;
		}
	}
	
	
	
}
/*
void sc_read_conf(scandal_config*	conf){
	
	//conf =  &hack_config; //hacked
	
	u16	i;
	u08*	ptr;
	u08*    flash_ptr;
	
	ptr = (u08*)conf;	//pointer to config return location
	flash_ptr = (u08*)0x1080;     //goto a particular part of EEPROM memory in MSP specific            // Segment A (Scandal Config)

	//Scandal configs
	// Read the flash segment 
	for(i=0;(i<(sizeof(scandal_config))) && (i < 128);i++) //size of scandal_config is 50 Bytes
	  *ptr++ = *flash_ptr++;				//basically copying the location of Segment A

	// User configs 
	// Read segment B 
	if(i < sizeof(scandal_config)){ // make sure that scandal config isnt bigger than expected and overflowed.
	  flash_ptr = (u08*)0x1000;
		for(;(i<(sizeof(scandal_config))); i++){
			*ptr++ = *flash_ptr++;
		}
	}
}*/

//FIXME cannot write or read anywhere
//atmega mod

void sc_write_conf(scandal_config*	conf){
	
	
	u16	i;
	uint8_t*    ptr;
	uint8_t*    flash_ptr;
	char cSREG; // from c code example in atmega64m1 datasheet
	uint16_t num;
	
	cSREG = SREG; // store status register SREG value temporarily 
	ptr = (u08*)conf;	//pointer to address of config to write to.
	
	cli(); //Global Interrupt Disable during timed sequence 
	
	//iterate through entire scandal_config block
	printf("EEPROM write start\r\n");
	//Segment A (Scandal configuration)      
	for(i=0; (i<(sizeof(scandal_config))) && (i < 128); i++) {//size of scandal_config is 50 Bytes
		num = i+FLASH_ADDRESS_OFFSET;
	   printf("EEPROM write iteration %d\r\n", i);
		// Wait for completion of previous write (EEWE = 0 when write finishes)
		printf("EEPROM write at address: %d is: %x\r\n", num, *ptr);
		
		eeprom_update_byte(( uint8_t *)num , *ptr);      *ptr++;
	  
		/*
		while (EECR & (1<<EEWE)) {
		
			// Set up address and data registers 
			//EEAR = (uint8_t*)i+FLASH_ADDRESS_OFFSET; //Write new EEPROM address to EEAR
			EEAR = i+FLASH_ADDRESS_OFFSET; //Write new EEPROM address to EEAR
			EEDR = *ptr; //Write new EEPROM data to EEDR (optional). 8bit EEDR. *ptr
	      
		   
	      *ptr++;
		  
			EECR |= (0<<EEPM1); // erase and write in 1 operation
			EECR |= (0<<EEPM0); // erase and write in 1 operation
			
			//following two instructions (EEMWE and EEWE) need to be together.
			EECR |= (1<<EEMWE); // start EEPROM write - Write logical one to EEMWE master enable 
			EECR |= (1<<EEWE); // Start eeprom write by setting EEWE to 1
			
			//EEPROM Control Register � EECR
			//Bits 5..4 � EEPM1 and EEPM0: EEPROM Programming Mode Bits
			//Bit 3 � EERIE: EEPROM Ready Interrupt Enable
			//Bit 2 � EEMWE: EEPROM Master Write Enable
			//Bit 1 � EEWE: EEPROM Write Enable
			//Bit 0 � EERE: EEPROM Read Enable
			
			
		}	*/		
	}
	//Segment B (User config)
	if(i < sizeof(scandal_config)){
		
		for(; (i<(sizeof(scandal_config))); i++) {//size of scandal_config is 128 Bytes (0x80)
	      num = i;
	      printf("EEPROM write iteration %d \r\n", i);
		   // Wait for completion of previous write (EEWE = 0 when write finishes)
		   printf("EEPROM write at address: %d is: %x\r\n", num, *ptr);
		
		   eeprom_update_byte(( uint8_t *) num, *ptr);         *ptr++;
	
	
			// Wait for completion of previous write (EEWE = 0 when write finishes)
			/*
			while(EECR & (1<<EEWE)) {
				// Set up address and data registers 
				EEAR = i; //Write new EEPROM address to EEAR
				EEDR = *ptr; //Write new EEPROM data to EEDR (optional). 8bit EEDR. *ptr
	         printf("EEPROM write at address: %d is: %x\r\n", i+FLASH_ADDRESS_OFFSET,  *ptr);
	         *ptr++;
			 
				EECR |= (0<<EEPM1); // erase and write in 1 operation
				EECR |= (0<<EEPM0); // erase and write in 1 operation
				//following two instructions (EEMWE and EEWE) need to be together.
				EECR |= (1<<EEMWE); // start EEPROM write - Write logical one to EEMWE master enable 
				EECR |= (1<<EEWE); // Start eeprom write by setting EEWE to 1
			
				//EEPROM Control Register � EECR
				//Bits 5..4 � EEPM1 and EEPM0: EEPROM Programming Mode Bits
				//Bit 3 � EERIE: EEPROM Ready Interrupt Enable
				//Bit 2 � EEMWE: EEPROM Master Write Enable
				//Bit 1 � EEWE: EEPROM Write Enable
				//Bit 0 � EERE: EEPROM Read Enable
         
			}*/			
		}		
	}
	
	SREG = cSREG; // restore SREG value (I-bit) 
	sei(); // set Global Interrupt Enable 
	
	
}


/*
//void sc_write_conf(scandal_config*	conf){
	
	//hack_config = &conf;
	
	
	u16	i;
	uint8_t*    ptr;
	uint8_t*    flash_ptr;
	uint16_t saved_sr;


	saved_sr = READ_SR;
	dint();

	ptr = (u08*)conf;
	flash_ptr = (u08*)0x1080;                 // Segment A 
	
	// Erase the flash segment 
	FCTL1 = FWKEY + ERASE;
	FCTL3 = FWKEY;
	*flash_ptr = 0;                           // Dummy write erases the segment 

	// Write the flash segment 
	for(i=0;i<(sizeof(scandal_config)) && (i < 128); i++){
	  FCTL3 = FWKEY; 
	  FCTL1 = FWKEY + WRT; //Set for write operation 
	  *flash_ptr++ = *ptr++;
	  FCTL1 = FWKEY; 
	  FCTL3 = FWKEY + LOCK; 
	}

	if(i < sizeof(scandal_config)){
		flash_ptr = (u08*)0x1000;                 // Segment B 
		
			// Erase the flash segment 
		FCTL1 = FWKEY + ERASE;
		FCTL3 = FWKEY;
		*flash_ptr = 0;                           
		
		// Write the flash segment 
		FCTL1 = FWKEY + WRT;                      
		for(;i<(sizeof(scandal_config));i++){
		  FCTL3 = FWKEY; 
		  FCTL1 = FWKEY + WRT;// Set for write operation
		  *flash_ptr++ = *ptr++;
		  FCTL1 = FWKEY; 
		  FCTL3 = FWKEY + LOCK; 
		}
	}

	if((saved_sr & GIE) != 0) {
	  eint();
	}
			
//}
*/

void sc_user_eeprom_read_block(u32 loc, u08* data, u08 length){
  
   u16	i;
  
   for(i=0; i<length; i++) {
	   printf("EEPROM block read at address: %d is: %x\r\n", i,   *data);
      *data = eeprom_read_byte((uint8_t*)i );
	   *data++;
   }
  
}


//#warning "sc_user_eeprom_write_block implementation presently ignores the loc parameter"
void sc_user_eeprom_write_block(u32 loc, u08* data, u08 length){
	u16	i;
	char cSREG; // from c code example in atmega64m1 datasheet
	uint16_t num;
	cSREG = SREG; // store status register SREG value 
	cli(); //Global Interrupt Disable during timed sequence 
	
	//Segment B (User config)
#if SIZEOF_SCANDAL_CONFIG <= 128
	//iterate through entire scandal_config block
	for(i=0; i<length; i++) {  //convert length to byte size
		
	   printf("EEPROM blcok write iteration %d\r\n", i);
		// Wait for completion of previous write (EEWE = 0 when write finishes)
		printf("EEPROM block write at address: %d is: %x\r\n", i, *data);
		eeprom_update_byte(( uint8_t *)i , *data);      *data++;
	}	
		
#else
#warning USER CONFIG WILL NOT BE WRITTEN TO EEPROM
#endif

	SREG = cSREG; // restore SREG value (I-bit) 
	sei(); // set Global Interrupt Enable 
	
}

/*
//#warning "sc_user_eeprom_write_block implementation presently ignores the loc parameter"
void sc_user_eeprom_write_block(u32 loc, u08* data, u08 length){
	u16	i;
	u08*    flash_ptr;
	uint16_t saved_sr;

	saved_sr = READ_SR;
	dint(); 
	
	{
	  volatile int blerg; 

	  for(blerg = 0; blerg < 10000; blerg++)
	    ;
	} 

#if SIZEOF_SCANDAL_CONFIG <= 128
	flash_ptr = (u08*)0x1000;                 // Segment B 
  
	// Erase the flash segment 
	FCTL1 = FWKEY + ERASE;
	FCTL3 = FWKEY;
	*flash_ptr = 0;                           
	
 	// Write the flash segment
	FCTL1 = FWKEY + WRT;                      
	for(i=0;i<length;i++){
	  FCTL3 = FWKEY; 
	  FCTL1 = FWKEY + WRT; // Set for write operation 
	  *flash_ptr++ = *data++;
	  FCTL1 = FWKEY; 
	  FCTL3 = FWKEY + LOCK; 
	}
#else
#warning USER CONFIG WILL NOT BE WRITTEN TO EEPROM
#endif
	if(saved_sr & GIE)
	  eint(); 
}
*/