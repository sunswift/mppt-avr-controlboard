/*
 * can.c
 *
 * Created: 22/06/2013 11:51:39 PM
 *  Author: william
 * Based off sensor_three_phase_BLDC.c
 *merged with MCP2510.c
 */ 

#include <avr/io.h>				// Define Port names for atmega64m1
#include <avr/interrupt.h> // Interrupt headers 

#include "scandal_message.h"
#include "can.h"		//macros and definitions for CAN module 

void doCAN();

//From sensor_three_phase_BLDC.c
//Atmel Corporation AVR452, Jan 2006

//global variables 

unsigned int speed=0x100;
//unsigned int measured_speed;
unsigned int message;

unsigned char num_channel;
unsigned char num_data;
unsigned char direction=CLOCKWISE;

/*
//FROM sensor_three_phase_BLDC
void can_init(void)   // init CAN macro to received frames on channel O
    {                 // and to send frames on channel 1
  CANGCON |= MSK_CANGCON_GRES; // reset CAN
//   
//    reset all mailboxes 
  for (num_channel = 0; num_channel < NB_MOB; num_channel++)
  {
   CANPAGE  = num_channel << 4;
   CANCDMOB = CH_DISABLE;
   CANSTMOB  = 0;
   CANIDT1  = 0;
   CANIDT2  = 0;
   CANIDT3  = 0;
   CANIDT4  = 0;
   CANIDM1  = 0;
   CANIDM2  = 0;
   CANIDM3  = 0;
   CANIDM4  = 0;
   for (num_data = 0; num_data < 8; num_data++) CANMSG = 0;
   }
   
   //Modify for 16Mhz crystal and 1Mbaud rate/
  //setup bit timing at 100k with 8 MHz crystal
 CANBT1  = 0x08;
 CANBT2  = 0x0C;
 CANBT3  = 0x37;
 CANGCON |=  MSK_CANGCON_ENA;      // start CAN 
 
 
// Initialise channels 0 to 5. 

 // Channel 0 init 
 CANPAGE = (0 << 4);               // CHNB=0x00; select channel 0 
 CANSTMOB  = 0x00;                 // reset channel status 
 CANCDMOB = CH_DISABLE;            // reset control and dlc register 

 // Channel 0: identifier = 11bits. CANIDT=0x123
 CANIDT1 = 0x24;
 CANIDT2 = 0x60;

 // Channel 0: mask = 11bits. 0x7F0 
 CANIDM1 = 0xFE;
 CANIDM2 &= ~0xE0;
 CANIDM4 = 0;

// Channel 0 configuration 
 CANIDT4 &=~0x04;                             // clear bit rtr in CANIDT4. 
 CANCDMOB |= DLC_MAX;                         // Reception 8 bytes.
 CANCDMOB |= CH_RxENA;                        // Reception enabled without buffer

  // Channel 1 init 
 CANPAGE = (1 << 4);                          // CHNB=0x01; select channel 1
 CANSTMOB  = 0x00;                            // reset channel status
 CANCDMOB = CH_DISABLE;                       // reset control and dlc register

 // Channel 1: identifier = 11bits. CANIDT=0x123 
 CANIDT1 = 0x24;
 CANIDT2 = 0x60;

 // Channel 1: mask = 11bits. 0x7F0
 CANIDM1 = 0xFE;
 CANIDM2 &= ~0xE0;
 CANIDM4 = 0;
// interrupt configuration
 CANIE2|=0x01;                                           // IECH0=1
 CANGIE = ((1<<ENRX)|(1<<ENIT));              // Can_Rx & IT enable

}
*/

//From AVR Freaks, modified for atmega

void can_init(void)//working
{

int mob_number;
int j; //loop counters
//int i;

   // Switch CAN to reset mode
    CANGCON  |=  (1<<SWRES);
	
	//CAN General Control Register - CANGCON
	// Bit 7  ABRQ: Abort Request
	// Bit 6  OVRQ: Overload Frame Request
	// Bit 5  TTC: Time Trigger Communication
	// Bit 4  SYNTTC: Synchronization of TTC
	// Bit 3  LISTEN: Listening Mode
	// Bit 2  TEST: Test Mode
	// Bit 1  ENA/STB: Enable / Standby Mode
	// Bit 0  SWRES: Software Reset Request 
 
// reset all mailboxes MObs
//FIXME: MObs only go to 0 to 5 (no.of mobs defined? )
  //for (i =0; i<15; i++)
  for (mob_number =0; mob_number<5; mob_number++)
  {
    CANPAGE = (mob_number<<4);    // select MOb, shifts 'ith' Mob up by 4 bits
	
		// CAN Page MOb Register - CANPAGE
		// Bit 7:4  MOBNB3:0: MOb Number	(0 to 5)
		// Bit 3  AINC: Auto Increment of the FIFO CAN Data Buffer Index (Active Low)
		// Bit 2:0  INDX2:0: FIFO CAN Data Buffer Index
		// 	
    
	CANCDMOB = 0;       // disable mailbox MOb
	//  CAN MOb Control and DLC Register - CANCDMOB 
	//  Bit 7:6  CONMOB1:0: Configuration of Message Object = 0b00 (disable)
	//  Bit 5  RPLV: Reply Valid (auto reply) = 0b0
	//  Bit 4  IDE: Identifier Extension (A/B = 0/1) =0b0
	//  Bit 3:0  DLC3:0: Data Length Code =0b0000
	
	CANSTMOB = 0;       // clear status interrupts
	// CAN MOb Status Register - CANSTMOB
	//  Bit 7  DLCW: Data Length Code Warning 
	// Bit 6  TXOK: Transmit OK
	//  Bit 5  RXOK: Receive OK
	//  Bit 4  BERR: Bit Error (Only in Transmission)
	//  Bit 3  SERR: Stuff Error
	//  Bit 2  CERR: CRC Error
	//  Bit 1  FERR: Form Error
	//  Bit 0  AERR: Acknowledgment Error


	// CAN Identifier Tag Registers -
    CANIDT1 = 0;       // clear ID
    CANIDT2 = 0;
    CANIDT3 = 0;
    CANIDT4 = 0;
	
	// 	CAN Identifier Mask Registers - identifier of data frame to send 
    CANIDM1 = 0;       // get all messages
    CANIDM2 = 0;       // 1 = check bit (enable comparison for truth)
    CANIDM3 = 0;       // 0 = ignore bit (force true)
    CANIDM4 = 0;

// 		CAN Enable Interrupt MOb Registers

    CANIE1 = 0;				// reserved bits.
    CANIE2 = (1<<mob_number);   // disable? enable MOb interrupts (write 1 to bit)
	

    for (j = 0; j<8; j++) //why loop?
        CANMSG = 0;       // clear data
			// CAN Data Message Register - CANMSG
			// Bit 7:0  MSG7:0: Message Data = 0x00
  } 


	
	//eg
 /* setup bit timing at 100k with 8 MHz crystal*/
//  CANBT1  = 0x08;// = 0b00001000//BRP = 0b000100 = 4
//  CANBT2  = 0x0C;//=0b00001100 SJW = 0b00, PRS = 0b110
//  CANBT3  = 0x37; // = 0b00110111, PHS1=0b011, PHS2=0b011,
   
    // Set bus speed to 125Kbps with 16Mhz clock
	
	//TODO: Set bus speed to 50kbps, 
	//CANBT1 = 0x26; //0b00100110 50kbps
	//CANBT1 = 0x26; //0b00100110 100kbps	
	CANBT1 = 0x1E; //50kbps, longer phase segments 1, 2, short re sync	

	//CANBT1 = 0x0E; //=0b00001110 250kbps
	//CAN Bit Timing Register 1 - CANBT1
	// Bit 7 Reserved Bit
	// Bit 6:1  BRP5:0: Baud Rate Prescaler = 0b000111
	// Bit 0  Reserved Bit = 0b0
	
	//CANBT2 = 0x0E;// = 0b00000100 50kbps
	//CANBT2 = 0x04;// = 0b00000100 250kbps
	//CANBT2 = 0x04;// = 0b00000100 100kbps
	CANBT2 = 0x64; //0x6E; //40kbps //50kbps, longer phase segments 1, 2, short re sync	
	
	// 	CAN Bit Timing Register 2 - CANBT2
	// Bit 7 Reserved Bit
	// Bit 6:5  SJW1:0: Re-Synchronization Jump Width = 0b00
	// Bit 4  Reserved Bit
	// Bit 3:1  PRS2:0: Propagation Time Segment = 0b010
	// Bit 0  Reserved Bit
	
	//CANBT3 = 0x36;// = 0b00010011 50kbps
	//CANBT3 = 0x13;// = 0b00010011 100kbps
	CANBT3 = 0x7E; //50kbps, longer phase segments 1, 2, short re sync	
	
	
	// CAN Bit Timing Register 3 - CANBT3
	// Bit 7 Reserved Bit
	// Bit 6:4  PHS2[2:0]: Phase Segment 2 = 0b001
	// Bit 3:1  PHS1[2:0]: Phase Segment 1 = 0b001
	// Bit 0  SMP: Sample Point(s) = 0b1

//    CANBT1=0x0E;
//    CANBT2=0x0C;
//    CANBT3=0x37;


   CANGIT = 0; //reset all general interrupts
	//   CAN General Interrupt Register - CANGIT
	//    Bit 7  CANIT: General Interrupt Flag
	//    Bit 6  BOFFIT: Bus Off Interrupt Flag
	//    Bit 5  OVRTIM: Overrun CAN Timer
	//    Bit 4  BXOK: Frame Buffer Receive Interrupt
	//    Bit 3  SERG: Stuff Error General
	//    Bit 2  CERG: CRC Error General
	//    Bit 1  FERG: Form Error General
	//    Bit 0  AERG: Acknowledgment Error General
   
   CANGIE = (1 << ENIT) | (1 << ENRX) | (1 << ENTX); //enable all interrupts, enable transmit and recieve interrupts
	// CAN General Interrupt Enable Register - CANGIE
	//  Bit 7  ENIT: Enable all Interrupts (Except for CAN Timer Overrun Interrupt)
	//  Bit 6  ENBOFF: Enable Bus Off Interrupt
	//  Bit 5  ENRX: Enable Receive Interrupt
	//  Bit 4  ENTX: Enable Transmit Interrupt
	//  Bit 3  ENERR: Enable MOb Errors Interrupt
	//  Bit 2  ENBX: Enable Frame Buffer Interrupt
	//  Bit 1  ENERG: Enable General Errors Interrupt
	//  Bit 0  ENOVRT: Enable CAN Timer Overrun Interrupt


    // Set timer prescaler to 199 which resuts in a timer
   // freq of 10 kHz @ 16MHz
	// CAN Timer Control Register - CANTCON
	//  Bit 7:0  TPRSC7:0: CAN Timer Prescaler
	CANTCON = 99;//199;//99;
	//CANTCON = 9;

   // reset all mObs
   //for (mob_number = 0; mob_number < 15; mob_number ++)
//    for (mob_number = 0; mob_number < 5; mob_number ++) //6 MOB's on atmega64m1
//    {
//       CANPAGE = mob_number <<4;
//       CANCDMOB = 0x00;
//       CANSTMOB = 0x00;
//    // you do not need to do anything else
//    }

   // Enable the CAN
   CANGCON =  (1<<ENASTB);

} 





// #pragma vector=CANIT_vect      // CAN ISR
// __interrupt void  CAN_ISR(void) {
//FROM brendonshaw @ AVR Freaks for the AT90CAN128
 /*
//ISR(CANIT_vect)
ISR(CAN_INT_vect)   // handler for CAN interrupt
{
   uint8_t canpage;
   uint8_t mob;
   
   if ((CANHPMOB & 0xF0) != 0xF0)//  check for priority
   {
			// CAN Highest Priority MOb Register - CANHPMOB
			// Bit 7:4  HPMOB3:0: Highest Priority MOb Number
			// Bit 3:0  CGP3:0: CAN General Purpose Bits
			
      // save MOb page register
      canpage = CANPAGE;
      
      // select MOb page with the highest priority
      CANPAGE = CANHPMOB & 0xF0;
      mob = (CANHPMOB >> 4);
      
      // a interrupt is only generated if a message was transmitted or received
      if (CANSTMOB & (1 << TXOK))
      {
         // clear MOb
         CANSTMOB &= 0;
         CANCDMOB = 0;
         
         
         // enable transmission
         CANCDMOB |= (1<<CONMOB0);
         
         // reset interrupt
         if (mob < 8)
            CANIE2 &= ~(1 << mob);
         else
            CANIE1 &= ~(1 << (mob - 8));

      }
         
      if(CANSTMOB & (1 << RXOK) )   // Receive OK
      {

         CANSTMOB &= (~(1 << RXOK));         // rest RX Flag

         
         // clear flags
         CANCDMOB = (1 << CONMOB1) | (CANCDMOB & (1 << IDE));
         
         // reset interrupt
         if (mob < 8)
            CANIE2 &= ~(1 << mob);
         else
            CANIE1 &= ~(1 << (mob - 8));
         
      }
      
      // restore MOb page register
      CANPAGE = canpage;
   }
   else
   {
      // no MOb matches with the interrupt => general interrupt
      CANGIT |= 0;
   }
}

*/
//hints from http://www.avrfreaks.net/index.php?name=PNphpBB2&file=printview&t=106343&start=0

void doCAN() { //works!!
   //uint8_t canpage;
   //uint8_t mob;
   
   //load the CAN identifier 
   //uint32_t id = 0xFFFF;// not a real ID //works
      
   uint32_t id =scandal_mk_scandal_error_id();  // proper error id
  
  //load the timestamp
   u32 value= scandal_get_realtime32();// find the current scandal time
   //u32 value = 0xFFFF; //random

	//load a message
   message = UNSWMPPTNG_ERROR_WATCHDOG_RESET;//
   //message = 0xFFFF; //random message

//choose the MOB1 as the mailbox, allow message pointer to auto increment INDX 
// and start at index 0 INDX = 0. INDX points to the ith byte of CANMSG
  CANPAGE = 0b00010000;//(1 << 4);   //MOBNB3:0 = 0b0001 select channel 1 
  // CAN Page MOb Register - CANPAGE
		// Bit 7:4  MOBNB3:0: MOb Number	(0 to 5) = 0b0001 => 1 (MOB1)
		// Bit 3  AINC: Auto Increment of the FIFO CAN Data Buffer Index (Active Low)=0b0 (auto increment of Data buffer CANMSG)
		// Bit 2:0  INDX2:0: FIFO CAN Data Buffer Index = 0b000 (start at INDX0)
	    
		
//load the identifier into the CAN register.
//CAN2.0B identifier = 29bits.
//IDT28:18 = id(), IDT17:0 = id()		
	CANIDT4 = (char)((id << 3) & 0x000000FF);//(char)((id & 0x000F) << 3);//(char)((id << 3)& 0xF8); //
  CANIDT3 = (char)((id >> 5) & 0x000000FF);//(char)((id & 0x00F0) << 3);//(char)((id >> 5)& 0xFF);
  CANIDT2 = (char)((id >> 13) & 0x000000FF);//(char)((id & 0x000F) << 3);//(char)((id >> 13)& 0xFF);
  CANIDT1 = (char)((id >> 21) & 0x000000FF);//(char)((id & 0x000F) << 3);//(char)((id >> 21)& 0xFF);
  
 	// 	CAN Identifier Mask Registers - identifier of data frame to send 
    CANIDM1 = 0;       // get all messages
    CANIDM2 = 0;       // 1 = check bit (enable comparison for truth)
    CANIDM3 = 0;       // 0 = ignore bit (force true)
    CANIDM4 = 0; 
  
  
//   CANIDT2 = (char)(id << 5);
//   CANIDT1 = (char)(id >> 3);
  
  //CANMSG = (char)(message >> 8);
  //Start loading /putting data (8 bytes of it) in the mailbox (MoB)
  
  // for (i=0; i<8; i++) CANMSG = can_data[i];  //load saved data 
  
  // for (i=0; i<8; i++) CANMSG = can_data[i];  //load saved data 

  CANMSG = (char)(message); // 1st byte // error message
  CANMSG = 0x00;						// 2nd byte
  CANMSG = 0x00;						// 3rd byte
  CANMSG = 0x00;						// 4th byte
  CANMSG = (value >> 24) & 0xFF; //time stamp
  CANMSG = (value >> 16) & 0xFF;
  CANMSG = (value >> 8) & 0xFF;
  CANMSG = (value >> 0) & 0xFF;
  
//how to transmit 8 bytes if CANMSG is only 8 bits long>  
//   	msg.data[0] = err;
// 
// 	msg.data[4] = (value >> 24) & 0xFF;
// 	msg.data[5] = (value >> 16) & 0xFF;
// 	msg.data[6] = (value >> 8) & 0xFF;
// 	msg.data[7] = (value >> 0) & 0xFF;
// 	
  //CAN Data Message Register - CANMSG
  //Bit 7:0  MSG7:0: Message Data
  
  CANSTMOB = 0x00;
	
	CANCDMOB = 0x18; // IDE = 0b1 (CAN2.0B), DLC3:0 = 0b1000 transmit 8 bytes   
  //CANCDMOB = 0x12; // IDE = 0b1 (CAN2.0B), DLC3:0 = 0b0010 transmit 2 bytes 
  //CANCDMOB = 0x02; // IDE = 0b0 (CAN2.0A), DLC3:0 = 0b0010 transmit 2 bytes 
  
  //CANCDMOB = msg->length;

  CANCDMOB |= CH_TxENA;    //  tx transmission enabled 
  CANEN2 |= (1 << 1);       // ENMOB5:0 = 0b000010 Enable MOB for channel 1  
  
  //CANSTCH=0x00; // reset channel status?
  
  
  //CANPAGE = (0 << 4);    // select chanel 0
   
   
   
//   
//   canpage = CANPAGE;
//       
//   // select MOb page with the highest priority
//   CANPAGE = CANHPMOB & 0xF0;
//   mob = (CANHPMOB >> 4);
//       
//   // clear MOb
//   CANSTMOB &= 0;
//          
//          
// 	// enable transmission
// 	CANCDMOB |= (1<<CONMOB0); //enable tx
//          
//   // reset interrupt
//   if (mob < 8)
//     CANIE2 &= ~(1 << mob); //disable the interrupt
//   else
//     CANIE1 &= ~(1 << (mob - 8));
// 
//      
//   // restore MOb page register
//   CANPAGE = canpage;

}





/*! -------------------------------------------------------------------------
    \file mcp2510.c
        MCP2510 Driver

	File name: MCP2510.c
	Author: David Snowdon
	Date: 25/4/02
	
	
		File name: MCP2510.c
	Author: William Widjaja
	Date: 5/8/13
	Description: Added CAN functions for Atmega64M1
    -------------------------------------------------------------------------- */
#include "scandal_can.h"
#include "spi_devices.h"
#include "scandal_spi.h"
#include "scandal_error.h"
#include "mcp2510.h"
#include "scandal_led.h"

#include "can_drv.h" //Atmega specific drivers. Will need to fix these so that it dosent go thru MCP2510 file
#include "can.h" //Atmega specific drivers. 

//Uart debug stuff
#include "uart.h"		//macros and definitions for UART module



/* "spi_devices.h" must #define MCP2510 in order for this to compile.
   MCP2510 is the identifier of the SPI device to be used with
   spi_select(); */


/* File scope variables */
u32	fil1_data, fil1_mask;
u32	fil2_data, fil2_mask;

/* Buffers */
#if CAN_TX_BUFFER_SIZE > 0
/* Transmit buffer */
can_msg			cantxbuf[CAN_TX_BUFFER_SIZE];
u08			tx_buf_start;
u08			tx_num_msgs;
u08			tx_buf_lock;
#endif

#if CAN_RX_BUFFER_SIZE > 0
/* Receive Buffer */
volatile can_msg	canrxbuf[CAN_RX_BUFFER_SIZE];
volatile u08		rx_buf_start;
volatile u08		rx_num_msgs;
volatile u08		rx_buf_lock;
#endif

/* Prototypes */
u08 enqueue_message(can_msg*	msg);
u08 send_queued_messages(void);
u08 buffer_received(void);
void careful_clear_receive_interrupt(u08  flag);

//atmega specfiics
//extern uint8_t can_send_message(can_msg*	msg);//const can_t *msg);
//extern uint8_t atmega_send_message(can_msg*	msg);//const can_t *msg);
extern uint8_t atmega_send_message(u32   id, u08*  buf, u08   size, u08   priority);
extern void _enable_mob_interrupt(uint8_t mob); 
extern uint8_t _find_free_mob(void); 
void can_copy_message_to_mob(can_msg*	msg);//const can_t *msg);

//atmega prototypes
extern uint8_t atmega_receive_message(u32* id, u08* buf, u08* length);

//can_buffer
void can_buffer_init (void); 
/* -------------------------------------------------------------------------
   MCP2510 Data
   ------------------------------------------------------------------------- */
/* The fields below are set up to correspond to
   the baud rates defined by scandal */
#define CNF1_FIELD 0
#define CNF2_FIELD 1
#define CNF3_FIELD 2
/*! \todo Put in the actual register values for the various bit timings */
/* The bit timings given below were generated using intrepidcs's
   MCP2510 Bit Timing calculation utility.
   http://www.intrepidcs.com/mcp2510 */
/* All these values assume Multiple Bit Sampling and no wake up filter */
u08 bit_timings[SCANDAL_NUM_BAUD][3] = {
  {0x00, 0xD8, 0x01},    /* SCANDAL_B1000 */    /* NB: This is risky due to MCP2510 errata */
  {0x00, 0xF8, 0x05},    /* SCANDAL_B500 */
  {0x01, 0xF8, 0x05},    /* SCANDAL_B250 */
  {0x03, 0xF8, 0x05},    /* SCANDAL_B125 */
  {0x07, 0xFA, 0x07},    /* SCANDAL_B50  */
  {0x1F, 0xFF, 0x07}     /* SCANDAL_B10  */
};

/* CAN Interface Functions */
/*! \fn init_can
    \brief Initialise the CAN hardware, setting options to their defaults (as specified in scandal_can.h)
 */


//FIXME for atmega64m1
//Atmega specs: 
	//6 MO buffers
	//11bit (A) and 29bit (B) identifiers 
	//1Mbit @ 8Mhz Clkio.


//Notes from current integrator LPC11C14
/* CAN_ functions are non scandal specific.

 * A note about transmission: Often what happens in a main loop is a test for a
 * 1 second timer, and then a burst of scandal_send_channels happens. If we try
 * to send more messages than we have transmit buffers, we could fail to send
 * some messages. To solve this problem, we have a transmit buffer. When we
 * call scandal_send_channel, can_send_msg gets called, and eventually CAN_Send
 * gets called. If CAN_Send fails (i.e. there was no free message object), then
 * we store the message temporarily in a CAN_txbuf location. In the main while
 * loop, every iteration calls handle_scandal, which calls can_poll. can_poll
 * will call send_enqueued messages which sends out any enqueued messages.

 * For example: In the steering wheel, we send out wavesculptor commands every
 * 100ms. We also send out about 15 scandal channels every 1s. This means that
 * when we go to send out those 17 messages on the bounary of 1s, our transmit
 * codepath has to be able to handle 17 messages in *very* quick succession.
 * Currently on the LPC11C14, we have 32 message objects, 20 of which are used
 * for message reception. That only leaves 12 for message transmission. We need
 * to handle the case where we send out more than 12 messages in quick
 * succession, and the transmit buffer helps us with this by spreading out the
 * message transmission over multiple main loop iterations using handle_scandal
 * and can_poll.
 */



//Note: can_drv.h defines pins for CAN in and out.

//not working DNU
void init_can(void){
	/* This is done a little dodgy hee and needs fixing */
	//u32	i;
	//u08	value;

	//Reset the atmega CAN module
	Can_reset(); // send a requset to reset the resets the CAN controller
	
	//Do we need to play around with CANGCON register?
	//CANGCON
	// 	Bit 7  ABRQ: Abort Request
	// 	Bit 6  OVRQ: Overload Frame Request
	// 	Bit 5  TTC: Time Trigger Communication
	// 	Bit 4  SYNTTC: Synchronization of TTC
	// 	Bit 3  LISTEN: Listening Mode
	// 	Bit 2  TEST: Test Mode
	// 	Bit 1  ENA/STB: Enable / Standby Mode
	// 	Bit 0  SWRES: Software Reset Request


	//init_spi(); MSP

	//MCP2510_reset(); MSP

	// Need some kind of big delay here
// #ifdef __PCM__
// 	delay_ms(10);
// #else
// 	for(i=0;i<10000;i++)
// 		__asm__ ("nop");
// #endif

	//Setup ports on Atmega specific macro
  //- Pull-up on TxCAN & RxCAN one by one to use bit-addressing
  CAN_PORT_DIR &= ~(1<<CAN_INPUT_PIN ); // set Rx to input port direction defined for atmega uC
  CAN_PORT_DIR &= ~(1<<CAN_OUTPUT_PIN); // set Tx to input
  CAN_PORT_OUT |=  (1<<CAN_INPUT_PIN ); //set initial port value to 1
  CAN_PORT_OUT |=  (1<<CAN_OUTPUT_PIN); //set initially to 1

	
	/* Configure the bit timing */
	//can_baud_rate(DEFAULT_BAUD); //mcp2515.c MSP default_baud = 115200bits/sec

	//ATMEGA configure bit timing rate.
	can_fixed_baudrate(DEFAULT_BAUD); //can_drv.c can_fixed_baudrate(u08 mode)
	
	//Can_bit_timing(DEFAULT_BAUD); //write to the CANBT can bit timing register. Uses FOSC and CAN_BAUDRATE to derive before hand, just writes the values in this function
	//can_fixed_baudrate used.for Non-CAN_AUTOBAUD
	
	/* Configure the interrupts */
	/*! \todo Configure the MCP2510 Interrupt registers */

	/* Set up recieve filters */
	/*! \todo Set up the MCP2510 Receive registers */
	/* Below is a temporary measure */
// 	value = 0;
// 	value |= (MCP2510_RECEIVE_EXTENDED << trRXM00); //buffer data from ??
// 	MCP2510_write(RXB1CTRL, &value, 1); //set up SPI and write to MCP register to ...

//	value |= (1 << trBUKT01); /* Allow overflow to buffer 1*/
/*	MCP2510_write(RXB0CTRL, &value, 1);*/

	//ATmega other setup 
	//- Enable CAN peripheral
  can_clear_all_mob(); // c.f. function in "can_drv.c"        
	Can_enable();

	/* Set the controller to normal mode */
// 	MCP2510_set_mode(MCP2510_NORMAL_MODE);
// 
// 	fil1_data = 0; fil1_mask = 0xFFFFFFFF;
// 	fil2_data = 0; fil2_mask = 0xFFFFFFFF;

// #if CAN_TX_BUFFER_SIZE > 0 
// 	tx_buf_lock = 0;
// 	tx_buf_start = 0;
// 	tx_num_msgs = 0;
// #endif
// 
// #if CAN_RX_BUFFER_SIZE > 0
// 	rx_buf_lock = 0;
// 	rx_buf_start = 0;
// 	rx_num_msgs = 0;
// #endif
// 	
// 	value = 0x03;
// 	MCP2510_write(CANINTE, &value, 1);

/*	enable_can_interrupt();*/
}

void can_buffer_init (void){
	
	fil1_data = 0; fil1_mask = 0xFFFFFFFF;
	fil2_data = 0; fil2_mask = 0xFFFFFFFF;

#if CAN_TX_BUFFER_SIZE > 0 
	tx_buf_lock = 0;
	tx_buf_start = 0;
	tx_num_msgs = 0;
#endif

#if CAN_RX_BUFFER_SIZE > 0
	rx_buf_lock = 0;
	rx_buf_start = 0;
	rx_num_msgs = 0;
#endif

}


// Taken from sensor_three_phase_BLDC.c 
// void can_init(void)   // init CAN macro to received frames on channel O
//     {                 // and to send frames on channel 1
//   CANGCON |= MSK_CANGCON_GRES; /* reset CAN */
//   /* reset all mailboxes */
//   for (num_channel = 0; num_channel < 15; num_channel++)
//   {
//    CANPAGE  = num_channel << 4;
//    CANCDMOB = CH_DISABLE;
//    CANSTMOB  = 0;
//    CANIDT1  = 0;
//    CANIDT2  = 0;
//    CANIDT3  = 0;
//    CANIDT4  = 0;
//    CANIDM1  = 0;
//    CANIDM2  = 0;
//    CANIDM3  = 0;
//    CANIDM4  = 0;
//    for (num_data = 0; num_data < 8; num_data++) CANMSG = 0;
//    }
//   /* setup bit timing at 100k with 8 MHz crystal*/
//  CANBT1  = 0x08;
//  CANBT2  = 0x0C;
//  CANBT3  = 0x37;
//  CANGCON |=  MSK_CANGCON_ENA;      /* start CAN */
// 
//  /* Channel 0 init */
//  CANPAGE = (0 << 4);               /* CHNB=0x00; select channel 0 */
//  CANSTMOB  = 0x00;                 /* reset channel status */
//  CANCDMOB = CH_DISABLE;            /* reset control and dlc register */
// 
//  /* Channel 0: identifier = 11bits. CANIDT=0x123 */
//  CANIDT1 = 0x24;
//  CANIDT2 = 0x60;
// 
//  /* Channel 0: mask = 11bits. 0x7F0 */
//  CANIDM1 = 0xFE;
//  CANIDM2 &= ~0xE0;
//  CANIDM4 = 0;
// 
//  /* Channel 0 configuration */
//  CANIDT4 &=~0x04;                             /* clear bit rtr in CANIDT4. */
//  CANCDMOB |= DLC_MAX;                         /* Reception 8 bytes.*/
//  CANCDMOB |= CH_RxENA;                        /* Reception enabled without buffer.*/
// 
//   /* Channel 1 init */
//  CANPAGE = (1 << 4);                          /* CHNB=0x01; select channel 1 */
//  CANSTMOB  = 0x00;                            /* reset channel status */
//  CANCDMOB = CH_DISABLE;                       /* reset control and dlc register */
// 
//  /* Channel 1: identifier = 11bits. CANIDT=0x123 */
//  CANIDT1 = 0x24;
//  CANIDT2 = 0x60;
// 
//  /* Channel 1: mask = 11bits. 0x7F0 */
//  CANIDM1 = 0xFE;
//  CANIDM2 &= ~0xE0;
//  CANIDM4 = 0;
//  /* interrupt configuration */
//  CANIE2|=0x01;                                           /* IECH0=1 */
//  CANGIE = ((1<<ENRX)|(1<<ENIT));              /* Can_Rx & IT enable */
// 
// }
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 






/*! \fn    can_interrupt
    \brief Should be called by the host code when the controller generates an interrupt
 */
void can_interrupt(void){
#if CAN_RX_BUFFER_SIZE > 0
	buffer_received();
#endif
}

/*! \fn 	can_poll
    \brief	should be called by the host code when there is some time available to
		do some background work */
void can_poll(void){
#if CAN_RX_BUFFER_SIZE > 0
	buffer_received();
#endif

#if CAN_TX_BUFFER_SIZE > 0
	send_queued_messages();
#endif
}




/*! Get a message from the CAN controller */
u08 can_get_msg(can_msg* msg){ //message called from handle scandal - in while loop
#if CAN_RX_BUFFER_SIZE > 0

  if(rx_num_msgs == 0)
	return (NO_MSG_ERR);

  disable_can_interrupt();
  
  *msg = canrxbuf[rx_buf_start]; // check the can rx buffer (global array)
  rx_buf_start = (rx_buf_start + 1) & CAN_RX_BUFFER_MASK;
  rx_num_msgs--;
  
  enable_can_interrupt();

  return(NO_ERR);
#else
  //return(MCP2510_receive_message(&(msg->id), msg->data, &(msg->length)));
  return(atmega_receive_message(&(msg->id), msg->data, &(msg->length)));
#endif
}



//Atmega Can 

// get next free MOb
uint8_t _find_free_mob(void)
{
   
   uint8_t i;
   for (i = 0;i < 6;i++) //6 mobs (0 to 5) only (not 15).
   {
      // load MOb page
      CANPAGE = i << 4;
      
      // check if MOb is in use
      if ((CANCDMOB & ((1 << CONMOB1) | (1 << CONMOB0))) == 0) //if either is in transmit or recieves
         return i;
   }
   
   return 0xFF;
} 

// enable interrupt of corresponding MOb
void _enable_mob_interrupt(uint8_t mob)
{
   if (mob < 8)
      CANIE2 |= (1 << mob);
   else
      CANIE1 |= (1 << (mob - 8));
} 

// void can_copy_message_to_mob(can_msg*	msg)//;const can_t *msg)
// {
// 
//    uint8_t i;
// 
//    // write DLC (Data Length Code)
//    //CAN MOb Control and DLC Register
//    CANCDMOB = msg->length;
//    //Bit 7:6  CONMOB1:0: Configuration of Message Object 01 - enable transmission.
//    
//    //Bit 3:0  DLC3:0: Data Length Code
//    
//    // standard CAN ID
//    //CAN Identifier Tag Registers
//    CANIDT4 = 0;
//    CANIDT3 = 0;
//    CANIDT2 = (uint8_t)  msg->id << 5;
//    CANIDT1 = (uint16_t) msg->id >> 3;
//       
// 	 //Bit 31:3  IDT28:0: Identifier Tag (2.0B)
// 	  
//    // Put message data into the CAN message
//    // Dummy data
//    for (i = 0;i < msg->length;i++)
//    {
//       CANMSG = i;
//    }
//    //Real data:
//    for (i = 0;i < msg->length;i++)
//    {
//       CANMSG = msg->data[i];
//    }
// 
// } 
// modify for priority aswell. 
//currently sends out can messages only, does not check mailboxes
//
//FIXME!!! Changed format - check for MOBs

// uint8_t atmega_send_message(can_msg* msg)//const can_t *msg)
// {
	
	//testing send message
uint8_t atmega_send_message(u32   id,
				 u08*  buf,
				 u08   size,
				 u08   priority)
{
	
	//where does priority go?

	  //uint8_t canpage;
   //uint8_t mob;
   volatile int i;
   //load the CAN identifier 
   //uint32_t id = 0xFFFF;// not a real ID //works
      
   //uint32_t id =msg->id;  // proper error id
  
  //load the timestamp
   //u32 value= scandal_get_realtime32();// find the current scandal time
   //u32 value = 0xFFFF; //random
	//load a message
   //message = msg->data;//
   //message = 0xFFFF; //random message

//FIXME for dynamic mailboxes
//find a free MOB
	volatile uint8_t mob;

  mob = _find_free_mob();
	if (mob == 0xFF) { //invalid mob
		
		return(BUF_FULL_ERR);
		
	} else { // valid mob.
			//printf("mob = %d\r\n", mob);
   // load corresponding MOb page ...
   CANPAGE = (mob << 4);

	//choose the MOB1 as the mailbox, allow message pointer to auto increment INDX 
	// and start at index 0 INDX = 0. INDX points to the ith byte of CANMSG
		//CANPAGE = 0b00010000;//(1 << 4);   //MOBNB3:0 = 0b0001 select channel 1 
		// CAN Page MOb Register - CANPAGE
			// Bit 7:4  MOBNB3:0: MOb Number	(0 to 5) = 0b0001 => 1 (MOB1)
			// Bit 3  AINC: Auto Increment of the FIFO CAN Data Buffer Index (Active Low)=0b0 (auto increment of Data buffer CANMSG)
			// Bit 2:0  INDX2:0: FIFO CAN Data Buffer Index = 0b000 (start at INDX0)
	    
		
	//load the identifier into the CAN register.
	//CAN2.0B identifier = 29bits.
	//IDT28:18 = id(), IDT17:0 = id()		
		CANIDT4 = (char)((id << 3) & 0x000000FF);//(char)((id & 0x000F) << 3);//(char)((id << 3)& 0xF8); //
		CANIDT3 = (char)((id >> 5) & 0x000000FF);//(char)((id & 0x00F0) << 3);//(char)((id >> 5)& 0xFF);
		CANIDT2 = (char)((id >> 13) & 0x000000FF);//(char)((id & 0x000F) << 3);//(char)((id >> 13)& 0xFF);
		CANIDT1 = (char)((id >> 21) & 0x000000FF);//(char)((id & 0x000F) << 3);//(char)((id >> 21)& 0xFF);
  
 	// 	CAN Identifier Mask Registers - identifier of data frame to send 
    CANIDM1 = 0;       // get all messages
    CANIDM2 = 0;       // 1 = check bit (enable comparison for truth)
    CANIDM3 = 0;       // 0 = ignore bit (force true)
    CANIDM4 = 0; 
  
  
//   CANIDT2 = (char)(id << 5);
//   CANIDT1 = (char)(id >> 3);
  
  //CANMSG = (char)(message >> 8);
  //Start loading /putting data (8 bytes of it) in the mailbox (MoB)
  
  // for (i=0; i<8; i++) CANMSG = can_data[i]; /* load saved data */
  
   for (i=0; i<size; i++) {
			CANMSG = buf[i]; /* load saved data */
   }   	
		//   CANMSG = msg->data[0];//(char)(message); // 1st byte // error message
		//   CANMSG = msg->data[1];						// 2nd byte
		//   CANMSG = msg->data[2];						// 3rd byte
		//   CANMSG = msg->data[3];						// 4th byte
		//   CANMSG = msg->data[4](value >> 24) & 0xFF; //time stamp
		//   CANMSG = (value >> 16) & 0xFF;
		//   CANMSG = (value >> 8) & 0xFF;
		//   CANMSG = (value >> 0) & 0xFF;
  
		//how to transmit 8 bytes if CANMSG is only 8 bits long>  
		//   	msg.data[0] = err;
		// 
		// 	msg.data[4] = (value >> 24) & 0xFF;
		// 	msg.data[5] = (value >> 16) & 0xFF;
		// 	msg.data[6] = (value >> 8) & 0xFF;
		// 	msg.data[7] = (value >> 0) & 0xFF;
		// 	
			//CAN Data Message Register - CANMSG
			//Bit 7:0  MSG7:0: Message Data
  
			CANSTMOB = 0x00; //reset channel status??
  
			//set to CAN 2.0B
	
			//FIXME : transmit 'size bytes"
			CANCDMOB = 0x10; // IDE = 0b1 (CAN2.0B), DLC3:0 = 0b1000 transmit 8 bytes   
			//CANCDMOB = 0x12; // IDE = 0b1 (CAN2.0B), DLC3:0 = 0b0010 transmit 2 bytes 
			//CANCDMOB = 0x02; // IDE = 0b0 (CAN2.0A), DLC3:0 = 0b0010 transmit 2 bytes 
  
			// load the length
			//DLC = msg->length;//doesnt work
			CANCDMOB |=size;
	
			//#define CH_TxENA           0x40 = 0b01000000
			CANCDMOB |= CH_TxENA;    //  tx transmission enabled 
				//CANEN2 |= (1 << 1);       // ENMOB5:0 = 0b000010 Enable MOB for channel 1  
				CANEN2 |= (1 << mob);       // ENMOB5:0 = 0b000010 Enable MOB for channel n  
		
		//CANPAGE = (mob << 4);
			//if transmission done, free MOB by change back to 0
				CANCDMOB &= !CH_TxENA;    //  tx transmission disabled 
			//CANEN2 &= (0 << 1);       // ENMOB5:0 = 0b000010 disable MOB for channel 1  
				CANEN2 &= (0 << mob);       // ENMOB5:0 = 0b000010 disable MOB for channel 1  
	
	
			return(NO_ERR);
			}
}


//not working
// {
//    // check if there is any free MOb
//    uint8_t mob;
// 
//    mob = _find_free_mob();
//    
//    // load corresponding MOb page ...
//    CANPAGE = (mob << 4);
//    
//    // clear flags
//    CANSTMOB &= 0;
//    
//    // ... and copy the data
//    can_copy_message_to_mob( msg );
//    
//    // enable interrupt
//    _enable_mob_interrupt(mob);
//    
//    // enable transmission
//    CANCDMOB |= (1<<CONMOB0);
//    
//    return (mob + 1);
// 

/*! Send a message to the CAN controller */
u08 can_send_msg(can_msg* msg, u08 priority){
	
	//FIXME!!!!!!!!!! Blindly sending messages!
#if CAN_TX_BUFFER_SIZE > 0
  u08 err;
  err = enqueue_message(msg);
  send_queued_messages();
  return err;
#else
	// insert Atmega specific send message commands.
	//return(atmega_send_message(&msg)); 
	return(atmega_send_message(msg->id, msg->data, msg->length, priority));
  //return(MCP2510_transmit_message(msg->id, msg->data, msg->length, priority));
#endif

	//FIXME no checking.
	//return(atmega_send_message(msg->id, msg->data, msg->length, priority));
  

}


// conditionally defined function - only if transmit buffers are used
//make sure tx and rx variables are initialised.
#if CAN_TX_BUFFER_SIZE > 0
u08 enqueue_message(can_msg* msg){
	u08 pos;
	u08 i;
	
	if(tx_num_msgs >= CAN_TX_BUFFER_SIZE) // check if there is room
		return BUF_FULL_ERR;								// if not, report no room, 

	pos = (tx_buf_start + tx_num_msgs) & CAN_TX_BUFFER_MASK; // find position of message 
																														// based on number of msg 

	cantxbuf[pos].id = msg->id;			//store id, data and length
	for(i=0; i<8; i++)
		cantxbuf[pos].data[i] = msg->data[i];
	cantxbuf[pos].length = msg->length;

	tx_num_msgs++;

	return NO_ERR;
}

u08 send_queued_messages(void){
	can_msg*	msg;
	u08		err;

	if(tx_num_msgs <= 0)
		return (NO_MSG_ERR);

	msg = &(cantxbuf[tx_buf_start]);

	//disable_can_interrupt();
	//FIXME Atmega
	//err = MCP2510_transmit_message(msg->id, msg->data, msg->length, (msg->id >> 21) & 0xFF);
	err = atmega_send_message(msg->id, msg->data, msg->length, (msg->id >> 21) & 0xFF);
	//enable_can_interrupt();

	if(err == NO_ERR){
	    tx_buf_start = (tx_buf_start + 1) & CAN_TX_BUFFER_MASK;
	    tx_num_msgs--;
   	}

	return err;
}
#endif

#if CAN_RX_BUFFER_SIZE > 0
u08 buffer_received(void){
	u08 	pos, err=NO_ERR;
	can_msg* msg;

	while(err == NO_ERR){
		/* Copy a received message out of mail box 0 */
		if(rx_num_msgs >= CAN_RX_BUFFER_SIZE){
			disable_can_interrupt();
			careful_clear_receive_interrupt(trRX0IF);
			enable_can_interrupt();
			return BUF_FULL_ERR;
		}else{
			disable_can_interrupt();
			pos = (rx_buf_start + rx_num_msgs) & CAN_RX_BUFFER_MASK;
			msg = (can_msg*)&(canrxbuf[pos]);
	
			err = MCP2510_receive_message(&(msg->id), msg->data, &(msg->length));
	
			if(err == NO_ERR){
				rx_num_msgs++;
			}
			enable_can_interrupt();
		}
	}

	return err;
}
#endif

/*! Register an message ID we want to receive */
/*! \todo Actually register the ID!!! */
u08 can_register_id(u32 mask, u32 data, u08 priority){
  u08   buf[4];

  u32 data_diff;
  data_diff = data ^ fil1_data; /* The bits which are different will be 1, the bits that aren't will be 0 */


  /* If fil1_mask is its original value, set fil1_mask the requested value */
  if(fil1_mask == 0xFFFFFFFF){
  	fil1_data = data;
  	fil1_mask = mask;
  }else{
  	fil1_mask = (fil1_mask & mask) & (~data_diff);
  	fil1_data = data & fil1_mask;
  }
	//FIXME
  //MCP2510_set_mode(MCP2510_CONFIGURATION_MODE);

    /* SID10:SID3 */
  buf[0] = fil1_data >> 21;
  buf[1] = (((fil1_data >> 18) & 0x07) << 5) | ((u32)1<<EXIDE) | ((fil1_data >> 16) & 0x03);
  buf[2] = ((fil1_data >> 8) & 0xFF);
  buf[3] = ((fil1_data >> 0) & 0xFF);
  //FIXME
  //MCP2510_write(RXF0SIDH, buf, 4);


  buf[0] = fil1_mask >> 21;
  buf[1] = (((fil1_mask >> 18) & 0x07) << 5) | ((u32)1<<EXIDE) | ((fil1_mask >> 16) & 0x03);
  buf[2] = ((fil1_mask >> 8) & 0xFF);
  buf[3] = ((fil1_mask >> 0) & 0xFF);
  //FIXME
  //MCP2510_write(RXM0SIDH, buf, 4);

  //FIXME
  //MCP2510_set_mode(MCP2510_NORMAL_MODE);

  return NO_ERR;
}

/*! Set the baud rate mode to one of the rate modes */
u08 can_baud_rate(u08 mode){
	
	//FIXME atmega
  return(MCP2510_bit_timing(mode));//MSP
}

/*
 * MCP2510 Functions
 * NB: All of these functions assume that the SPI driver calls will not fail
 */

/*! Read the MCP2510 TX Error count */
u08 MCP2510_read_tx_errors(void){
	u08 value;
	MCP2510_read(TEC, &value, 1);
	return value;
}

/*! Read the MCP2510 RX Error count */
u08 MCP2510_read_rx_errors(void){
	u08 value;
	MCP2510_read(REC, &value, 1);
	return value;
}

/*! Set the operating mode of the MCP2510 */
void MCP2510_set_mode(u08 mode){
	/* This piece of ugliness is the workaround for a silicon bug
		as suggested in the revision AA errata */

	if(mode == MCP2510_LISTEN_MODE){
		MCP2510_bit_modify(CANCTRL, 0xE0, (MCP2510_LOOPBACK_MODE << 5));
		MCP2510_bit_modify(CANCTRL, 0xE0, (mode << 5));
	}else{
		MCP2510_bit_modify(CANCTRL, 0xE0, (mode << 5));

		/* This piece of ugliness is the workaround for a silicon bug
			as suggested in the errata */
		while(MCP2510_get_mode() != mode){
			MCP2510_bit_modify(CANCTRL,0xE0, (MCP2510_CONFIGURATION_MODE << 5));
			MCP2510_bit_modify(CANCTRL,0xE0, (mode << 5));
		}
	}

}

void MCP2510_set_clkout_mode(u08 mode){
  MCP2510_bit_modify(CANCTRL, 0x03, mode); 
}

/*! Retrieve the operating mode of the MCP2510 */
u08 MCP2510_get_mode(){
    	u08 value;
    	MCP2510_read(CANSTAT, &value, 1);
	return(value >> 5);
}


//Fixme atmega - 

/* Note: This routine will fail if none of the transmit buffers are available */
/*! Transmit a message using the MCP2510 and a free Tx buffer */
u08     MCP2510_transmit_message(u32   id,
				 u08*  buf,
				 u08   size,
				 u08   priority){
  unsigned char value;
  unsigned char idbuf[8];

  value = MCP2510_read_status();
  if((value & (1<<2)) != 0)    /* If the buffer is pending a transmission, return BUF_FULL_ERR */
   	return BUF_FULL_ERR;

  /* In order to comply with the CAN standard, the size (DLC) must
     be less than or equal to 8 */
  if(size>8)
    size = 8;

  /* Load the ID */ // put it all into idbuf 
  idbuf[0] = (id >> 21) & 0xFF;
  idbuf[1] = (((id >> 18) & 0x07) << 5) | (1<<EXIDE) | ((id >> 16) & 0x03);
  idbuf[2] = ((id >> 8) & 0xFF);
  idbuf[3] = ((id >> 0) & 0xFF);
  MCP2510_write(TXB0SIDH, idbuf, 4);

  /* Load the data */
  MCP2510_write(TXB0D0, buf, size);

  /* Set the size byte */
  MCP2510_write(TXB0DLC, &size, 1);

  /* Set the priority and flag the buffer to be transmitted */
  MCP2510_bit_modify(TXB0CTRL, TXBNCTRL_TXP_MASK, priority);
  MCP2510_RTS(0x01);

  return(NO_ERR);
}

void careful_clear_receive_interrupt(u08  flag){
	u08 	value;

	/* This is a hack solution to a pretty nasty silicon bug.
		It needs to be improved such that we don't lose any
		incoming messages */

	value = MCP2510_read_status();

	if(value & (1<<2)){
		/* Clear the TXREQ bit */
		MCP2510_bit_modify(TXB0CTRL, (1<<trTXREQ0), (0<<trTXREQ0));
		/* Read thestatus again */
		value = MCP2510_read_status();
		if(value & (1<<2)){	/* Check to see if the flag is still set, indicating the message is currently being tramitted */
			while( value & (1<<2) ) /* Wait for the bit to be cleared */
				value = MCP2510_read_status();
			/* Once its been cleared, reset the receive flag */
		    	MCP2510_bit_modify(CANINTF, (1<<flag), 0x00);
		}else{
			/* Clear the receive flag */
		   	MCP2510_bit_modify(CANINTF, (1<<flag), 0x00);
		   	/* Re set the tx request */
			MCP2510_bit_modify(TXB0CTRL, (1<<trTXREQ0), (1<<trTXREQ0));
		}
	}else /* The bit wasn't set at all... Clear the flag */
		MCP2510_bit_modify(CANINTF, (1<<flag), 0x00);

}


/* buf has to be capable of holding 8 bytes */
uint8_t atmega_receive_message(u32* id, u08* buf, u08* length){
  u08     value;

  value = MCP2510_read_status();
  if(value & (1<<STATUS_RX0IF)){ /* Valid message recieved */
    	/* Make sure we haven't suffered an overflow error */
  	MCP2510_read(EFLG, buf, 1);
  	if(*buf & (1<<trRX0OVR)){
  		careful_clear_receive_interrupt(trRX0IF);
  		MCP2510_bit_modify(EFLG, (1<<trRX0OVR), 0x00);
  		return NO_MSG_ERR;
  	}

    	/* Copy in the identifier - to be updated to support
    	   extended identifiers */
    	MCP2510_read(RXB0SIDH, buf, 4);

    	/* Check to make sure we haven't received a standard length ID */
    	if(!(buf[1] & (1 << EXIDE))){
    		return(STD_ID_ERR);
    	}

  	(*id) = ((u32)buf[0]) << 21;
    	(*id) |= ((u32)(buf[1] >> 5) & 0x07) << 18 ;
    	(*id) |= ((u32)(buf[1] & 0x03)) << 16;
    	(*id) |= ((u32)buf[2]) << 8;
    	(*id) |= ((u32)buf[3]) << 0;

    	/* Read the length */
    	MCP2510_read(RXB0DLC, buf, 1);
    	*length = buf[0] & 0x0F;

    	/* Read length number of bytes from the recieve buffer */
    	MCP2510_read(RXB0D0, buf, *length);


	/* The function below clears the receive interrupt in a manner that should
		work around the silicon bug detailed in errata item 6 */
	careful_clear_receive_interrupt(trRX0IF);

    	return NO_ERR;
  }

//  if(value & (1<<STATUS_RX1IF)){ /* Valid message recieved */
//
//  	/* Make sure we haven't suffered an overflow error */
//  	MCP2510_read(EFLG, buf, 1);
//  	if(*buf & (1<<trRX1OVR)){
//  		MCP2510_bit_modify(EFLG, (1<<trRX1OVR), 0x00);
//  		careful_clear_receive_interrupt(trRX1IF);
//  		return NO_MSG_ERR;
//  	}
//
//   	/* Copy in the identifier */
//    	MCP2510_read(RXB1SIDH, buf, 4);
//
//    	/* Check to make sure we haven't received a standard length ID */
//    	if(!(buf[1] & (1 << EXIDE)))
//    		return(STD_ID_ERR);
//
//  	(*id) = ((u32)buf[0]) << 21;
//    	(*id) |= ((u32)(buf[1] >> 5) & 0x07) << 18 ;
//   	(*id) |= ((u32)(buf[1] & 0x03)) << 16;
//    	(*id) |= ((u32)buf[2]) << 8;
//    	(*id) |= ((u32)buf[3]) << 0;
//
//    	/* Read the length */
//    	MCP2510_read(RXB1DLC, buf, 1);
//    	*length = buf[0] & 0x0F;
//
//    	/* Read length bytes from the recieve buffer */
//    	MCP2510_read(RXB1D0, buf, *length);
//
//	careful_clear_receive_interrupt(trRX1IF);
//
//    	return NO_ERR;
//    }

  /* There was no message to be recieved,
     return an error */
  return NO_MSG_ERR;
}


/* buf has to be capable of holding 8 bytes */
u08 MCP2510_receive_message(u32* id, u08* buf, u08* length){
  u08     value;

  value = MCP2510_read_status();
  if(value & (1<<STATUS_RX0IF)){ /* Valid message recieved */
    	/* Make sure we haven't suffered an overflow error */
  	MCP2510_read(EFLG, buf, 1);
  	if(*buf & (1<<trRX0OVR)){
  		careful_clear_receive_interrupt(trRX0IF);
  		MCP2510_bit_modify(EFLG, (1<<trRX0OVR), 0x00);
  		return NO_MSG_ERR;
  	}

    	/* Copy in the identifier - to be updated to support
    	   extended identifiers */
    	MCP2510_read(RXB0SIDH, buf, 4);

    	/* Check to make sure we haven't received a standard length ID */
    	if(!(buf[1] & (1 << EXIDE))){
    		return(STD_ID_ERR);
    	}

  	(*id) = ((u32)buf[0]) << 21;
    	(*id) |= ((u32)(buf[1] >> 5) & 0x07) << 18 ;
    	(*id) |= ((u32)(buf[1] & 0x03)) << 16;
    	(*id) |= ((u32)buf[2]) << 8;
    	(*id) |= ((u32)buf[3]) << 0;

    	/* Read the length */
    	MCP2510_read(RXB0DLC, buf, 1);
    	*length = buf[0] & 0x0F;

    	/* Read length number of bytes from the recieve buffer */
    	MCP2510_read(RXB0D0, buf, *length);


	/* The function below clears the receive interrupt in a manner that should
		work around the silicon bug detailed in errata item 6 */
	careful_clear_receive_interrupt(trRX0IF);

    	return NO_ERR;
  }

//  if(value & (1<<STATUS_RX1IF)){ /* Valid message recieved */
//
//  	/* Make sure we haven't suffered an overflow error */
//  	MCP2510_read(EFLG, buf, 1);
//  	if(*buf & (1<<trRX1OVR)){
//  		MCP2510_bit_modify(EFLG, (1<<trRX1OVR), 0x00);
//  		careful_clear_receive_interrupt(trRX1IF);
//  		return NO_MSG_ERR;
//  	}
//
//   	/* Copy in the identifier */
//    	MCP2510_read(RXB1SIDH, buf, 4);
//
//    	/* Check to make sure we haven't received a standard length ID */
//    	if(!(buf[1] & (1 << EXIDE)))
//    		return(STD_ID_ERR);
//
//  	(*id) = ((u32)buf[0]) << 21;
//    	(*id) |= ((u32)(buf[1] >> 5) & 0x07) << 18 ;
//   	(*id) |= ((u32)(buf[1] & 0x03)) << 16;
//    	(*id) |= ((u32)buf[2]) << 8;
//    	(*id) |= ((u32)buf[3]) << 0;
//
//    	/* Read the length */
//    	MCP2510_read(RXB1DLC, buf, 1);
//    	*length = buf[0] & 0x0F;
//
//    	/* Read length bytes from the recieve buffer */
//    	MCP2510_read(RXB1D0, buf, *length);
//
//	careful_clear_receive_interrupt(trRX1IF);
//
//    	return NO_ERR;
//    }

  /* There was no message to be recieved,
     return an error */
  return NO_MSG_ERR;
}


/*! Reset */
/* Note that the controller won't be available for 128 Fosc cycles */
void MCP2510_reset(void){
  spi_select_device(MCP2510);
  spi_transfer(MCP2510_RESET_COMMAND);
  spi_deselect_all();
}

/*! Read */
/* There must be enough space in the buffer to store the data */
/*! \todo Make the read and write commands use interrupt driven SPI transfers */
/*! \todo Return decent error messages! */
void MCP2510_read(u08	addr, u08* buf, u08 num_bytes){
  unsigned char i;

  spi_select_device(MCP2510);
  spi_transfer(MCP2510_READ_COMMAND);
  spi_transfer(addr);

  for(i = 0; i < num_bytes; i++)
    buf[i] = spi_transfer(0);

  spi_deselect_all();
}

/*! Write */
/*! \todo Make the read and write commands use interrupt driven SPI transfers */
/*! \todo Return decent error messages! */
void MCP2510_write(u08 addr, u08* buf, u08 num_bytes){
  u08 i;

  spi_select_device(MCP2510);
  spi_transfer(MCP2510_WRITE_COMMAND);
  spi_transfer(addr);

  for(i = 0; i < num_bytes; i++)
    spi_transfer(buf[i]);

  spi_deselect_all();
}

/*! Request to Send
 *
 *  Sends the data in one of the transmit buffers
 */
void MCP2510_RTS(u08 buf_bitfield){
  spi_select_device(MCP2510);
  spi_transfer(MCP2510_RTS_COMMAND | buf_bitfield );
  spi_deselect_all();
}

/*! Read Status
 *
 *  Returns the status register
 */
u08 MCP2510_read_status(){
  u08 value;

  spi_select_device(MCP2510);
  spi_transfer(MCP2510_READSTATUS_COMMAND);
  value = spi_transfer(0);
  spi_deselect_all();

  return(value);
}

/*! Bit modify */
/*  Modify the bits of a particular register using a mask and a data field */
/*  \todo Modify the bit modify command to use interrupt drive SPI */
void MCP2510_bit_modify(u08 addr, u08 mask, u08 data){
  spi_select_device(MCP2510);
  spi_transfer(MCP2510_BITMODIFY_COMMAND);
  spi_transfer(addr);
  spi_transfer(mask);
  spi_transfer(data);
  spi_deselect_all();
}

/*! Set the bit timing (Baud Rate) */
u08 MCP2510_bit_timing(u08 mode){
	
	//FIXME atmega
	
	
  /* Configure the bit timing */
  MCP2510_write(CNF1, &(bit_timings[mode][CNF1_FIELD]), 1);
  MCP2510_write(CNF2, &(bit_timings[mode][CNF2_FIELD]), 1);
  MCP2510_write(CNF3, &(bit_timings[mode][CNF3_FIELD]), 1);

  /*! \todo Proper error handling here */
  return NO_ERR;
}





//Interrupt for receive message 
//from http://www.avrfreaks.net/index.php?name=PNphpBB2&file=printview&t=117572&start=0
ISR (CAN_INT_vect)
{                 
   uint8_t length;
   uint8_t save_can_page;
   uint8_t byte;
   uint8_t can_data[7]; //FIX magic numbers
   
	u08 	pos; 
	u08 err = NO_ERR;
	can_msg* msg;
    
   save_can_page = CANPAGE;         // Save current MOB
   CANPAGE = CANHPMOB & 0xF0;      // Selects MOB with highest priority interrupt

////////////////////////// RX Interrupt //////////////////////////////
//make sure rx interrupt is enabled

   if (CANSTMOB & (1<<RXOK)) //pg 190
   {     // Interrupt caused by receive finished                     

			if(rx_num_msgs >= CAN_RX_BUFFER_SIZE){ // check if there is room in rx buffer
			
				return BUF_FULL_ERR;
			
			} else {
				pos = (rx_buf_start + rx_num_msgs) & CAN_RX_BUFFER_MASK;
				msg = (can_msg*)&(canrxbuf[pos]); //copy temporary can_msg to global rx buffer
				
				// Copy a received message out of mail box 0 
				*(&msg->length) = (CANCDMOB & 0x0F);   // DLC, number of bytes to be received

				for (byte=0; byte < *(&msg->length); byte++)
				{
					 msg->data[byte] = CANMSG;       // Get data, INDX auto increments CANMSG
				} // for
      
				*(&msg->id) = CANIDT;
	  
				CANCDMOB = ((1<<CONMOB1) | (8<<DLC0));  // Enable Reception 11 bit IDE DLC8
				// save the message and put it into the rx buffer to be processed.
				rx_num_msgs++;
			}  

      // Note - the DLC field of the CANCDMO register is updated by the received MOb. If the value differs from expected DLC, an error is set
   } // if (CANSTMOB...   
   
   CANSTMOB = 0x00;       // Reset interrupt, must be done in software
   CANPAGE = save_can_page;      // Restore original MOB
}

/*
// Check to make sure we haven't received a standard length ID 
    		return(STD_ID_ERR);
    	}

  	(*id) = ((u32)buf[0]) << 21;
    	(*id) |= ((u32)(buf[1] >> 5) & 0x07) << 18 ;
    	(*id) |= ((u32)(buf[1] & 0x03)) << 16;
    	(*id) |= ((u32)buf[2]) << 8;
    	(*id) |= ((u32)buf[3]) << 0;

    	// Read the length 
    	MCP2510_read(RXB0DLC, buf, 1);
    	*length = buf[0] & 0x0F;

    	// Read length number of bytes from the recieve buffer 
    	MCP2510_read(RXB0D0, buf, *length);


*/

/*
	//IDT28:18 = id(), IDT17:0 = id()		
		CANIDT4 = (char)((id << 3) & 0x000000FF);//(char)((id & 0x000F) << 3);//(char)((id << 3)& 0xF8); //
		CANIDT3 = (char)((id >> 5) & 0x000000FF);//(char)((id & 0x00F0) << 3);//(char)((id >> 5)& 0xFF);
		CANIDT2 = (char)((id >> 13) & 0x000000FF);//(char)((id & 0x000F) << 3);//(char)((id >> 13)& 0xFF);
		CANIDT1 = (char)((id >> 21) & 0x000000FF);//(char)((id & 0x000F) << 3);//(char)((id >> 21)& 0xFF);
  
 	// 	CAN Identifier Mask Registers - identifier of data frame to send 
    CANIDM1 = 0;       // get all messages
    CANIDM2 = 0;       // 1 = check bit (enable comparison for truth)
    CANIDM3 = 0;       // 0 = ignore bit (force true)
    CANIDM4 = 0; 
	*/

/*
typedef struct can_mg {
  u32 id;
  u08 data[CAN_MSG_MAXSIZE];
  u08 length;
} can_msg;
*/


/*
//! Get a message from the CAN controller 
u08 can_get_msg(can_msg* msg){ //message called from handle scandal - in while loop
#if CAN_RX_BUFFER_SIZE > 0

  if(rx_num_msgs == 0)
	return (NO_MSG_ERR);

  disable_can_interrupt();
  
  *msg = canrxbuf[rx_buf_start]; // check the can rx buffer (global array)
  rx_buf_start = (rx_buf_start + 1) & CAN_RX_BUFFER_MASK;
  rx_num_msgs--;
  
  enable_can_interrupt();

  return(NO_ERR);
#else
  //return(MCP2510_receive_message(&(msg->id), msg->data, &(msg->length)));
  return(atmega_receive_message(&(msg->id), msg->data, &(msg->length)));
#endif
}
*/


/*
#if CAN_RX_BUFFER_SIZE > 0
u08 buffer_received(void){
	u08 	pos, err=NO_ERR;
	can_msg* msg;

	while(err == NO_ERR){
		// Copy a received message out of mail box 0 
		if(rx_num_msgs >= CAN_RX_BUFFER_SIZE){
			disable_can_interrupt();
			careful_clear_receive_interrupt(trRX0IF);
			enable_can_interrupt();
			return BUF_FULL_ERR;
		}else{
			disable_can_interrupt();
			pos = (rx_buf_start + rx_num_msgs) & CAN_RX_BUFFER_MASK;
			msg = (can_msg*)&(canrxbuf[pos]);
	
			err = MCP2510_receive_message(&(msg->id), msg->data, &(msg->length));
	
			if(err == NO_ERR){
				rx_num_msgs++;
			}
			enable_can_interrupt();
		}
	}

	return err;
}

*/

/*
//FROM sensor_three_phase_BLDC.c
// Routine for CAN on Tx and Rx
ISR(CAN_INT_vect){   // handler for CAN interrupt

unsigned int id;                                      // can_data index 

// echo receive data on channel 0 reception 
 CANPAGE = (0 << 4);                          // CHNB=0x00; select channel 0 
 if((CANSTMOB & MSK_CANSTMOB_RxOk) == MSK_CANSTMOB_RxOk)
 {
 id = (((int)(CANIDT2))>>5) + (((int)(CANIDT1))<<3);       // V2.0 part A
 switch(id)
   {
				//case(0x120): Stop_motor();    break; // Stop motor

				//case(0x121):  Run_motor();     break; // Run motor

     case(0x122):  speed=(((int)(CANMSG))<<8);
                   speed = speed + (int)(CANMSG);

                   break; //  set speed

     case(0x123): direction=CANMSG;  break; // set direction motor

     case(0x124):  if ( (CANIDT4 & 0x04) == 0x04)
                    {
                         CANPAGE = (1 << 4);   // select chanel 1 
                         CANIDT2 = (char)(id << 5);
                         CANIDT1 = (char)(id >> 3);
                         CANMSG = (char)(measured_speed >> 8);
                         CANMSG = (char)(measured_speed);
                         CANSTMOB = 0x00;
                         CANCDMOB = 0x02;            // transmit 2 bytes 
                         CANCDMOB |= CH_TxENA;    //  emission enabled 
                         //CANEN2 |= (1 << 1);       // channel 1 enable 
                         CANPAGE = (0 << 4);    // select chanel 0
                      }
                     break;                  // send back measured speed
   }
 }

 CANPAGE = (0 << 4);
 CANSTMOB=0x00;                              // reset channel 0 status 
 CANEN2 |= (1 << 0);                         // channel 0 enable 
 CANCDMOB = DLC_MAX;                         // receive 8 bytes 
 CANCDMOB |= CH_RxENA;                       // reception enable 
 CANGIT = CANGIT;                            //  reset all flags 

}
*/